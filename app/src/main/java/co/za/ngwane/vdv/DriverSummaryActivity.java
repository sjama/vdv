package co.za.ngwane.vdv;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.device.ScanManager;
import android.device.scanner.configuration.PropertyID;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.codehaus.jackson.map.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.rtmc.dto.RoadSafety;
import co.za.rtmc.dto.Vehicle;
import co.za.rtmc.enums.AppConstants;
import co.za.rtmc.service.LicenceDecoder;
import co.za.rtmc.service.LocationAddress;
import co.za.rtmc.service.SessionManager;
import co.za.rtmc.service.Validator;
import co.za.rtmc.vo.AppLocationService;
import co.za.rtmc.vo.BoRoadSafetyInspection;
import co.za.rtmc.vo.GroupDTO;
import co.za.rtmc.vo.InfringementsGroup;
import co.za.rtmc.vo.Lmidtypecd;
import co.za.rtmc.vo.MvLicDiscVo;
import co.za.rtmc.vo.RdtstrptRwsVo;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_PHONE_STATE;

/**
 * Created by Jama on 2016/04/26.
 */
public class DriverSummaryActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener, View.OnFocusChangeListener {


    // Progress Dialog Object
    ProgressDialog prgDialog;
    // Error Msg TextView Object
    TextView errorMsg;
    AppLocationService appLocationService;


    // Name Edit View Object
    EditText surnameET;
    // Name Edit View Object
    EditText nameET;
    // Email Edit View Object
    EditText driverLicCodeET;
    // Passwprd Edit View Object
    EditText idNumberET;
    EditText mvTypeET;
    EditText expDateET;
    EditText prdpExpDateET;
    EditText prdpET;

    String licCode = "";
    String dlIdType = "";
    String action = "";
    private Context mContext;

    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    MvLicDiscVo discVo = new MvLicDiscVo();
    private CoordinatorLayout coordinatorLayout;
    Location gpsLocation;
    String[] addDetails;

    private final static String SCAN_ACTION = ScanManager.ACTION_DECODE;//default action
    private ActionBar actionBar;
    private EditText showScanResult;
    private Button btn;
    private Button mScan;
    private Button mClose;
    private int type;
    private int outPut;

    private Vibrator mVibrator;
    private ScanManager mScanManager;
    private SoundPool soundpool = null;
    private int soundid;
    private String barcodeStr;
    private boolean isScaning = false;
    private SweetAlertDialog pDialog;
    private BoRoadSafetyInspection roadSafetyInspection;


    String regNumber = "";
    private List<GroupDTO> groupDTOS = new ArrayList<>();
    private InfringementsGroup infringementsGroup;
    SessionManager session;
    private String menuCd;

    private  RdtstrptRwsVo rwsVo = new RdtstrptRwsVo();
    private Vehicle vehicle = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new SessionManager(getApplicationContext());
        menuCd = session.getMenuCd();

        if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_DRIVER) ) {
            setContentView(R.layout.activity_driver_summary_verify);

        } else   {
            setContentView(R.layout.activity_driver_summary);

        }


        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        appLocationService = new AppLocationService( DriverSummaryActivity.this);
        menuCd = session.getMenuCd();
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        Intent intent = getIntent();
        gpsLocation = getLocation();

        try {
            discVo = (MvLicDiscVo) intent.getSerializableExtra("discVo");
            rwsVo = (RdtstrptRwsVo) intent.getSerializableExtra("rdtstrptRwsVo");
            roadSafetyInspection = (BoRoadSafetyInspection) intent.getSerializableExtra("inspectionData");


            expDateET = (EditText) findViewById(R.id.regNumber);
            if ( rwsVo.getDriLicValidToD() != null ) {
                expDateET.setText(rwsVo.getDriLicValidToD());
                expDateET.setEnabled(false);
                expDateET.setClickable(false);

                SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date targetD = sdf.parse(rwsVo.getDriLicValidToD());
                Date date2 = new Date();

                if ( Validator.isDateOutOfRange(targetD, new Date(), Calendar.MONTH, 3 ) ) {
                    expDateET.setBackgroundColor(Color.GREEN);
                } else if (  Validator.isDateOutOfRange(targetD, new Date(), Calendar.MONTH, 2 )) {
                    expDateET.setBackgroundColor(Color.YELLOW);
                } else if ( targetD.compareTo(date2) < 0 ) {
                    expDateET.setBackgroundColor( Color.RED );
                }

                setDateTimeField();

            }

            prdpExpDateET = (EditText) findViewById(R.id.prdpEDate);
            if ( rwsVo.getDriPrDPValidToD() != null ) {
                prdpExpDateET.setText(rwsVo.getDriPrDPValidToD());
                prdpExpDateET.setEnabled(false);

                SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date targetD = sdf.parse(rwsVo.getDriPrDPValidToD());
                Date date2 = new Date();

                if ( Validator.isDateOutOfRange(targetD, new Date(), Calendar.MONTH, 3 ) ) {
                    prdpExpDateET.setBackgroundColor(Color.GREEN);
                } else if (  Validator.isDateOutOfRange(targetD, new Date(), Calendar.MONTH, 2 )) {
                    prdpExpDateET.setBackgroundColor(Color.YELLOW);
                } else if ( targetD.compareTo(date2) < 0 ) {
                    prdpExpDateET.setBackgroundColor( Color.RED );
                }

                setDateTimeField();

            } else {
                prdpExpDateET.setText( "N/A" );
                prdpExpDateET.setEnabled(false);

            }

        }catch (Exception e){
            e.printStackTrace();

        }

        errorMsg = (TextView) findViewById(R.id.register_error);
        // Find Name Edit View control by ID

        surnameET = (EditText) findViewById(R.id.driverSurName);
        surnameET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(25)});

        if ( rwsVo.getDrisurname() != null  ) {
            surnameET.setText(rwsVo.getDrisurname());
            surnameET.setEnabled(false);
        }


        nameET = (EditText) findViewById(R.id.driverName);
        nameET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(3)});
        if ( rwsVo.getDriname() != null  ) {
            nameET.setText(rwsVo.getDriname());
            nameET.setEnabled(false);
        }


        driverLicCodeET = (EditText) findViewById(R.id.driverLics);
        driverLicCodeET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});

        if ( rwsVo.getLiccd() != null  ) {
            driverLicCodeET.setText( rwsVo.getLiccd() );
            driverLicCodeET.setEnabled(false);
        }

        prdpET = (EditText) findViewById(R.id.driverPdps);
        prdpET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});
        prdpET.setEnabled(false);

        if ( rwsVo.getDriPrDPs() != null  ) {
            prdpET.setText( rwsVo.getDriPrDPs() );
            prdpET.setEnabled(false);
        } else {
            prdpET.setText( "N/A" );
        }

        idNumberET = (EditText) findViewById(R.id.idNumber);
        idNumberET.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        if ( rwsVo.getDriidn() != null  ) {
            idNumberET.setText(rwsVo.getDriidn());
            idNumberET.setEnabled(false);
        }

        final Spinner spinner1 = (Spinner) findViewById(R.id.idTypeSpinner);
        // Spinner click listener
        spinner1.setOnItemSelectedListener(this);
        // Create an ArrayAdapter using the string array and a default spinner layout
        final ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.idType_array, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner1.setAdapter(adapter1);

        spinner1.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(
                            AdapterView<?> parent, View view, int position, long id) {
                        System.out.print(parent.getItemAtPosition(position));
                        dlIdType = parent.getItemAtPosition(position).toString();
                        if (  rwsVo.getDrividtypecd() != null ) {
                            int spinnerPosition = adapter1.getPosition(rwsVo.getDrividtypecd());
                            spinner1.setSelection(spinnerPosition);
                            spinner1.setEnabled(false);
                        }
                        // showToast("Spinner1: position=" + position + " id=" + id);
                        // idnumber input status is changed from number to text.
                        if (dlIdType.equalsIgnoreCase(Validator.RSA_IDENTITY_DOCUMENT)) {
                            // show password
                            idNumberET.setInputType(InputType.TYPE_CLASS_NUMBER);
                        } else {
                            // hide password
                            idNumberET.setInputType(InputType.TYPE_CLASS_TEXT);
                        }
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        // showToast("Spinner1: unselected");
                    }
                });


//
//        Button mEmailSignInButton = (Button) findViewById(R.id.btnDOne);
//        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                invokeBE("", "","","");
//
//                if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_QUERY_DRIVER) ) {
//                    navigateToHomeForm();
//                } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_ISSUE_FINE) )  {
//                    navigateToVehicle();
//                } else  if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_QUERY_FINE) )  {
//                    getWebserviceResponse();
//                } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_DRIVER) )  {
//                    navigateToHomeForm();
//                }
//            }
//        });


        BottomNavigationView bottomNavigationView;
        if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_DRIVER) ) {
            bottomNavigationView = findViewById(R.id.bottom_navigation);

        } else   {
            bottomNavigationView = findViewById(R.id.bottom_navigation);

        }


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                View focusView = null;
                boolean cancel= false;
                switch (item.getItemId()) {
                    case R.id.action_recents:
                        DriverSummaryActivity.super.onBackPressed();

                        break;
//                    case R.id.action_favorites:
//                        Toast.makeText(ChargeCodeActivity.this, "Favorites", Toast.LENGTH_SHORT).show();
//                        break;
                    case R.id.action_nearby:
                        if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_QUERY_DRIVER) ) {
                            navigateToHomeForm();
                        } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_ISSUE_FINE) )  {
                            proceedVehicleOption();
                        } else  if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_QUERY_FINE) )  {
                            getWebserviceResponse();
                        } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_DRIVER) )  {
                            navigateToHomeForm();
                        }


                        break;

                }

                return true;
            }
        });


    }

    public void proceedOption(){
        String message =" How would you like to capture driver details?";
        new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("")
                .setContentText(message)
                .setConfirmText("Scan Licence")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        initDriverScan();
                    }
                })
                .setCancelText("Manual Capture")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        navigateToTableForm();
                    }
                })
                .show();

    }

    public void invokeBE(String idNumber, String regNumber, String licCode, String driverName) {
// Show Progress Dialog
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
//        pDialog.show();
        StringEntity entity = null;
        try {

            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SessionUser", MODE_PRIVATE);
            rwsVo.setUsername(sharedPref.getString("phephaUser",null));

            if ( roadSafetyInspection == null) {
                roadSafetyInspection = new BoRoadSafetyInspection();
            }
            roadSafetyInspection.setDriverSurname( rwsVo.getDrisurname());
            roadSafetyInspection.setDriverName( rwsVo.getDriname() );
            roadSafetyInspection.setIdNumber( rwsVo.getDriidn() );
            roadSafetyInspection.setActionStatus(action);
            roadSafetyInspection.setInfrastructureNumber(rwsVo.getUsername());

            if (gpsLocation != null) {
                double latitude = gpsLocation.getLatitude();
                roadSafetyInspection.setGpsXCoordinates( String.valueOf(latitude));
                roadSafetyInspection.setGpsYCoordinates( String.valueOf(gpsLocation.getLongitude()));
                rwsVo.setGpsXCoordinates(String.valueOf(latitude));
                rwsVo.setGpsYCoordinates(String.valueOf(gpsLocation.getLongitude()));
            } else {
                roadSafetyInspection.setGpsXCoordinates( null);
                roadSafetyInspection.setGpsYCoordinates( null );
            }



            Lmidtypecd lmidtypecd = new Lmidtypecd();

            if (rwsVo.getDrividtypecd().length() < 2 )
                lmidtypecd.setCode( "0" + rwsVo.getDrividtypecd() );
            else
                lmidtypecd.setCode( rwsVo.getDrividtypecd() );


            roadSafetyInspection.setIdTypeCd( lmidtypecd );


            if (addDetails != null && addDetails.length > 2) {
                roadSafetyInspection.setLocationName(addDetails[3]);
                roadSafetyInspection.setAddressData(Arrays.toString(addDetails));
                rwsVo.setLocationName(addDetails[3]);
                rwsVo.setProvice(addDetails[5]);
                rwsVo.setStreet(addDetails[1]);
                rwsVo.setAddressData(Arrays.toString(addDetails));
            } else
                roadSafetyInspection.setLocationName(null);

            //entity = new ByteArrayEntity(new JSONObject().toString().getBytes("UTF-8"));
            Gson gson = new GsonBuilder().create();
            entity = new StringEntity(gson.toJson(roadSafetyInspection));
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        //AsyncHttpClient client = new AsyncHttpClient();

        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        // client.post(getApplicationContext(),"http://10.0.2.2:9090/phepha/svs/userspRws/doLogin", entity,"application/json", new AsyncHttpResponseHandler() {

        client.post(getApplicationContext(), AppConstants.BACK_OFFICE_BO_APP_URL + "/saveRoadInspection", entity, "application/json", new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    // JSON Object
                    pDialog.hide();
                    System.out.println(new String(responseBody));
                    Gson gson = new GsonBuilder().create();
                    ObjectMapper mapper = new ObjectMapper();
                    RoadSafety roadSafetyInspection = mapper.readValue( responseBody , RoadSafety.class);
                    //RoadSafety roadSafetyInspection = gson.fromJson(responseBody, RoadSafety.class);
                    // RdtstrptRwsVo rwsVo = gson.fromJson(responseBody, RdtstrptRwsVo.class);
                    // When the JSON response has status boolean value assigned with true
//                    if (!roadSafetyInspection.getRoadSafetyInspections().isEmpty() ) {
//                        navigateToManualForm(roadSafetyInspection);
//                    }
//                    // Else display error message
//                    else {
//                        showSuccess();
//                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                // pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Service unvailable.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Internal Server Error.", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Could not get connection to BO.", Toast.LENGTH_LONG).show();
                }

                navigateToHomeForm();

            }
        });
    }


    public void getWebserviceResponse( ) {

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        StringEntity entity = null;
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        String appURL = "http://10.10.0.3:8080/backoffice/infringeList/" +rwsVo.getDrividtypecd()
                +"/" + rwsVo.getDriidn() +"/" + rwsVo.getLicCardNo();

        client.get(appURL, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    pDialog.hide();
                    System.out.println(new String(responseBody));
                    Gson gson = new GsonBuilder().create();
                    infringementsGroup = gson.fromJson(responseBody,InfringementsGroup.class);
                    GroupDTO groupDTO = new GroupDTO();
                    groupDTO.setGroupName("Infringement notice");
                    groupDTO.setGroupTotal(infringementsGroup.getInfringements().size()+"");
                    groupDTOS.add(groupDTO);
                    groupDTO = new GroupDTO();
                    groupDTO.setGroupName("Warrant of execution");
                    groupDTO.setGroupTotal(infringementsGroup.getWarrantStatusList().size()+"");
                    groupDTOS.add(groupDTO);
                    groupDTO = new GroupDTO();
                    groupDTO.setGroupName("Enforcement order");
                    groupDTO.setGroupTotal(infringementsGroup.getEnforcementStatusList().size()+"");
                    groupDTOS.add(groupDTO);
                    groupDTO = new GroupDTO();
                    groupDTO.setGroupName("Court case");
                    groupDTO.setGroupTotal(infringementsGroup.getCourtStatusList().size()+"");
                    groupDTOS.add(groupDTO);

                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

                navigateToGroup();

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private void navigateToHomeForm() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }

    private void navigateToVehicle() {
        Intent intent = new Intent(this, VehicleSearch.class);
        intent.putExtra("rdtstrptRwsVo", rwsVo);
        startActivity(intent);
        finish();
    }

    private void navigateToTableForm() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    private void navigateToLocationForm() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);

    }

    private void setDateTimeField() {
        expDateET.setOnClickListener(this);
        expDateET.setOnFocusChangeListener(this);
        expDateET.setImeOptions(EditorInfo.IME_ACTION_DONE);


        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                expDateET.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


    }

    private void populateVo(RdtstrptRwsVo rwsVo) {

        View focusView = null;
        boolean cancel = false;
        String idNumber = idNumberET.getText().toString();
        String driverName = nameET.getText().toString();
        // String licCode = driverLicCodeET.getText().toString();


        if (TextUtils.isEmpty(driverName)) {
            nameET.setError("Please enter driver name");
            focusView = nameET;
            cancel = true;
        }

        if (TextUtils.isEmpty(surnameET.getText().toString())) {
            surnameET.setError("Please enter driver Surname.");
            focusView = surnameET;
            cancel = true;
        }

        if (TextUtils.isEmpty(idNumber)) {
            idNumberET.setError("Driver Id Number Required");
            focusView = idNumberET;
            cancel = true;
        } else {
            if (!Validator.isIdDocNValidLength( dlIdType.trim() , idNumber )) {
                idNumberET.setError("This type of identification number requires 13 characters.");
                focusView = idNumberET;
                focusView.requestFocus();
                return;
            }

            if (!Validator.isCheckDigitValid( dlIdType.trim() , idNumber )) {
                idNumberET.setError("Identification number invalid.");
                focusView = idNumberET;
                focusView.requestFocus();
                return;
            }

        }


        if (TextUtils.isEmpty(licCode) || licCode.trim().equalsIgnoreCase("Select DL Code")) {
            Toast.makeText(DriverSummaryActivity.this,
                    "Please select DL Code.!!", Toast.LENGTH_LONG)
                    .show();
            return;
        }



        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            rwsVo.setMvreg(discVo != null? discVo.getMvRegN(): regNumber);
            rwsVo.setDriname(driverName);


            //proceedOption();
            invokeWS(idNumber, regNumber, licCode, driverName);

        }
    }

    private void navigateToGroup() {
        Intent intent = new Intent(this, InfringeGroupActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("groupDTOS", (ArrayList<? extends Parcelable>) groupDTOS);
        Gson gson = new GsonBuilder().create();
        bundle.putString("infringementsGroup",gson.toJson(infringementsGroup));
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void proceedVehicleOption(){
        String message =" How would you like to proceed?";
        new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("")
                .setContentText(message)
                .setConfirmText("Scan Disk")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        initDriverScan();
                    }
                })
                .setCancelText("Manual Capture")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        navigateToVehicle();
//                        navigateToVehicleDetailsForm(rwsVo);
                    }
                })
                .show();

    }

    private void navigateToVehicleDetailsForm(RdtstrptRwsVo vo) {
        Intent intent = new Intent(this, VehicleSearch.class);
        intent.putExtra("rdtstrptRwsVo", vo);
        startActivity(intent);
    }



    private void navigateToManualForm(RoadSafety vo) {
        Intent intent = new Intent(this, HistoryActivity.class);
        intent.putExtra("inspectionDataList", vo);
        startActivity(intent);



    }

    public void invokeWS(String idNumber, String regNumber, String licCode, String driverName) {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();
        StringEntity entity = null;
        try {

            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SessionUser", MODE_PRIVATE);
            rwsVo.setUsername(sharedPref.getString("phephaUser",null));

            if ( roadSafetyInspection == null) {
                roadSafetyInspection = new BoRoadSafetyInspection();
            }
            roadSafetyInspection.setDriverSurname( rwsVo.getDrisurname());
            roadSafetyInspection.setDriverName( rwsVo.getDriname() );
            roadSafetyInspection.setIdNumber( rwsVo.getDriidn() );
            roadSafetyInspection.setActionStatus(action);
            roadSafetyInspection.setInfrastructureNumber(rwsVo.getUsername());

            if ( gpsLocation != null) {
                double latitude = gpsLocation.getLatitude();
                roadSafetyInspection.setGpsXCoordinates( String.valueOf(latitude));
                roadSafetyInspection.setGpsYCoordinates( String.valueOf(gpsLocation.getLongitude()));


            } else {
                roadSafetyInspection.setGpsXCoordinates(null);
                roadSafetyInspection.setGpsYCoordinates( null);

            }

            if (addDetails != null ) {
                roadSafetyInspection.setLocationName(addDetails[3]);
                roadSafetyInspection.setAddressData(Arrays.toString(addDetails));
            } else {
                roadSafetyInspection.setLocationName(null);
                roadSafetyInspection.setAddressData(null);
            }


            Lmidtypecd lmidtypecd = new Lmidtypecd();
            lmidtypecd.setCode( rwsVo.getDrividtypecd() );


            roadSafetyInspection.setIdTypeCd( lmidtypecd );

            //entity = new ByteArrayEntity(new JSONObject().toString().getBytes("UTF-8"));
            Gson gson = new GsonBuilder().create();
            entity = new StringEntity(gson.toJson(roadSafetyInspection));
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        //AsyncHttpClient client = new AsyncHttpClient();

        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        // client.post(getApplicationContext(),"http://10.0.2.2:9090/phepha/svs/userspRws/doLogin", entity,"application/json", new AsyncHttpResponseHandler() {

        client.post(getApplicationContext(), AppConstants.BACK_OFFICE_BO_APP_URL + "/saveRoadInspection", entity, "application/json", new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    // JSON Object
                    pDialog.hide();
                    System.out.println(new String(responseBody));
                    Gson gson = new GsonBuilder().create();
                    ObjectMapper mapper = new ObjectMapper();
                    RoadSafety roadSafetyInspection = mapper.readValue( responseBody , RoadSafety.class);

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to BO.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    //Toast.makeText(getApplicationContext(), "Could not get connection to BO.", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void  showSuccess(){

        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Road Test Report")
                .setContentText("Road Test Results Added Successfully")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        navigateToHomeForm();
                    }
                })
                .show();


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        System.out.print(parent.getItemAtPosition(position));
        licCode = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public Location getLocation(){

        if(requestLocationPerm()) {
            gpsLocation = appLocationService.getLocation(LocationManager.GPS_PROVIDER, getApplicationContext());
            if (gpsLocation != null) {
                double latitude = gpsLocation.getLatitude();
                double longitude = gpsLocation.getLongitude();
                String result = "Latitude: " + gpsLocation.getLatitude() +
                        " Longitude: " + gpsLocation.getLongitude();
                LocationAddress.getAddressFromLocation(latitude,longitude,getApplicationContext(),new GeocoderHandler());
            } else {
                // showSettingsAlert();
            }
        }

        return gpsLocation;

    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    addDetails =  locationAddress.split("\n");
                    if (addDetails != null && addDetails.length > 2) {
                    rwsVo.setLocationName(addDetails[3]);
                    rwsVo.setProvice(addDetails[5]);
                    rwsVo.setStreet(addDetails[1]);
                    rwsVo.setAddressData(Arrays.toString(addDetails));
                }

                    break;
                default:
                    locationAddress = null;
            }
            // tvAddress.setText(locationAddress);
        }
    }

    @Override
    public void onBackPressed() {
    }

    public void onPreviousPressed() {
        new SessionManager(getApplicationContext()).SavePreferences(true);
        super.onBackPressed();
        return;
    }

    @Override
    public void onClick(View v) {

        if(v == expDateET) {
            fromDatePickerDialog.show();
        }
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(v == expDateET && hasFocus) {
            fromDatePickerDialog.show();
        }
    }

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private boolean requestLocationPerm() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission = checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showMessageOKCancel("You need to allow access to Location",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                                REQUEST_CODE_ASK_PERMISSIONS);
                                    }
                                }
                            });
                }
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
        // insertDummyContact();
        return false;
    }


    /// final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private boolean requestPhoneStatePerm() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission = checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.READ_PHONE_STATE)) {
                    showMessageOKCancel("You need to allow access to phone state",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                                                REQUEST_CODE_ASK_PERMISSIONS);
                                    }
                                }
                            });
                }
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
        // insertDummyContact();
        return false;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(DriverSummaryActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    getLocation();
                } else {
                    // Permission Denied
                    Toast.makeText(DriverSummaryActivity.this, "LOCATION ACCESS Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void showSettingsAlert() {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Location Service")
                .setContentText("Unable to determine your location : Please ensure your GPS is turned on")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        DriverSummaryActivity.this.startActivity(intent);
                    }
                })
                .show();

    }

    private void initDriverScan() {
        // TODO Auto-generated method stub
        //if(type == 3)
        mScanManager.stopDecode();
        isScaning = true;
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        mScanManager.startDecode();

    }


// Scanning code

    private BroadcastReceiver mScanReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            isScaning = false;
            soundpool.play(soundid, 1, 1, 0, 0, 1);
            //showScanResult.setText("");
            mVibrator.vibrate(100);

            byte[] barcode = intent.getByteArrayExtra(ScanManager.DECODE_DATA_TAG);
            int barcodelen = intent.getIntExtra(ScanManager.BARCODE_LENGTH_TAG, 0);
            byte temp = intent.getByteExtra(ScanManager.BARCODE_TYPE_TAG, (byte) 0);
            android.util.Log.i("debug", "----codetype--" + temp);
            barcodeStr = new String(barcode, 0, barcodelen);
//            showScanResult.append(" length："  +barcodelen);
//            showScanResult.append(" barcode："  +barcodeStr);
//            showScanResult.append(" barcode："  +bytesToHexString(barcode));
            //showScanResult.setText(barcodeStr);
            menuCd = session.getMenuCd();

            if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_VEHICLE) ||
                    menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_ISSUE_FINE)) {
                //TODO: Capture Result and open new view to display results
                final MvLicDiscVo vo = readLicDiskBarCode(barcodeStr);
                System.out.println("Registration Number = " + vo.getMvRegN());
                checkEnatisForVehicle(vo);
            } else{

                    Toast.makeText(getApplicationContext(), "Barcode Not getting.", Toast.LENGTH_LONG).show();

            }



        }

    };

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    private void initScan() {
        // TODO Auto-generated method stub
        mScanManager = new ScanManager();
        mScanManager.openScanner();

        mScanManager.switchOutputMode( 0);
        soundpool = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 100); // MODE_RINGTONE
        soundid = soundpool.load("/etc/Scan_new.ogg", 1);
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if(mScanManager != null) {
            mScanManager.stopDecode();
            isScaning = false;
        }
        unregisterReceiver(mScanReceiver);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        initScan();
//        showScanResult.setText("");
        IntentFilter filter = new IntentFilter();
        int[] idbuf = new int[]{PropertyID.WEDGE_INTENT_ACTION_NAME, PropertyID.WEDGE_INTENT_DATA_STRING_TAG};
        String[] value_buf = mScanManager.getParameterString(idbuf);
        if(value_buf != null && value_buf[0] != null && !value_buf[0].equals("")) {
            filter.addAction(value_buf[0]);
        } else {
            filter.addAction(SCAN_ACTION);
        }

        registerReceiver(mScanReceiver, filter);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.activity_main, menu);
        MenuItem settings = menu.add(0, 1, 0, R.string.menu_settings).setIcon(R.drawable.ic_home_black_24dp);
        // 绑定到actionbar
        //SHOW_AS_ACTION_IF_ROOM 显示此项目在动作栏按钮如果系统决定有它。 可以用1来代替
        MenuItem version = menu.add(0, 2, 0, R.string.menu_settings);
        settings.setShowAsAction(1);
        version.setShowAsAction(0);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case 1:
                try{
                    Intent intent = new Intent("android.intent.action.SCANNER_SETTINGS");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                break;
            case 2:
                PackageManager pk = getPackageManager();
                PackageInfo pi;
                try {
                    pi = pk.getPackageInfo(getPackageName(), 0);
                    Toast.makeText(this, "V" +pi.versionName , Toast.LENGTH_SHORT).show();
                } catch (PackageManager.NameNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void navigateToManualForm(RdtstrptRwsVo vo ) {
        Intent intent = new Intent(this, DriverSummaryActivity.class);
        intent.putExtra("rdtstrptRwsVo", vo);
        intent.putExtra("inspectionData", roadSafetyInspection);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void navigateToHome() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }

    private void  showFail(final RdtstrptRwsVo rwsVo){

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("NTP 733 Road Test Report")
                .setContentText("Driver not found on eNatis")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        navigateToManualForm(rwsVo);
                    }
                })
                .show();


    }

    public MvLicDiscVo readLicDiskBarCode(String barCdString) {
        MvLicDiscVo mvLicDiscVo = new MvLicDiscVo();
        try {

            String delims = "[%]+";
            String[] tokens = barCdString.split( delims );

            for ( int i = 0; i < tokens.length; i++ ) {
                // System.out.println( tokens[ i ] );

                switch ( i ) {
                    case 0:
                        break;
                    case 1:
                        mvLicDiscVo.setCertN( tokens[ i ] );
                        break;
                    case 2:
                        //mvLicDiscVo.setCertN( tokens[ i ] );
                        break;
                    case 3:
                        mvLicDiscVo.setUsernumber( tokens[ i ] );
                        break;

                    case 4:
                        //mvLicDiscVo.setUsernumber( tokens[ i ] );
                        break;

                    case 5:
                        mvLicDiscVo.setDocCtrlN( tokens[ i ] );
                        break;
                    case 6:
                        mvLicDiscVo.setMvRegN( tokens[ i ] );
                        break;

                    case 7:
                        mvLicDiscVo.setRegtN( tokens[ i ] );
                        break;

                    case 8:
                        mvLicDiscVo.setMvDescCdDesc( tokens[ i ] );
                        break;

                    case 9:
                        mvLicDiscVo.setMake( tokens[ i ] );
                        break;
                    case 10:
                        mvLicDiscVo.setModel( tokens[ i ] );
                        break;
                    case 11:
                        mvLicDiscVo.setColor( tokens[ i ] );
                        break;
                    case 12:
                        mvLicDiscVo.setVin( tokens[ i ] );
                        break;

                    case 13:
                        mvLicDiscVo.setEngineNo( tokens[ i ] );
                        break;
                    case 14:
                        mvLicDiscVo.setExpiry( tokens[ i ] );
                        break;
                }
            }

        } catch ( Exception e ) {
            e.printStackTrace();;
            System.out.println( "Exception ----" );
        }
        return mvLicDiscVo;
    }

    public void checkEnatisForVehicle(final MvLicDiscVo mvLicDiscVo ) {

        StringEntity entity = null;
        //AsyncHttpClient client = new AsyncHttpClient();
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        String appURL = AppConstants.BACK_OFFICE_APP_URL+ "v1/enatis/X1001Vehicle/" + mvLicDiscVo.getMvRegN();


        client.get(appURL, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    // JSON Object
//                    pDialog.hide();
                    System.out.println(new String(responseBody));
                    Gson gson = new GsonBuilder().create();
                    vehicle = gson.fromJson(responseBody, Vehicle.class);
                    // When the JSON response has status boolean value assigned with true

                    if ( responseBody.isEmpty() ) {
                        mvLicDiscVo.setEnatisNoData(true);
                        mvLicDiscVo.setMvDescCdDesc( null );
                        Toast.makeText(getApplicationContext(), "Data not found on eNaTIS..!", Toast.LENGTH_LONG).show();
//                        showFail( mvLicDiscVo );
                    } else {
                        ObjectMapper mapper = new ObjectMapper();
                        vehicle = mapper.readValue( responseBody , Vehicle.class);
                        mvLicDiscVo.setMvDescCdDesc(  vehicle.getVehicleDet().getMvcat().getDesc());
                        mvLicDiscVo.setMvRegN(vehicle.getVehicleDet().getLicN());
                        mvLicDiscVo.setRwStatus(vehicle.getVehicleDet().getRwstat().getDesc());
                        mvLicDiscVo.setSapMark(vehicle.getVehicleDet().getSapmark().getDesc());
                        mvLicDiscVo.setColor(vehicle.getVehicleDet().getMainColour().getDesc());
                        mvLicDiscVo.setVin(vehicle.getVehicleDet().getVinOrChassis());
                        mvLicDiscVo.setMake(vehicle.getVehicleDet().getMake().getDesc());
                        mvLicDiscVo.setModel(vehicle.getVehicleDet().getModelName().getDesc());


                        mvLicDiscVo.setExpiry(vehicle.getVehicleDet().getMvlicExpiryD());
                        mvLicDiscVo.setMvState(vehicle.getVehicleDet().getMvstate().getDesc());

                        mvLicDiscVo.setMvDescCdDesc( vehicle.getVehicleDet().getMvcat().getDesc() );
                        mvLicDiscVo.setRwStatus(vehicle.getVehicleDet().getRwstat().getDesc());
                        mvLicDiscVo.setSapMark(vehicle.getVehicleDet().getSapmark().getDesc());

                        if ( !vehicle.getVehicleDet().getEngineN().equalsIgnoreCase(mvLicDiscVo.getEngineNo()) ||
                                !vehicle.getVehicleDet().getVinOrChassis().equalsIgnoreCase(mvLicDiscVo.getVin())  ||
                                !vehicle.getVehicleDet().getMake().getDesc().equalsIgnoreCase(mvLicDiscVo.getMake())) {
                            mvLicDiscVo.setEnatisNoMatch(false);

                        }

                        //set the owner details for display in the next page
                        if ( vehicle.getOwner() != null) {

                            mvLicDiscVo.setBusOrSurname(vehicle.getOwner().getPerDet().getPerId().getBusOrSurname());
                            mvLicDiscVo.setIdDocN(vehicle.getOwner().getPerDet().getPerId().getIdDocN());
                            mvLicDiscVo.setInitials(vehicle.getOwner().getPerDet().getPerId().getInitials());
                            mvLicDiscVo.setDesc(vehicle.getOwner().getPerDet().getPerId().getIdDocType().getDesc());

                            mvLicDiscVo.setAddr1(vehicle.getOwner().getPerDet().getStreetAddr().getAddr1());
                            mvLicDiscVo.setAddr2(vehicle.getOwner().getPerDet().getStreetAddr().getAddr2());
                            mvLicDiscVo.setAddr3(vehicle.getOwner().getPerDet().getStreetAddr().getAddr3());
                            mvLicDiscVo.setPostalCd(vehicle.getOwner().getPerDet().getStreetAddr().getPostalCd().getCode());
                        }

                        navigatetoReportActivity( mvLicDiscVo );
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }


            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
//                pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }

                mvLicDiscVo.setMvDescCdDesc( null );
                navigatetoReportActivity(mvLicDiscVo);
            }
        });

    }

    public void navigatetoReportActivity(MvLicDiscVo discVo){

        Intent intent = new Intent(this, VehicleSearchResultsActivity.class);
        intent.putExtra("rdtstrptRwsVo", rwsVo);
        intent.putExtra("discVo",discVo);
        startActivity(intent);
    }


}
