package co.za.ngwane.vdv;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.rtmc.dto.VehicleDet;
import co.za.rtmc.enums.AppConstants;
import co.za.rtmc.service.LocationAddress;
import co.za.rtmc.service.SessionManager;
import co.za.rtmc.service.Validator;
import co.za.rtmc.vo.AppLocationService;
import co.za.rtmc.vo.BoRoadSafetyInspection;
import co.za.rtmc.vo.MvLicDiscVo;
import co.za.rtmc.vo.RdtstrptRwsVo;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

/**
 * Created by Jama on 2016/06/09.
 */
public class VehicleSearchResultsActivity extends MainActivity implements View.OnClickListener, View.OnFocusChangeListener {
    AppLocationService appLocationService;
    Location gpsLocation;
    String[] addDetails;
    EditText regNumberET;
    EditText colorET;
    EditText vinET;
    EditText makeModelET;
    EditText modelET;
    EditText rwStatET;
    EditText sapMarkET;

    EditText mvTypeET;
    EditText mvLicDateET;
    EditText mvStateET;
    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    String mvType = "";
    SweetAlertDialog pDialog;
    private CoordinatorLayout coordinatorLayout;
    MvLicDiscVo mvLicDiscVo = new MvLicDiscVo();
    BoRoadSafetyInspection roadSafetyInspection = new BoRoadSafetyInspection();
    private VehicleDet vehicle = null;
    SessionManager session;
    private String menuCd;
    MvLicDiscVo discVo = null;

    private RdtstrptRwsVo rwsVo = new RdtstrptRwsVo();
    static final String[] MV_CAT = new String[] {"Select MV Type",
            AppConstants.MV_CAT_LIGHT_PASSENGER_MV_DESC,
            AppConstants.MV_CAT_HEAVY_PASSENGER_MV_DESC,
            AppConstants.MV_CAT_LIGHT_LOAD_VEH_NOT_DRAW_DESC,
            AppConstants.MV_CAT_HEAVY_LOAD_DRAW_VEHICLE_DESC,
            AppConstants.MV_CAT_HEAVY_LOAD_VEH_NOT_DRAW_DESC,
            AppConstants.MV_CAT_MOTORCYCLE_DESC,
            AppConstants.MV_CAT_SPECIAL_VEHICLE_DESC
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        session = new SessionManager(getApplicationContext());

        appLocationService = new AppLocationService( VehicleSearchResultsActivity.this);
        menuCd = session.getMenuCd();

        if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_VEHICLE ) ) {
            setContentView(R.layout.vehicledetails_search_result_verify);
        } else  {
            setContentView(R.layout.vehicledetails_search_result);
        }

        Intent intent = getIntent();
        gpsLocation = getLocation();

        rwsVo = (RdtstrptRwsVo) intent.getSerializableExtra("rdtstrptRwsVo");
        discVo  = (MvLicDiscVo) intent.getSerializableExtra("discVo");

        if ( discVo!= null && discVo.isEnatisNoData()) {
            showNoData();
        }

        if (discVo!= null && discVo.isEnatisNoMatch()) {
            showWarning();
        }

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        mvLicDateET = (EditText) findViewById(R.id.mvExpDate);
        mvLicDateET.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        // setDateTimeField();

        regNumberET = (EditText) findViewById(R.id.mvRegNumET);
        mvStateET = (EditText) findViewById(R.id.mvState);
        mvStateET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});
        regNumberET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(10)});


        colorET = (EditText) findViewById(R.id.mvColorET);
        colorET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(15)});

        vinET = (EditText) findViewById(R.id.mvVinET);
        vinET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(17)});

        makeModelET = (EditText) findViewById(R.id.mvMmET);
        makeModelET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});

        modelET = (EditText) findViewById(R.id.mvModel);
        modelET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});
//        mvType = (EditText) findViewById(R.id.mvTypeET);
        mvLicDateET = (EditText) findViewById(R.id.mvExpDate);

        rwStatET = (EditText) findViewById(R.id.mvRwStat);
        rwStatET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});
        sapMarkET = (EditText) findViewById(R.id.mvSapMark);
        sapMarkET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});

        try {
            regNumberET.setText(discVo.getMvRegN());
            regNumberET.setEnabled(false);
            colorET.setText(discVo.getColor());

            if (discVo.getColor() != null && !discVo.getColor().isEmpty()) {
                colorET.setEnabled(false);
            }

            vinET.setText(discVo.getVin());
            if (discVo.getVin() != null && !discVo.getVin().isEmpty()) {
                vinET.setEnabled(false);
            }

            makeModelET.setText(discVo.getMake());
            if (discVo.getMake() != null && !discVo.getMake().isEmpty()) {
                makeModelET.setEnabled(false);
            }
            modelET.setText(discVo.getModel());
            if (discVo.getModel() != null && !discVo.getModel().isEmpty()) {
                modelET.setEnabled(false);
            }
            mvType = discVo.getMvDescCdDesc();
            // mvLicDateET.setText(discVo.getExpiry());
            if (discVo.getExpiry() != null && !discVo.getExpiry().isEmpty()) {
                mvLicDateET.setEnabled(false);
            }
            mvStateET.setText(discVo.getMvState());
            mvStateET.setEnabled(false);

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date targetD = null;
            Date date2 = null;
            String calculatedD =null;
            // if(!discVo.isScanned()) {

            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(new Long(discVo.getExpiry()));
            System.out.println(c.get(Calendar.YEAR));
            System.out.println(c.get(Calendar.MONTH));
            System.out.println(c.get(Calendar.DAY_OF_MONTH));

            String month = String.valueOf(c.get(Calendar.MONTH) + 1);
            if (month.length() == 1) {
                month = "0" + month;
            }

            calculatedD = c.get(Calendar.YEAR) + "-" + month + "-" + c.get(Calendar.DAY_OF_MONTH);
            mvLicDateET.setText(calculatedD);

            targetD = sdf.parse(calculatedD);
            System.out.println(c.get(Calendar.DAY_OF_MONTH));

            sapMarkET.setText(discVo.getSapMark());
            sapMarkET.setEnabled(false);
            rwStatET.setText(discVo.getRwStatus());
            rwStatET.setEnabled(false);

            if(discVo.getSapMark().equalsIgnoreCase(AppConstants.NONE)){
                sapMarkET.setBackgroundColor(Color.GREEN);
            }else{
                sapMarkET.setBackgroundColor(Color.RED);
                sapMarkET.setTypeface(sapMarkET.getTypeface(), Typeface.BOLD);
            }

            if(discVo.getRwStatus().equalsIgnoreCase(AppConstants.ROAD_WORTHY)){
                rwStatET.setBackgroundColor(Color.GREEN);
            }else{
                rwStatET.setBackgroundColor(Color.RED);
                rwStatET.setTypeface(rwStatET.getTypeface(), Typeface.BOLD);
            }


            if ( Validator.isDateOutOfRange(targetD, new Date(), Calendar.MONTH, 3 ) ) {
                mvLicDateET.setBackgroundColor(Color.GREEN);
            } else if (  Validator.isDateOutOfRange(targetD, new Date(), Calendar.MONTH, 2 )) {
                mvLicDateET.setBackgroundColor(Color.YELLOW);
            } else if ( targetD.compareTo(new Date()) < 0 ) {
                mvLicDateET.setBackgroundColor( Color.RED );
                mvLicDateET.setTypeface(mvLicDateET.getTypeface(), Typeface.BOLD);
            }

            mvLicDateET.setEnabled(false);

        }catch (Exception e){
            e.printStackTrace();

        }



        final Spinner spinner = (Spinner) findViewById(R.id.mvTypeSpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
/*        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.mvType_array, android.R.layout.simple_spinner_item);*/

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, MV_CAT);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(

                            AdapterView<?> parent, View view, int position, long id) {
                        if ( discVo != null && (discVo.getMvDescCdDesc() != null )) {
                            int spinnerPosition = adapter.getPosition(discVo.getMvDescCdDesc());
                            spinner.setSelection(spinnerPosition);

                            if (discVo.getMvDescCdDesc() != null) {
                                spinner.setEnabled(false);
                            }
                        }
                        System.out.print(parent.getItemAtPosition(position));
                        mvType = parent.getItemAtPosition(position).toString();
                        // showToast("Spinner1: position=" + position + " id=" + id);

                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        // showToast("Spinner1: unselected");
                    }
                });


        Button mEmailSignInButton = findViewById(R.id.btnVDContinue);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_QUERY_DRIVER ) ) {
                    navigateToHomeForm();
                } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_ISSUE_FINE) )  {
                    navigateToCharge();
                } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_VEHICLE) )  {
                    navigateToManualForm(discVo);
                } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_DRIVER) )  {
                    navigateToHomeForm();
                }

////                invokeWS(discVo);
//                navigateToManualForm(discVo);
//  //              navigateToHomeForm();
            }
        });

        BottomNavigationView bottomNavigationView;
        if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_VEHICLE) ) {
            bottomNavigationView = findViewById(R.id.bottom_navigation);

        } else   {
            bottomNavigationView = findViewById(R.id.bottom_navigation);

        }

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                View focusView = null;
                boolean cancel= false;
                switch (item.getItemId()) {
                    case R.id.action_recents:
                        VehicleSearchResultsActivity.super.onBackPressed();

                        break;
//                    case R.id.action_favorites:
//                        Toast.makeText(ChargeCodeActivity.this, "Favorites", Toast.LENGTH_SHORT).show();
//                        break;
                    case R.id.action_nearby:
                        if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_QUERY_DRIVER ) ) {
                            navigateToHomeForm();
                        } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_ISSUE_FINE) )  {
                            navigateToCharge();
                        } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_VEHICLE) )  {
                            navigateToManualForm(discVo);
                        } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_DRIVER) )  {
                            navigateToHomeForm();
                        }


                        break;

                }

                return true;
            }
        });




    }

    private void navigateToCharge() {

        Intent intent = new Intent(this,ChargeCodeActivity.class);
        intent.putExtra("discVo", discVo);
        intent.putExtra("rdtstrptRwsVo", rwsVo);
        startActivity(intent);

    }

    private void  showNoData(){

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("NTP 733 Road Test Report")
                .setContentText("Vehicle not found on eNatis")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();


    }

    public void showWarning( ) {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("NTP 733 Road Test Report")
                .setContentText("Vehicle details do not match with the details on eNatis")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();
    }


    private void navigateToManualForm(MvLicDiscVo vo) {
        Intent intent = new Intent(this, ChargeCodeActivity.class);
        intent.putExtra("discVo", vo);
        startActivity(intent);

    }

    private void  showFail(){

        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("NTP 733 Road Test Report")
                .setContentText("Please select Motor Vehicle type.")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();


    }

    public Location getLocation(){

        if(requestLocationPerm()) {
            gpsLocation = appLocationService.getLocation(LocationManager.GPS_PROVIDER, getApplicationContext());
            if (gpsLocation != null) {
                double latitude = gpsLocation.getLatitude();
                double longitude = gpsLocation.getLongitude();
                String result = "Latitude: " + gpsLocation.getLatitude() +
                        " Longitude: " + gpsLocation.getLongitude();
                LocationAddress.getAddressFromLocation(latitude,longitude,getApplicationContext(), new GeocoderHandler());
            } else {
                // showSettingsAlert();
            }
        }

        return gpsLocation;

    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    addDetails =  locationAddress.split("\n");
                    //String prov= addDetails[9];
                    //setProvinceCD(prov);

                    break;
                default:
                    locationAddress = null;
            }
            // tvAddress.setText(locationAddress);
        }
    }

    private boolean requestLocationPerm() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission = checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showMessageOKCancel("You need to allow access to Location",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                                REQUEST_CODE_ASK_PERMISSIONS);
                                    }
                                }
                            });
                }
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
        // insertDummyContact();
        return false;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(VehicleSearchResultsActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    getLocation();
                } else {
                    // Permission Denied
                    Toast.makeText(VehicleSearchResultsActivity.this, "LOCATION ACCESS Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


      @Override
    public void onClick(View v) {

        if(v == mvLicDateET) {
            fromDatePickerDialog.show();
        }
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(v == mvLicDateET && hasFocus) {
            fromDatePickerDialog.show();
        }
    }

    //private method of your class
    private int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }

    public void onPreviousPressed() {
        super.onBackPressed();
        return;
    }

    @Override
    public void onBackPressed() {
    }


      private void navigateToHomeForm() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }


    private void navigateToTableForm() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    private void navigateToLocationForm() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);

    }


}
