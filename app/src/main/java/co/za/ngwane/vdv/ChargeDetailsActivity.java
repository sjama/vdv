package co.za.ngwane.vdv;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Date;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.ngwane.vdv.andoirdbluetoothprint.UnicodeFormatter;
import co.za.ngwane.vdv.dto.InfringementDto;
import co.za.rtmc.service.Validator;
import co.za.rtmc.vo.ChargeDetails;
import co.za.rtmc.vo.MvLicDiscVo;
import co.za.rtmc.vo.RdtstrptRwsVo;

public class ChargeDetailsActivity extends AppCompatActivity {


    private EditText mChargeCodeView;
    private EditText mChargeDetailsView;
    private EditText mChargeDiscountedView;
    private EditText mChargeePenaltyView;
    private ChargeDetails chargeDetails;
    private MvLicDiscVo discVo = new MvLicDiscVo();
    private RdtstrptRwsVo rwsVo = new RdtstrptRwsVo();

    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charge_details);

        Gson gson = new GsonBuilder().create();
        chargeDetails = gson.fromJson(this.getIntent().getExtras().getString("chargeDetails"), ChargeDetails.class);

        Intent intent = getIntent();

        discVo = (MvLicDiscVo) intent.getSerializableExtra("discVo");
        rwsVo = (RdtstrptRwsVo) intent.getSerializableExtra("rdtstrptRwsVo");

        mChargeCodeView = (EditText) findViewById(R.id.charge_codeET);
        mChargeCodeView.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(5)});
        mChargeCodeView.setTextColor(Color.BLACK);
        mChargeCodeView.setEnabled(false);
        mChargeCodeView.setText(chargeDetails.getLtcnt().getCntcd());

        mChargeDetailsView = (EditText) findViewById(R.id.charge_detailsET);
        mChargeDetailsView.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(200)});
        mChargeDetailsView.setTextColor(Color.BLACK);
        mChargeDetailsView.setEnabled(false);
        mChargeDetailsView.setText(chargeDetails.getLtcnt().getCntshort());
        mChargeDetailsView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });

        mChargeDiscountedView = (EditText) findViewById(R.id.charge_disc_amountET);
        mChargeDiscountedView.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(100)});
        mChargeDiscountedView.setTextColor(Color.BLACK);
        mChargeDiscountedView.setEnabled(false);
        mChargeDiscountedView.setText("R " + String.valueOf(chargeDetails.getLtcntfees().getCntdiscountamt()));

        mChargeePenaltyView = (EditText) findViewById(R.id.charge_pen_amountET);
        mChargeePenaltyView.setFilters(new InputFilter[]{new InputFilter.AllCaps(), new InputFilter.LengthFilter(100)});
        mChargeePenaltyView.setTextColor(Color.BLACK);
        mChargeePenaltyView.setEnabled(false);
        mChargeePenaltyView.setText("R " + String.valueOf(chargeDetails.getLtcntfees().getCntpenaltyamt()));


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //Ok I will speak to the guys on how we finalise this...


                switch (item.getItemId()) {
                    case R.id.action_recents:
                        ChargeDetailsActivity.super.onBackPressed();

                        break;
                    case R.id.action_nearby:
                        try {
                            findBT();
                            if(isConnected()){
                                beginListenForData();
                                sendData();
                                closeBT();
                                navigateToMenuActivity();
                            }else {
                                showConnectionErrorAlert();
                            }

                        } catch (Exception e) {

                        }
                        break;

                }
                return true;
            }
        });

    }

    void findBT() {

        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (mBluetoothAdapter == null) {
                // myLabel.setText("No bluetooth adapter available");
            }

            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }

            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {

                    // RPP300 is the name of the bluetooth printer device
                    // we got this name from the list of paired devices
                    if (!device.getName().equals("HP OfficeJet 250")) {
                        mmDevice = device;
                        break;
                    }
                }
            }

            // myLabel.setText("Bluetooth device found.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * after opening a connection to bluetooth printer device,
     * we have to listen and check if a data were sent to be printed.
     */
    void beginListenForData() {
        try {
            final Handler handler = new Handler();

            // this is the ASCII code for a newline character
            final byte delimiter = 10;

            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];

            workerThread = new Thread(new Runnable() {
                public void run() {

                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {

                        try {

                            int bytesAvailable = mmInputStream.available();

                            if (bytesAvailable > 0) {

                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);

                                for (int i = 0; i < bytesAvailable; i++) {

                                    byte b = packetBytes[i];
                                    if (b == delimiter) {

                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );

                                        // specify US-ASCII encoding
                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                        // tell the user data were sent to bluetooth printer device
                                        handler.post(new Runnable() {
                                            public void run() {
                                                //myLabel.setText(data);
                                            }
                                        });

                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }

                    }
                }
            });

            workerThread.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // close the connection to bluetooth printer.
    void closeBT() throws IOException {
        try {
            stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
            //myLabel.setText("Bluetooth Closed");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    boolean isConnected() throws IOException {
        try {

            try {
                closeBT();
                UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
                mBluetoothAdapter.cancelDiscovery();
                //UUID uuide = UUID.randomUUID();
                mmSocket = true ?
                        mmDevice.createRfcommSocketToServiceRecord(uuid) :
                        mmDevice.createInsecureRfcommSocketToServiceRecord(uuid);
                mmSocket.connect();

                mmOutputStream = mmSocket.getOutputStream();
                mmInputStream = mmSocket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
               return  false;
            }



            //myLabel.setText("Bluetooth Opened");

        } catch (Exception e) {
            ChargeDetailsActivity.ConnectThread connectThread = new ChargeDetailsActivity.ConnectThread(mmDevice);
            connectThread.run();
            e.printStackTrace();
        }
        return true;
    }

    public int gen() {
        Random r = new Random( System.currentTimeMillis() );
        return ((1 + r.nextInt(2)) * 10000 + r.nextInt(10000));
    }

    // this will send text data to be printed by the bluetooth printer
    void sendData() throws IOException {
        try {
            InfringementDto infringementDto = new InfringementDto();

            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SessionUser", MODE_PRIVATE);
            rwsVo.setUsername(sharedPref.getString("phephaUser",null));

            if (rwsVo.getDrividtypecd().equalsIgnoreCase("02")) {
                // show password
                infringementDto.setIdType(Validator.RSA_IDENTITY_DOCUMENT);

            } else if (rwsVo.getDrividtypecd().equalsIgnoreCase("03")){
                infringementDto.setIdType(Validator.FOREIGN_IDENTITY_DOCUMENT);


            } else if (rwsVo.getDrividtypecd().equalsIgnoreCase("01")) {
                infringementDto.setIdType(Validator.TRAFFIC_REGISTER_NUMBER);

            }

            infringementDto.setInfringementNoticeNumber("024086121" +  gen());
            infringementDto.setGender(rwsVo.getGender());
            infringementDto.setInitials(rwsVo.getDriname());
            infringementDto.setSurname(rwsVo.getDrisurname());
            infringementDto.setIdNumber(rwsVo.getDriidn());
            infringementDto.setVehicleDescription(discVo.getColor() +" "+discVo.getMake()+" "+discVo.getModel());
            infringementDto.setRegistrationNumber(discVo.getMvRegN());

            infringementDto.setProvice(rwsVo.getProvice());
            infringementDto.setCityTown(rwsVo.getLocationName());
            infringementDto.setStreet(rwsVo.getStreet());
            infringementDto.setRouteNumber("");
            infringementDto.setDate(new Date().toString());
//            infringementDto.setTime(String.valueOf(new Date().getTime()));

            //MAIN CHARGE
            infringementDto.setMainChargeCode(chargeDetails.getLtcnt().getCntcd());
            infringementDto.setMainLegalRef(chargeDetails.getLtcnt().getCntshortsec());
            infringementDto.setMainChargeDescription(chargeDetails.getLtcnt().getCntshort());
            infringementDto.setMainPenaltyAmount(chargeDetails.getLtcntfees().getCntpenaltyamt()+"");
            infringementDto.setMainDiscountAmount(chargeDetails.getLtcntfees().getCntdiscountamt()+"");

//            String discount = chargeDetails.getLtcntfees().getCntdiscountamt()+"";
//            int discountedAmount = new Integer(chargeDetails.getLtcntfees().getCntpenalty()) - new Integer(discount);

            infringementDto.setMainDiscountedAmount(chargeDetails.getLtcntfees().getCntdiscountamt()+"");

            //ALTERNATIVE CHARGE
            infringementDto.setAltChargeCode("");
            infringementDto.setAltLegalRef("");
            infringementDto.setAltChargeDescription("");
            infringementDto.setAltPenaltyAmount("");
            infringementDto.setAltDiscountAmount("");
            infringementDto.setAltDiscountedAmount("");

            //OFFICER DETAILS
            infringementDto.setIssuingAuthority("Johannesburg Metro Police Department");
            infringementDto.setIssuingAuthorityCode("4068");
            infringementDto.setMagistrateDistrict("Johannesburg");
            infringementDto.setMagistrateDistrictCode("9034");
            infringementDto.setOfficerName("Jacob Marola");
            infringementDto.setInfrastructureNumber(rwsVo.getInfn());

            String infringementNoticeNumber = infringementDto.getInfringementNoticeNumber();
            String gender = infringementDto.getGender();
            String name = infringementDto.getSurname() + " " + infringementDto.getInitials();
            String idType = infringementDto.getIdType();
            String idNumber = infringementDto.getIdNumber();
            String vehicleDescription = infringementDto.getVehicleDescription();
            String registrationNumber = infringementDto.getRegistrationNumber();
            String provice = infringementDto.getProvice();
            String cityTown = infringementDto.getCityTown();
            String street = infringementDto.getStreet();
            String routeNumber = infringementDto.getRouteNumber();
            String date = infringementDto.getDate();
            String time = infringementDto.getTime();

            //MAIN CHARGE
            String mainChargeCode = infringementDto.getMainChargeCode();
            String mainLegalRef = infringementDto.getMainLegalRef();
            String mainChargeDescription = infringementDto.getMainChargeDescription();
            String mainPenaltyAmount = infringementDto.getMainPenaltyAmount();
            String mainDiscountAmount = infringementDto.getMainDiscountAmount();
            String mainDiscountedAmount = infringementDto.getMainDiscountedAmount();

            //ALTERNATIVE CHARGE
            String altChargeCode = infringementDto.getAltChargeCode();
            String altLegalRef = infringementDto.getAltLegalRef();
            String altChargeDescription = infringementDto.getAltChargeDescription();
            String altPenaltyAmount = infringementDto.getAltPenaltyAmount();
            String altDiscountAmount = infringementDto.getAltDiscountAmount();
            String altDiscountedAmount = infringementDto.getAltDiscountedAmount();

            //OFFICER DETAILS
            String issuingAuthority = infringementDto.getIssuingAuthority();
            String issuingAuthorityCode = infringementDto.getIssuingAuthorityCode();
            String magistrateDistrict = infringementDto.getMagistrateDistrict();
            String magistrateDistrictCode = infringementDto.getMagistrateDistrictCode();
            String officerName = infringementDto.getOfficerName();
            String infrastructureNumber = rwsVo.getUsername();

            String altCharge = " ";

            if (!altChargeCode.isEmpty()) {
                altCharge = "                        ALTERNATIVE CHARGE \n"
                        + "Charge Code       :" + altChargeCode + "       Legal Ref:" + altLegalRef + "\n"
                        + "Charge Description:" + altChargeDescription + " \n"
                        + "Penalty Amount    :" + altPenaltyAmount + "\n"
                        + "Discount Amount   :" + altDiscountAmount + "    Discounted Amount:" + altDiscountedAmount + "\n"
                        + "\n";
            }

            String footer = "                 ENQUIRIES AND INFROMATION"
                    + "\nAll enquiries regarding this Infringement Notice or the provisions of the AARTO Act may be made:\n"
                    + "1. By post, to: AARTO Enquires, Private Bag X112, Halfway House, 1685\n"
                    + "2. By Telephone: 086 122 7861 (0861AARTO1)\n"
                    + "3. By fax: 086 662 8861\n"
                    + "4. By e-mail: aartoenquiries@rtia.co.za"
                    + "\n\n\n\n";

            String chargeDetailsString = "\n"
                    + "" + "                    INFRINGEMENT NOTICE\n"
                    + "               Johannesburg Metro Police Department \n"
                    + "\n"
                    + "Infringement Notice Number :" + infringementNoticeNumber + "\n"
                    + "            INFRINGER AND MOTOR VEHICLE PARTICULARS \n"
                    + "Gender   :" + gender + "                   Name :" + name + "\n"
                    + "ID Type  :" + idType + "   ID Number :" + idNumber + "\n"
                    + "\n"
                    + "Vehicle Description :" + vehicleDescription + " \n"
                    + "Registration Number :" + registrationNumber + "\n"
                    + "\n"
                    + "                  LOCATION, DATE AND TIME OF INFRINGEMENT \n"
                    + "Province     :" + provice + "                  City/Town :" + cityTown + "\n"
                    + "Street       :" + street + "\n"
                    + "Route Number :" + routeNumber + " \n"
                    + "Date         :" + date + " " + "\n"
                    + "\n"
                    + "                        MAIN CHARGE \n"
                    + "Charge Code       :" + mainChargeCode + "       Legal Ref: " + mainLegalRef + "\n"
                    + "Charge Description:" + mainChargeDescription + " \n"
                    + "Penalty Amount    :R " + mainPenaltyAmount + "\n"
                    + "Discount Amount   :R " + mainDiscountAmount + "    Discounted Amount:R " + mainDiscountedAmount + "\n"
                    + "\n"
                    + altCharge
                    + "\n"
                    + "                        OFFICER DETAILS \n"
                    + "Issuing Authority        :" + issuingAuthority + "\n"
                    + "Issuing Authority Code   :" + issuingAuthorityCode + "\n"
                    + "Magistraterial District  :" + magistrateDistrict + "\n"
                    //+ "Magistraterial District Code :" + magistrateDistrictCode + "\n"
                    + "Officer Name             :" + officerName + "\n"
                    + "Infrastructure Number    :" + infrastructureNumber + "\n"
                    + "    \n"
                    +footer;

            mmOutputStream.write(chargeDetailsString.getBytes());
            // Setting height
            int gs = 29;
            mmOutputStream.write(intToByteArray(gs));
            int h = 140;
            mmOutputStream.write(intToByteArray(h));
            int n = 162;
            mmOutputStream.write(intToByteArray(n));

            // Setting Width
            int gs_width = 29;
            mmOutputStream.write(intToByteArray(gs_width));
            int w = 119;
            mmOutputStream.write(intToByteArray(w));
            int n_width = 2;
            mmOutputStream.write(intToByteArray(n_width));

            // tell the user data were sent
            //  myLabel.setText("Data sent.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static byte intToByteArray(int value) {
        byte[] b = ByteBuffer.allocate(4).putInt(value).array();

        for (int k = 0; k < b.length; k++) {
            System.out.println("Selva  [" + k + "] = " + "0x"
                    + UnicodeFormatter.byteToHex(b[k]));
        }

        return b[3];
    }

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket
            // because mmSocket is final.
            BluetoothSocket tmp = null;
            mmDevice = device;

            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.
                UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
                tmp = device.createInsecureRfcommSocketToServiceRecord(uuid);
            } catch (IOException e) {
                // Log.e(TAG, "Socket's create() method failed", e);
            }
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception.
                mmSocket.connect();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    // Log.e(TAG, "Could not close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            //  manageMyConnectedSocket(mmSocket);
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                //Log.e(TAG, "Could not close the client socket", e);
            }
        }
    }


    public void showConnectionErrorAlert() {
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("Printing Service")
                .setContentText("Unable to print : Please check bluetooth connection with printer")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();

                    }
                })
                .show();

    }

    public void navigateToMenuActivity(){
        Intent intent = new Intent(this, MenuActivity.class);
        //Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }

}
