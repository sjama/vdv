
package co.za.ngwane.vdv;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.codehaus.jackson.map.ObjectMapper;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.rtmc.dto.per.Licence;
import co.za.rtmc.dto.per.RootObject;
import co.za.rtmc.enums.AppConstants;
import co.za.rtmc.service.Validator;
import co.za.rtmc.vo.BoRoadSafetyInspection;
import co.za.rtmc.vo.RdtstrptRwsVo;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;
import za.co.technovolve.dlcserializerrsa.DrivingLicenseCard;

public class DriverDetailsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    EditText idNumberET;
    String dlIdType = "";
    SweetAlertDialog pDialog;
    DrivingLicenseCard card = null;
    BoRoadSafetyInspection roadSafetyInspection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.driver_query_detail);

        Intent intent = getIntent();
//        RdtstrptRwsVo cardJson = (RdtstrptRwsVo) intent.getSerializableExtra("card");
//        roadSafetyInspection = (BoRoadSafetyInspection) intent.getSerializableExtra("inspectionData");


        idNumberET = (EditText) findViewById(R.id.idNumber);
        // Instantiate Progress Dialog object
        Spinner spinner1 = (Spinner) findViewById(R.id.idTypeSpinner);
        // Spinner click listener
        spinner1.setOnItemSelectedListener(this);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.idType_array, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner1.setAdapter(adapter1);

        spinner1.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(
                            AdapterView<?> parent, View view, int position, long id) {
                        System.out.print(parent.getItemAtPosition(position));
                        dlIdType = parent.getItemAtPosition(position).toString();
                        // showToast("Spinner1: position=" + position + " id=" + id);
                        // idnumber input status is changed from number to text.
                        idNumberET.setText("");

                        if (dlIdType.equalsIgnoreCase(Validator.RSA_IDENTITY_DOCUMENT)) {
                            // show password
                            idNumberET.setInputType(InputType.TYPE_CLASS_NUMBER);
                        } else {
                            idNumberET.setInputType(InputType.TYPE_CLASS_TEXT);
                            idNumberET.setFilters(new InputFilter[] {filter, new InputFilter.AllCaps(), new InputFilter.LengthFilter(17)});

                        }
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        // showToast("Spinner1: unselected");
                    }
                });




//        Button mEmailSignInButton = (Button) findViewById(R.id.btnSubmit);
//        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                searchEnatis();
//            }
//        });


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_recents:
                        DriverDetailsActivity.super.onBackPressed();

                        break;
//                    case R.id.action_favorites:
//                        Toast.makeText(ChargeCodeActivity.this, "Favorites", Toast.LENGTH_SHORT).show();
//                        break;
                    case R.id.action_nearby:
                        searchEnatis();
                        break;

                }
                return true;
            }
        });

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public static InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (!Character.isLetterOrDigit(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }
    };

    private void searchEnatis() {

        View focusView = null;
        boolean cancel = false;
        String idNumber = idNumberET.getText().toString();

        if (TextUtils.isEmpty(idNumber)) {
            idNumberET.setError("Driver Id Number Required");
            focusView = idNumberET;
            cancel = true;
        } else {
            if (!Validator.isIdDocNValidLength( dlIdType.trim() , idNumber )) {
                idNumberET.setError("This type of identification number requires 13 characters.");
                focusView = idNumberET;
                focusView.requestFocus();
                return;
            }

            if (!Validator.isCheckDigitValid( dlIdType.trim() , idNumber )) {
                idNumberET.setError("Identification number invalid.");
                focusView = idNumberET;
                focusView.requestFocus();
                return;
            }

        }

        if (cancel) {
            focusView.requestFocus();
        } else {

            RdtstrptRwsVo rwsVo = new RdtstrptRwsVo();
            rwsVo.setDriidn(idNumber);
            rwsVo.setDrividtypecd(dlIdType);

            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SessionUser", Context.MODE_PRIVATE);
            rwsVo.setUsername(sharedPref.getString("phephaUser",null));

            invokeWS(idNumber, dlIdType, rwsVo);
        }
    }

    public void invokeWS(String idNumber, String dlIdType, final RdtstrptRwsVo rwsVo ) {
        // Show Progress Dialog
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        // Make RESTful webservice call using AsyncHttpClient object
        //ByteArrayEntity entity = new ByteArrayEntity(jsonObject.toString().getBytes("UTF-8"));
        //  entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        StringEntity entity = null;
        try {
            //entity = new ByteArrayEntity(new JSONObject().toString().getBytes("UTF-8"));
            Gson gson = new GsonBuilder().create();

            rwsVo.setDriidn(idNumber);
            rwsVo.setDrividtypecd(dlIdType);
//            rwsVo.setUsername("BHEKIL");
            entity = new StringEntity(gson.toJson(rwsVo));
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        } catch (Exception e) {
            e.printStackTrace();
        }


        //AsyncHttpClient client = new AsyncHttpClient();
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);

        String idtypecd = "";
        if (dlIdType.equalsIgnoreCase(Validator.RSA_IDENTITY_DOCUMENT)) {
            // show password
            idtypecd = "02";
        } else if (dlIdType.equalsIgnoreCase(Validator.FOREIGN_IDENTITY_DOCUMENT)){
            idtypecd = "03";

        } else {
            idtypecd = "03";
        }
        rwsVo.setDrividtypecd(idtypecd);
        RequestParams params = new RequestParams();
        params.put("idTypeCd", idtypecd);
        params.put("idNumber", idNumber);
        client.get(AppConstants.BACK_OFFICE_APP_URL + "v1/enatis/X1002Driver/"+ idtypecd + "/"+ idNumber, new TextHttpResponseHandler() {
        @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    // JSON Object
                    pDialog.hide();
                    System.out.println(responseBody);
                    Gson gson = new GsonBuilder().create();


                    //Person person = gson.fromJson(responseBody, Person.class);
                    // When the JSON response has status boolean value assigned with true

                    if ( responseBody.isEmpty() ) {
                        showFail(rwsVo);
                    }
                    // Else display error message
                    else {
                        ObjectMapper mapper = new ObjectMapper();
                        RootObject per = mapper.readValue( responseBody , RootObject.class);
                        rwsVo.setDriname( per.getPerDet().getPerId().getInitials() );
                        rwsVo.setDrisurname( per.getPerDet().getPerId().getBusOrSurname() );
                        rwsVo.setLicCardNo(per.getLicCard().getDriLicNr());
                        rwsVo.setDrividtypecd(per.getPerDet().getPerId().getIdDocType().getCode()+"");
                        rwsVo.setGender(per.getPerDet().getNatOfPer().getDesc());


                        String lics = "";
                        if (per.getLicence() != null) {
                            for (Licence lic :  per.getLicence()) {
                                lics = lics + lics != "" ? ", " : "" + lic.getDriLic().getCode();
                                //rwsVo.setLiccd(lic.getDriLic().getCode());
                                //rwsVo.setLiccd(person.getLicence().getDriLic().getCode());
                            }

                            rwsVo.setLiccd( lics );
                            rwsVo.setDriLicValidToD(per.getLicence().get(0).getDriLicValidToD());
                        }

                        String prdp = "";
                        if (per.getPrDP() != null) {

                            if (per.getPrDP().getPrDPCatG() != null ) {
                                prdp = per.getPrDP().getPrDPCatG().getCode();
                            }

                            if (per.getPrDP().getPrDPCatD() != null ) {
                                prdp =  prdp + prdp != "" ? ", " : "" + per.getPrDP().getPrDPCatD().getCode();
                            }

                            if (per.getPrDP().getPrDPCatP() != null ) {
                                prdp =   prdp + prdp != "" ? ", " : "" + per.getPrDP().getPrDPCatP().getCode();
                            }

                            rwsVo.setDriPrDPs( prdp );
                            rwsVo.setDriPrDPValidToD( per.getPrDP().getPrDPExpiryD() );
                        }


                        navigateToManualForm( rwsVo );
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "No Data Found on eNaTIS!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    navigateToHome();

                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Could not establish connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Could not establish connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Could not establish connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }

            }
        });
    }


    @Override
    public void onBackPressed() {
    }

    public void onPreviousPressed() {
        super.onBackPressed();
        return;
    }

    private void  showFail(final RdtstrptRwsVo rwsVo){

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("NTP 733 Road Test Report")
                .setContentText("Driver not found on eNatis")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        navigateToManualForm(rwsVo);
                    }
                })
                .show();


    }

    private void navigateToManualForm(RdtstrptRwsVo vo ) {
        Intent intent = new Intent(this, DriverSummaryActivity.class);
        intent.putExtra("rdtstrptRwsVo", vo);
        intent.putExtra("inspectionData", roadSafetyInspection);
        startActivity(intent);
    }

    private void navigateToHome() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }


}
