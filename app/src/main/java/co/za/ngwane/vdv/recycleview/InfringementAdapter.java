package co.za.ngwane.vdv.recycleview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

import co.za.ngwane.vdv.R;
import co.za.rtmc.vo.Infringement;

public class InfringementAdapter extends RecyclerView.Adapter<InfringementAdapter.MyViewHolder> {

    private List<Infringement> infringementList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView noticeNumber, date, status,licenseNumber;

        public MyViewHolder(View view) {
            super(view);
            noticeNumber = (TextView) view.findViewById(R.id.notice_no);
            date = (TextView) view.findViewById(R.id.infringe_date);
            status = (TextView) view.findViewById(R.id.infringe_stat);
            licenseNumber = (TextView) view.findViewById(R.id.license_number);
        }
    }


    public InfringementAdapter(List<Infringement> infringementList) {
        this.infringementList = infringementList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.infrigement_row_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Infringement infringement = infringementList.get(position);
        holder.noticeNumber.setText(infringement.getInfringementNoticeNumber());
        holder.date.setText(infringement.getInfringementDate());
        holder.status.setText(infringement.getInfringementStatusDescription());
        holder.licenseNumber.setText(infringement.getLicenceNumber());
    }

    @Override
    public int getItemCount() {
        return infringementList.size();
    }
}