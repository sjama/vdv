//package co.za.ngwane.vdv;
//import android.app.Activity;
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothDevice;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.ListView;
//import android.widget.Toast;
//import java.util.ArrayList;
//import java.util.Set;
//
///**
// * Created by jamasithole on 2019/06/04.
// */
//
//public class BluetoothUtil extends Activity {
//    private ListView lstvw;
//    private ArrayAdapter aAdapter;
//    private BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();
//    BluetoothDevice mmDevice;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        Button btn = (Button)findViewById(R.id.btnGet);
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(bAdapter==null){
//                    Toast.makeText(getApplicationContext(),"Bluetooth Not Supported",Toast.LENGTH_SHORT).show();
//                }
//                else{
//                    Set<BluetoothDevice> pairedDevices = bAdapter.getBondedDevices();
//                    ArrayList list = new ArrayList();
//                    if(pairedDevices.size()>0){
//                        for(BluetoothDevice device: pairedDevices){
//                            String devicename = device.getName();
//                            String macAddress = device.getAddress();
//                            list.add("Name: "+devicename+"MAC Address: "+macAddress);
//                        }
//                        lstvw = (ListView) findViewById(R.id.deviceList);
//                        aAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, list);
//                        lstvw.setAdapter(aAdapter);
//                    }else{
//                        if (bAdapter.startDiscovery()) {
//
//                            //If discovery has started, then display the following toast....//
//                            Toast.makeText(getApplicationContext(), "Discovering other bluetooth devices...",
//                                    Toast.LENGTH_SHORT).show();
//                        } else {
//
//                            //If discovery hasn’t started, then display this alternative toast//
//                            Toast.makeText(getApplicationContext(), "Something went wrong! Discovery has failed to start.",
//                                    Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }
//            }
//
//
//        });
//    }
//
//
//
//
//    // Create a BroadcastReceiver for ACTION_FOUND.
//    private final BroadcastReceiver receiver = new BroadcastReceiver() {
//        public void onReceive(Context context, Intent intent) {
//            String action = intent.getAction();
//            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
//                // Discovery has found a device. Get the BluetoothDevice
//                // object and its info from the Intent.
//                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
//                String deviceName = device.getName();
//                String deviceHardwareAddress = device.getAddress(); // MAC address
//            }
//        }
//    };
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//
//        // Don't forget to unregister the ACTION_FOUND receiver.
//        unregisterReceiver(receiver);
//    }
//}