package co.za.ngwane.vdv;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.microblink.recognizers.blinkbarcode.bardecoder.BarDecoderRecognizerSettings;
import com.microblink.recognizers.blinkbarcode.pdf417.Pdf417RecognizerSettings;
import com.microblink.recognizers.blinkbarcode.zxing.ZXingRecognizerSettings;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.rtmc.service.SessionManager;

/**
 * Created by Jama on 2016/04/26.
 */
public class HomeActivity extends AppCompatActivity {
    private CoordinatorLayout coordinatorLayout;
    SessionManager session;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Displays Home Screen
        setContentView(R.layout.activity_home);
        buildRecognitionSettings();

        // Request permissions if not granted, we need CAMERA permission and
        // WRITE_EXTERNAL_STORAGE permission because images that are taken by camera
        // will be stored on external storage and used in recognition process
        List<String> requiredPermissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requiredPermissions.add(android.Manifest.permission.CAMERA);
        }
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requiredPermissions.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requiredPermissions.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }

        if (requiredPermissions.size() > 0) {
            String[] permArray = new String[requiredPermissions.size()];
            permArray = requiredPermissions.toArray(permArray);
//            ActivityCompat.requestPermissions(this, permArray, PERMISSION_REQUEST_CODE);
        }

        session = new SessionManager(getApplicationContext());


        Button scanNow = (Button) findViewById(R.id.scan_button);
        scanNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initScan();
                //navigateToDriverDetails();
            }
        });

        Button searchMv = (Button) findViewById(R.id.mvSearch_button);
        searchMv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                proceedOption();
                //navigateToDriverDetails();
            }
        });


        Button tableBtn = (Button) findViewById(R.id.query_driver_button);
        tableBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToTrafficFineHomeForm();
            }
        });

       /* Button logoutBtn = (Button) findViewById(R.id.logout_button);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Clear the session data
                // This will clear all session data and
                // redirect user to LoginActivity
                session.logoutUser();
            }
        }); */



//        BottomBar bottomBar = BottomBar.attach(this, savedInstanceState);
//        bottomBar.setItemsFromMenu(R.menu.menu, new OnMenuTabSelectedListener() {
//            @Override
//            public void onMenuItemSelected(int itemId) {
//                switch (itemId) {
//                    case R.id.recent_item:
//                        //Snackbar.make(coordinatorLayout, "Recent Item Selected", Snackbar.LENGTH_LONG).show();
//                        navigateToHomeForm();
//                        break;
//                    case R.id.favorite_item:
//                        navigateToTableForm();
//                        break;
//                    case R.id.location_item:
//                        navigateToLocationForm();
//                        break;
//                }
//            }
//        });

    }

    private void buildRecognitionSettings() {
        // Don't enable recognizers and barcode types which you don't actually use because this will
        // significantly decrease the scanning speed.

        // Pdf417RecognizerSettings define the settings for scanning plain PDF417 barcodes.
        Pdf417RecognizerSettings pdf417RecognizerSettings = new Pdf417RecognizerSettings();
        // Set this to true to scan barcodes which don't have quiet zone (white area) around it
        // Use only if necessary because it drastically slows down the recognition process
        pdf417RecognizerSettings.setNullQuietZoneAllowed(true);
        // Set this to true to scan even barcode not compliant with standards
        // For example, malformed PDF417 barcodes which were incorrectly encoded
        // Use only if necessary because it slows down the recognition process
//        pdf417RecognizerSettings.setUncertainScanning(true);

        // BarDecoderRecognizerSettings define settings for scanning 1D barcodes with algorithms
        // implemented by Microblink team.
        BarDecoderRecognizerSettings oneDimensionalRecognizerSettings = new BarDecoderRecognizerSettings();
        // set this to true to enable scanning of Code 39 1D barcodes
        oneDimensionalRecognizerSettings.setScanCode39(true);
        // set this to true to enable scanning of Code 128 1D barcodes
        oneDimensionalRecognizerSettings.setScanCode128(true);
        // set this to true to use heavier algorithms for scanning 1D barcodes
        // those algorithms are slower, but can scan lower resolution barcodes
//        oneDimensionalRecognizerSettings.setTryHarder(true);

        // ZXingRecognizerSettings define settings for scanning barcodes with ZXing library
        // We use modified version of ZXing library to support scanning of barcodes for which
        // we still haven't implemented our own algorithms.
        ZXingRecognizerSettings zXingRecognizerSettings = new ZXingRecognizerSettings();
        // set this to true to enable scanning of QR codes
        zXingRecognizerSettings.setScanQRCode(true);
        zXingRecognizerSettings.setScanITFCode(true);

        // finally, when you have defined settings for each recognizer you want to use,
        // you should put them into array held by global settings object

//        mRecognitionSettings = new RecognitionSettings();
//        // add settings objects to recognizer settings array
//        // Pdf417Recognizer, BarDecoderRecognizer and ZXingRecognizer
//        //  will be used in the recognition process
//        mRecognitionSettings.setRecognizerSettingsArray(new RecognizerSettings[]{pdf417RecognizerSettings, oneDimensionalRecognizerSettings, zXingRecognizerSettings});

        // additionally, there are generic settings that are used by all recognizers or the
        // whole recognition process

        // by default, this option is true, which means that it is possible to obtain multiple
        // recognition results from the same image.
        // if you want to obtain one result from the first successful recognizer
        // (when first barcode is found, no matter which type) set this option to false
//        recognitionSettings.setAllowMultipleScanResultsOnSingleImage(false);

    }

    private void navigateToManualForm() {
        Intent intent = new Intent(this, VehicleDetailsActivity.class);
        startActivity(intent);

    }

    private void navigateToHomeForm() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }


    private void navigateToTableForm() {
        Intent intent = new Intent(this, DriverDetailsActivity.class);
        startActivity(intent);

    }

    private void navigateToTrafficFineHomeForm() {
        Intent intent = new Intent(this, TrafficFineHomeActivity.class);
        startActivity(intent);

    }

    private void navigateToLocationForm() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);

    }

    private void initScan() {
        Intent intent = new Intent(this, SimpleScannerActivity.class);
        startActivity(intent);
        //intentIntegrator.initiateScan(IntentIntegrator.ALL_CODE_TYPES)

    }

    private void manualScan() {
        Intent intent = new Intent(this, MvQueryScannerActivity.class);
        startActivity(intent);
        //intentIntegrator.initiateScan(IntentIntegrator.ALL_CODE_TYPES)

    }

    public void proceedOption(){
        String message =" How would you like to proceed?";
        new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("")
                .setContentText(message)
                .setConfirmText("Scan Disk")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        manualScan();
                    }
                })
                .setCancelText("Manual Capture")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        navigateToVehicleDetailsForm();
                    }
                })
                .show();

    }


    private void navigateToVehicleDetailsForm() {
        Intent intent = new Intent(this, VehicleSearch.class);
        //intent.putExtra("rdtstrptRwsVo", vo);
        startActivity(intent);
    }


    @Override
    protected void onResume() {
        super.onResume();
        // .... other stuff in my onResume ....
        this.doubleBackToExitPressedOnce = false;
    }

    @Override
    public void onBackPressed() {
 /*       if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Press back once more to exit.", Toast.LENGTH_SHORT).show();

*/    }

}
