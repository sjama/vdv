package co.za.ngwane.vdv;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.codehaus.jackson.map.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

import co.za.ngwane.vdv.recycleview.InfringementAdapter;
import co.za.ngwane.vdv.recycleview.RecyclerTouchListener;
import co.za.rtmc.dto.VehicleDet;
import co.za.rtmc.enums.AppConstants;
import co.za.rtmc.vo.GroupDTO;
import co.za.rtmc.vo.Infringement;
import co.za.rtmc.vo.InfringementsGroup;
import co.za.rtmc.vo.MvLicDiscVo;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class MainActivity extends AppCompatActivity {
    private List<Infringement> infringementList = new ArrayList<>();
    private List<GroupDTO> groupDTOS = new ArrayList<>();
    private RecyclerView recyclerView;
    private InfringementAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        infringementList = this.getIntent().getExtras().getParcelableArrayList("infringements");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new InfringementAdapter(infringementList);

        recyclerView.setHasFixedSize(true);

        // vertical RecyclerView
        // keep movie_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        // horizontal RecyclerView
        // keep movie_list_row.xml width to `wrap_content`
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        // adding custom divider line with padding 16dp
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        // row click listener
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Infringement infringement = infringementList.get(position);
                Toast.makeText(getApplicationContext(), infringement.getLicenceNumber() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigatetoReportActivity();
            }
        });

    }


    public void navigatetoReportActivity(){
        Intent intent = new Intent(this, MenuActivity.class);
        //Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }


    /**
     * Prepares sample data to provide data set to adapter
     * Tobe Replaced with Webservice Call
     */

    public void getWebserviceResposnse( ) {

        StringEntity entity = null;
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
       // String appURL = AppConstants.BACK_OFFICE_APP_URL+ "v1/enatis/X1001Vehicle/" + mvLicDiscVo.getMvRegN();
        String appURL = "http://192.168.9.11:8080/backoffice/infringeList/02/8512265513081/40630010Z7RT";

        client.get(appURL, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    System.out.println(new String(responseBody));
                    Gson gson = new GsonBuilder().create();
                    InfringementsGroup infringementsGroup = gson.fromJson(responseBody,InfringementsGroup.class);
                    GroupDTO groupDTO = new GroupDTO();
                    groupDTO.setGroupName("Infringement notice");
                    groupDTO.setGroupTotal(infringementsGroup.getInfringements().size()+"");
                    groupDTOS.add(groupDTO);
                    groupDTO.setGroupName("Warrant of execution");
                    groupDTO.setGroupTotal(infringementsGroup.getWarrantStatusList().size()+"");
                    groupDTOS.add(groupDTO);
                    groupDTO.setGroupName("Enforcement order");
                    groupDTO.setGroupTotal(infringementsGroup.getEnforcementStatusList().size()+"");
                    groupDTOS.add(groupDTO);
                    groupDTO.setGroupName("Court case");
                    groupDTO.setGroupTotal(infringementsGroup.getCourtStatusList().size()+"");
                    groupDTOS.add(groupDTO);


                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
//                pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

}
