package co.za.ngwane.vdv.dto;

public class InfringementDto {

    private String infringementNoticeNumber;
    private String gender;
    private String initials;
    private String surname;
    private String idType;
    private String idNumber;
    private String vehicleDescription;
    private String registrationNumber;
    private String provice;
    private String cityTown;
    private String street;
    private String routeNumber;
    private String date;
    private String time;

    //MAIN CHARGE
    private String mainChargeCode;
    private String mainLegalRef;
    private String mainChargeDescription;
    private String mainPenaltyAmount;
    private String mainDiscountAmount;
    private String mainDiscountedAmount;


    //ALTERNATIVE CHARGE
    private String altChargeCode;
    private String altLegalRef;
    private String altChargeDescription;
    private String altPenaltyAmount;
    private String altDiscountAmount;
    private String altDiscountedAmount;

    //OFFICER DETAILS
    private String  issuingAuthority;
    private String issuingAuthorityCode;
    private String magistrateDistrict;
    private String magistrateDistrictCode;
    private String officerName;
    private String infrastructureNumber;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getVehicleDescription() {
        return vehicleDescription;
    }

    public void setVehicleDescription(String vehicleDescription) {
        this.vehicleDescription = vehicleDescription;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getProvice() {
        return provice;
    }

    public void setProvice(String provice) {
        this.provice = provice;
    }

    public String getCityTown() {
        return cityTown;
    }

    public void setCityTown(String cityTown) {
        this.cityTown = cityTown;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMainChargeCode() {
        return mainChargeCode;
    }

    public void setMainChargeCode(String mainChargeCode) {
        this.mainChargeCode = mainChargeCode;
    }

    public String getMainLegalRef() {
        return mainLegalRef;
    }

    public void setMainLegalRef(String mainLegalRef) {
        this.mainLegalRef = mainLegalRef;
    }

    public String getMainChargeDescription() {
        return mainChargeDescription;
    }

    public void setMainChargeDescription(String mainChargeDescription) {
        this.mainChargeDescription = mainChargeDescription;
    }

    public String getMainPenaltyAmount() {
        return mainPenaltyAmount;
    }

    public void setMainPenaltyAmount(String mainPenaltyAmount) {
        this.mainPenaltyAmount = mainPenaltyAmount;
    }

    public String getMainDiscountAmount() {
        return mainDiscountAmount;
    }

    public void setMainDiscountAmount(String mainDiscountAmount) {
        this.mainDiscountAmount = mainDiscountAmount;
    }

    public String getMainDiscountedAmount() {
        return mainDiscountedAmount;
    }

    public void setMainDiscountedAmount(String mainDiscountedAmount) {
        this.mainDiscountedAmount = mainDiscountedAmount;
    }

    public String getAltChargeCode() {
        return altChargeCode;
    }

    public void setAltChargeCode(String altChargeCode) {
        this.altChargeCode = altChargeCode;
    }

    public String getAltLegalRef() {
        return altLegalRef;
    }

    public void setAltLegalRef(String altLegalRef) {
        this.altLegalRef = altLegalRef;
    }

    public String getAltChargeDescription() {
        return altChargeDescription;
    }

    public void setAltChargeDescription(String altChargeDescription) {
        this.altChargeDescription = altChargeDescription;
    }

    public String getAltPenaltyAmount() {
        return altPenaltyAmount;
    }

    public void setAltPenaltyAmount(String altPenaltyAmount) {
        this.altPenaltyAmount = altPenaltyAmount;
    }

    public String getAltDiscountAmount() {
        return altDiscountAmount;
    }

    public void setAltDiscountAmount(String altDiscountAmount) {
        this.altDiscountAmount = altDiscountAmount;
    }

    public String getAltDiscountedAmount() {
        return altDiscountedAmount;
    }

    public void setAltDiscountedAmount(String altDiscountedAmount) {
        this.altDiscountedAmount = altDiscountedAmount;
    }

    public String getIssuingAuthority() {
        return issuingAuthority;
    }

    public void setIssuingAuthority(String issuingAuthority) {
        this.issuingAuthority = issuingAuthority;
    }

    public String getIssuingAuthorityCode() {
        return issuingAuthorityCode;
    }

    public void setIssuingAuthorityCode(String issuingAuthorityCode) {
        this.issuingAuthorityCode = issuingAuthorityCode;
    }

    public String getMagistrateDistrict() {
        return magistrateDistrict;
    }

    public void setMagistrateDistrict(String magistrateDistrict) {
        this.magistrateDistrict = magistrateDistrict;
    }

    public String getMagistrateDistrictCode() {
        return magistrateDistrictCode;
    }

    public void setMagistrateDistrictCode(String magistrateDistrictCode) {
        this.magistrateDistrictCode = magistrateDistrictCode;
    }

    public String getOfficerName() {
        return officerName;
    }

    public void setOfficerName(String officerName) {
        this.officerName = officerName;
    }

    public String getInfrastructureNumber() {
        return infrastructureNumber;
    }

    public void setInfrastructureNumber(String infrastructureNumber) {
        this.infrastructureNumber = infrastructureNumber;
    }

    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    public void setInfringementNoticeNumber(String infringementNoticeNumber) {
        this.infringementNoticeNumber = infringementNoticeNumber;
    }
}
