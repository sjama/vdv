package co.za.ngwane.vdv;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.codehaus.jackson.map.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.rtmc.dto.VehicleDet;
import co.za.rtmc.enums.AppConstants;
import co.za.rtmc.service.Validator;
import co.za.rtmc.vo.BoRoadSafetyInspection;
import co.za.rtmc.vo.MvLicDiscVo;
import co.za.rtmc.vo.RdtstrptRwsVo;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

/**
 * Created by Jama on 2016/06/09.
 */
public class VehicleDetailsActivity extends MainActivity implements View.OnClickListener, View.OnFocusChangeListener {

    EditText regNumberET;
    EditText colorET;
    EditText vinET;
    EditText makeModelET;
    EditText modelET;
    EditText rwStatET;
    EditText sapMarkET;

    EditText mvTypeET;
    EditText mvLicDateET;
    EditText mvStateET;
    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    String mvType = "";
    SweetAlertDialog pDialog;
    private CoordinatorLayout coordinatorLayout;
    MvLicDiscVo mvLicDiscVo = new MvLicDiscVo();
    BoRoadSafetyInspection boRoadSafetyInspection = new BoRoadSafetyInspection();

    private RdtstrptRwsVo rwsVo = new RdtstrptRwsVo();
    static final String[] MV_CAT = new String[] {"Select MV Type",
            AppConstants.MV_CAT_LIGHT_PASSENGER_MV_DESC,
            AppConstants.MV_CAT_HEAVY_PASSENGER_MV_DESC,
            AppConstants.MV_CAT_LIGHT_LOAD_VEH_NOT_DRAW_DESC,
            AppConstants.MV_CAT_HEAVY_LOAD_DRAW_VEHICLE_DESC,
            AppConstants.MV_CAT_HEAVY_LOAD_VEH_NOT_DRAW_DESC,
            AppConstants.MV_CAT_MOTORCYCLE_DESC,
            AppConstants.MV_CAT_SPECIAL_VEHICLE_DESC
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicledetails);


        Intent intent = getIntent();
        rwsVo = (RdtstrptRwsVo) intent.getSerializableExtra("rdtstrptRwsVo");
        final MvLicDiscVo discVo  = (MvLicDiscVo) intent.getSerializableExtra("discVo");

        if ( discVo!= null && discVo.isEnatisNoData()) {
            showNoData();
        }

        if (discVo!= null && discVo.isEnatisNoMatch()) {
            showWarning();
        }

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        mvLicDateET = (EditText) findViewById(R.id.mvExpDate);
        mvLicDateET.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        setDateTimeField();

        regNumberET = (EditText) findViewById(R.id.mvRegNumET);
        regNumberET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(10)});
        mvStateET = (EditText) findViewById(R.id.mvState);
        mvStateET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});


        colorET = (EditText) findViewById(R.id.mvColorET);
        colorET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(15)});

        vinET = (EditText) findViewById(R.id.mvVinET);
        vinET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(17)});

        makeModelET = (EditText) findViewById(R.id.mvMmET);
        makeModelET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});

        modelET = (EditText) findViewById(R.id.mvModel);
        modelET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});
//        mvType = (EditText) findViewById(R.id.mvTypeET);
        mvLicDateET = (EditText) findViewById(R.id.mvExpDate);
        rwStatET = (EditText) findViewById(R.id.mvRwStat);
        rwStatET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});
        sapMarkET = (EditText) findViewById(R.id.mvSapMark);
        sapMarkET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});

        try {
            regNumberET.setText(discVo.getMvRegN());
            regNumberET.setEnabled(false);
            colorET.setText(discVo.getColor());

            if (discVo.getColor() != null && !discVo.getColor().isEmpty()) {
                colorET.setEnabled(false);
            }

            vinET.setText(discVo.getVin());
            if (discVo.getVin() != null && !discVo.getVin().isEmpty()) {
                vinET.setEnabled(false);
            }

            makeModelET.setText(discVo.getMake());
            if (discVo.getMake() != null && !discVo.getMake().isEmpty()) {
                makeModelET.setEnabled(false);
            }
            modelET.setText(discVo.getModel());
            if (discVo.getModel() != null && !discVo.getModel().isEmpty()) {
                modelET.setEnabled(false);
            }
            mvType = discVo.getMvDescCdDesc();
            mvLicDateET.setText(discVo.getExpiry());
            if (discVo.getExpiry() != null && !discVo.getExpiry().isEmpty()) {
                mvLicDateET.setEnabled(false);
            }
            mvStateET.setText(discVo.getMvState());
            mvStateET.setEnabled(false);

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date targetD = sdf.parse(discVo.getExpiry());
            Date date2 = new Date();
            //checkEnatisForVehicle(discVo.getMvRegN());
            sapMarkET.setText(discVo.getSapMark());
            sapMarkET.setEnabled(false);
            rwStatET.setText(discVo.getRwStatus());
            rwStatET.setEnabled(false);

            if(discVo.getSapMark().equalsIgnoreCase(AppConstants.NONE)){
                sapMarkET.setBackgroundColor(Color.GREEN);
            }else{
                sapMarkET.setBackgroundColor(Color.RED);
            }

            if(discVo.getRwStatus().equalsIgnoreCase(AppConstants.ROAD_WORTHY)){
                rwStatET.setBackgroundColor(Color.GREEN);
            }else{
                rwStatET.setBackgroundColor(Color.RED);
            }


            if ( Validator.isDateOutOfRange(targetD, new Date(), Calendar.MONTH, 3 ) ) {
                mvLicDateET.setBackgroundColor(Color.GREEN);
            } else if (  Validator.isDateOutOfRange(targetD, new Date(), Calendar.MONTH, 2 )) {
                mvLicDateET.setBackgroundColor(Color.YELLOW);
            } else if ( targetD.compareTo(date2) < 0 ) {
                mvLicDateET.setBackgroundColor( Color.RED );
            }

            System.out.println("Registration Number = " + discVo.getMvRegN());

        }catch (Exception e){
            e.printStackTrace();

        }



        final Spinner spinner = (Spinner) findViewById(R.id.mvTypeSpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
/*        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.mvType_array, android.R.layout.simple_spinner_item);*/

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, MV_CAT);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(

                            AdapterView<?> parent, View view, int position, long id) {
                        if ( discVo != null && (discVo.getMvDescCdDesc() != null )) {
                            int spinnerPosition = adapter.getPosition(discVo.getMvDescCdDesc());
                            spinner.setSelection(spinnerPosition);

                            if (discVo.getMvDescCdDesc() != null) {
                                spinner.setEnabled(false);
                            }
                        }
                        System.out.print(parent.getItemAtPosition(position));
                        mvType = parent.getItemAtPosition(position).toString();
                        // showToast("Spinner1: position=" + position + " id=" + id);

                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        // showToast("Spinner1: unselected");
                    }
                });


        Button mEmailSignInButton = (Button) findViewById(R.id.btnVDContinue);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean cancel = false;
                View focusView = null;

                regNumberET = (EditText) findViewById(R.id.mvRegNumET);
                colorET = (EditText) findViewById(R.id.mvColorET);
                vinET = (EditText) findViewById(R.id.mvVinET);
                makeModelET = (EditText) findViewById(R.id.mvMmET);
//        mvType = (EditText) findViewById(R.id.mvTypeET);
                mvLicDateET = (EditText) findViewById(R.id.mvExpDate);

                if (TextUtils.isEmpty(mvLicDateET.getText().toString())) {
                    mvLicDateET.setError(getString(R.string.error_invalid_licExpD));
                    focusView = mvLicDateET;
                    cancel = true;
                }


                if (TextUtils.isEmpty(makeModelET.getText().toString())) {
                    makeModelET.setError(getString(R.string.error_invalid_vMake));
                    focusView = makeModelET;
                    cancel = true;
                }

                if (TextUtils.isEmpty(modelET.getText().toString())) {
                    modelET.setError(getString(R.string.error_invalid_vModel));
                    focusView = modelET;
                    cancel = true;
                }


                if (TextUtils.isEmpty(colorET.getText().toString())) {
                    colorET.setError(getString(R.string.error_invalid_vColor));
                    focusView = colorET;
                    cancel = true;
                }

                if (TextUtils.isEmpty(regNumberET.getText().toString())) {
                    regNumberET.setError(getString(R.string.error_invalid_VehReg));
                    focusView = regNumberET;
                    cancel = true;
                }

                if (TextUtils.isEmpty(mvType) || mvType.trim().equalsIgnoreCase("Select MV Type")) {
                    Toast.makeText(VehicleDetailsActivity.this,
                            "Please select Motor Vehicle type.!!", Toast.LENGTH_LONG)
                            .show();
                    return;
                }

  /*              if (TextUtils.isEmpty(mvType) || mvType.trim().equalsIgnoreCase("Select MV Type")) {
                    showFail();
                }
*/

                if (cancel) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                } else {
                    discVo.setMvRegN(regNumberET.getText().toString());
                    discVo.setColor(colorET.getText().toString());
                    colorET.setEnabled(false);
                    discVo.setVin(vinET.getText().toString());

                    if (TextUtils.isEmpty(discVo.getMake())) {
                        discVo.setMake(makeModelET.getText().toString());
                    }

                    discVo.setMvDescCdDesc(mvType);
                    discVo.setExpiry(mvLicDateET.getText().toString());
                    mvLicDateET.setEnabled(false);

                    System.out.print(discVo.toString());
                    //invokeWS( discVo );
                    boRoadSafetyInspection.setMvRegn(discVo.getMvRegN());
                    boRoadSafetyInspection.setMake(discVo.getMake());
                    boRoadSafetyInspection.setModel(discVo.getModel());
                    boRoadSafetyInspection.setColor(discVo.getColor());
                    boRoadSafetyInspection.setLicDiscExpd(discVo.getExpiry());


                    //Data From eNaTis below
                    boRoadSafetyInspection.setMvRwStat(discVo.getRwStatus());
                    boRoadSafetyInspection.setSapMark(discVo.getSapMark());

                    //END Of eNaTis

                    if (rwsVo  == null){
                        rwsVo = new RdtstrptRwsVo();
                    }
                    rwsVo.setMvreg( discVo.getMvRegN() );
                    if ( mvType.equalsIgnoreCase(AppConstants.MV_CAT_MOTORCYCLE_DESC)) {
                        rwsVo.setMvtypecd(AppConstants.MV_CAT_MOTORCYCLE);
                        rwsVo.setMvtypedesc(mvType);
                    } else if ( mvType.equalsIgnoreCase(AppConstants.MV_CAT_LIGHT_PASSENGER_MV_DESC )) {
                        rwsVo.setMvtypecd( AppConstants.MV_CAT_LIGHT_PASSENGER_MV );
                        rwsVo.setMvtypedesc(mvType);
                    } else if ( mvType.equalsIgnoreCase(AppConstants.MV_CAT_HEAVY_PASSENGER_MV_DESC )) {
                        rwsVo.setMvtypecd( AppConstants.MV_CAT_HEAVY_PASSENGER_MV );
                        rwsVo.setMvtypedesc( mvType );
                    } else if ( mvType.equalsIgnoreCase(AppConstants.MV_CAT_LIGHT_LOAD_VEH_NOT_DRAW_DESC )) {
                        rwsVo.setMvtypecd( mvType );
                        rwsVo.setMvtypedesc( AppConstants.MV_CAT_LIGHT_LOAD_VEH_NOT_DRAW );
                    } else if ( mvType.equalsIgnoreCase(AppConstants.MV_CAT_HEAVY_LOAD_VEH_NOT_DRAW_DESC )) {
                        rwsVo.setMvtypecd( AppConstants.MV_CAT_HEAVY_LOAD_VEH_NOT_DRAW );
                        rwsVo.setMvtypedesc( mvType );
                    } else if ( mvType.equalsIgnoreCase(AppConstants.MV_CAT_HEAVY_LOAD_DRAW_VEHICLE_DESC )) {
                        rwsVo.setMvtypecd( AppConstants.MV_CAT_HEAVY_LOAD_DRAW_VEHICLE );
                        rwsVo.setMvtypedesc( mvType );
                    } else if ( mvType.equalsIgnoreCase(AppConstants.MV_CAT_SPECIAL_VEHICLE_DESC )) {
                        rwsVo.setMvtypecd( AppConstants.MV_CAT_SPECIAL_VEHICLE );
                        rwsVo.setMvtypedesc( mvType );
                    }


                    navigateToManualForm(discVo);
                }
            }
        });






    }


    private void setDateTimeField() {
        mvLicDateET.setOnClickListener(this);
        mvLicDateET.setOnFocusChangeListener(this);
        mvLicDateET.setImeOptions(EditorInfo.IME_ACTION_DONE);

        Calendar newCalendar = Calendar.getInstance();
        // DatePickerDialog THEME_HOLO_LIGHT
//        DatePickerDialog dpd = new DatePickerDialog(getActivity(),AlertDialog.THEME_TRADITIONAL,this,year, month, day){

        fromDatePickerDialog = new DatePickerDialog(this,  new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                mvLicDateET.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        if ( !TextUtils.isEmpty(mvLicDateET.getText())) {
            mvLicDateET.setText(mvLicDateET.getText());
        }
    }

    private void  showNoData(){

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("NTP 733 Road Test Report")
                .setContentText("Vehicle not found on eNatis")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();


    }

    public void showWarning( ) {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("NTP 733 Road Test Report")
                .setContentText("Vehicle details do not match with the details on eNatis")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();
    }


    private void navigateToManualForm(MvLicDiscVo vo) {

        Intent intent = new Intent(this, PersonDetailActivity.class);

        intent.putExtra("discVo", vo);
        intent.putExtra("inspectionData", boRoadSafetyInspection);
        startActivity(intent);

    }

    private void  showFail(){

        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText("NTP 733 Road Test Report")
                .setContentText("Please select Motor Vehicle type.")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .show();


    }

/*
    @Override
    public void onBackPressed() {
    }*/

    @Override
    public void onClick(View v) {

        if(v == mvLicDateET) {
            fromDatePickerDialog.show();
        }
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(v == mvLicDateET && hasFocus) {
            fromDatePickerDialog.show();
        }
    }

    //private method of your class
    private int getIndex(Spinner spinner, String myString) {
        int index = 0;

        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                index = i;
                break;
            }
        }
        return index;
    }

    public void onPreviousPressed() {
        super.onBackPressed();
        return;
    }

    @Override
    public void onBackPressed() {
    }

    public void checkEnatisForVehicle(String regN ) {
        // Show Progress Dialog
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();
        StringEntity entity = null;
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        RequestParams params = new RequestParams();
        client.get(AppConstants.BACK_OFFICE_APP_URL + "v1/enatis/X1007Vehicle/"+ regN, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {

                    System.out.println(new String(responseBody));
                    Gson gson = new GsonBuilder().create();
                    if ( responseBody.isEmpty() ) {
                        mvLicDiscVo.setEnatisNoData(true);
                        mvLicDiscVo.setMvDescCdDesc( null );
                        Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                    } else {
                        ObjectMapper mapper = new ObjectMapper();
                        VehicleDet vehicle = mapper.readValue( responseBody , VehicleDet.class);

                        mvLicDiscVo.setRwStatus(vehicle.getRwstat().getDesc());
                        mvLicDiscVo.setSapMark(vehicle.getSapmark().getDesc());

                    }

                    pDialog.hide();

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
//                pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }

            }
        });
     }


    public void invokeWS(final MvLicDiscVo vo) {

        StringEntity entity = null;
        try {
            Gson gson = new GsonBuilder().create();
            rwsVo.setMvtypecd("1"); // Needs to fix to mvType
            rwsVo.setMvreg(vo.getMvRegN());
            entity = new StringEntity(gson.toJson(rwsVo));
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        } catch (Exception e) {
            e.printStackTrace();
        }


        AsyncHttpClient client = new AsyncHttpClient();
        // client.post(getApplicationContext(),"http://10.0.2.2:9090/phepha/svs/userspRws/doLogin", entity,"application/json", new AsyncHttpResponseHandler() {
        client.post(getApplicationContext(), "http://41.185.29.249:8080/phepha/svs/rdtstrptRws/add", entity, "application/json", new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    // JSON Object
//                    pDialog.hide();
                    System.out.println(new String(responseBody));
                    Gson gson = new GsonBuilder().create();
                    rwsVo = gson.fromJson(responseBody, RdtstrptRwsVo.class);
                    // When the JSON response has status boolean value assigned with true
                    if (!TextUtils.isEmpty(rwsVo.getRptnum())) {
                        navigateToManualForm(vo);
                    }
                    // Else display error message
                    else {
                        //errorMsg.setText(obj.getString("error_msg"));
                        //showProgress(false);
                        Toast.makeText(getApplicationContext(), "Failed To Generate Report Number", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
//                pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void navigateToHomeForm() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }


    private void navigateToTableForm() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    private void navigateToLocationForm() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);

    }


}
