package co.za.ngwane.vdv;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.codehaus.jackson.map.ObjectMapper;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.rtmc.dto.Vehicle;
import co.za.rtmc.enums.AppConstants;
import co.za.rtmc.service.Validator;
import co.za.rtmc.vo.MvLicDiscVo;
import co.za.rtmc.vo.RdtstrptRwsVo;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class VehicleSearch extends AppCompatActivity  implements AdapterView.OnItemSelectedListener {
    EditText regNumberET;
    SweetAlertDialog pDialog;
    RdtstrptRwsVo rwsVo = new RdtstrptRwsVo();
    private Vehicle vehicle = null;
    String searhType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_search);
        Intent intent = getIntent();

        rwsVo = (RdtstrptRwsVo) intent.getSerializableExtra("rdtstrptRwsVo");
        regNumberET = (EditText) findViewById(R.id.mvRegNumET);
        regNumberET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(10)});
        regNumberET.setVisibility(View.INVISIBLE);

        final TextView txtView = (TextView)findViewById(R.id.searchBy);
        txtView.setVisibility(View.INVISIBLE);

        // Instantiate Progress Dialog object
        Spinner spinner1 = (Spinner) findViewById(R.id.idTypeSpinner);
        // Spinner click listener
        spinner1.setOnItemSelectedListener(this);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.searhTypes_array, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner1.setAdapter(adapter1);

        spinner1.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(
                            AdapterView<?> parent, View view, int position, long id) {
                        System.out.print(parent.getItemAtPosition(position));
                        searhType = parent.getItemAtPosition(position).toString();
                        // showToast("Spinner1: position=" + position + " id=" + id);
                        // idnumber input status is changed from number to text.
                        regNumberET.setText("");

                        if (searhType.equalsIgnoreCase(Validator.VIN_N)) {
                            txtView.setVisibility(View.VISIBLE);
                            txtView.setText("Vin/Chassis Number");
                            regNumberET.setVisibility(View.VISIBLE);
                            regNumberET.setInputType(InputType.TYPE_CLASS_TEXT);
                            regNumberET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});

                        } else {
                            regNumberET.setVisibility(View.VISIBLE);
                            txtView.setVisibility(View.VISIBLE);
                            txtView.setText("Vehicle Reg. Number");
                            regNumberET.setInputType(InputType.TYPE_CLASS_TEXT);
                            regNumberET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(10)});

                        }
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        // showToast("Spinner1: unselected");
                    }
                });

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_recents:
                        VehicleSearch.super.onBackPressed();

                        break;
//                    case R.id.action_favorites:
//                        Toast.makeText(ChargeCodeActivity.this, "Favorites", Toast.LENGTH_SHORT).show();
//                        break;
                    case R.id.action_nearby:
                        boolean cancel = false;
                        View focusView = null;

                        regNumberET = (EditText) findViewById(R.id.mvRegNumET);

                        if (TextUtils.isEmpty(regNumberET.getText().toString())) {
                            regNumberET.setError(getString(R.string.error_invalid_VehReg));
                            focusView = regNumberET;
                            cancel = true;
                        }


                        if (cancel) {
                            // There was an error; don't attempt login and focus the first
                            // form field with an error.
                            focusView.requestFocus();
                        } else {
                            MvLicDiscVo discVo = new MvLicDiscVo();
                            discVo.setMvRegN(regNumberET.getText().toString());

                            checkEnatisForVehicle( discVo );
                        }
                        break;

                }
                return true;
            }
        });



//        Button bPreviousButton = (Button) findViewById(R.id.prev_button);
//        bPreviousButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                onPreviousPressed();
//            }
//        });
//
//        Button mEmailSignInButton = (Button) findViewById(R.id.btnVDContinue);
//        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                boolean cancel = false;
//                View focusView = null;
//
//                regNumberET = (EditText) findViewById(R.id.mvRegNumET);
//
//                if (TextUtils.isEmpty(regNumberET.getText().toString())) {
//                    regNumberET.setError(getString(R.string.error_invalid_VehReg));
//                    focusView = regNumberET;
//                    cancel = true;
//                }
//
//
//                if (cancel) {
//                    // There was an error; don't attempt login and focus the first
//                    // form field with an error.
//                    focusView.requestFocus();
//                } else {
//                    MvLicDiscVo discVo = new MvLicDiscVo();
//                    discVo.setMvRegN(regNumberET.getText().toString());
//
//                    checkEnatisForVehicle( discVo );
//                }
//            }
//        });

    }

    public static InputFilter filter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                if (!Character.isLetterOrDigit(source.charAt(i))) {
                    return "";
                }
            }
            return null;
        }
    };


    public void onPreviousPressed() {
        super.onBackPressed();
        return;
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void checkEnatisForVehicle(final MvLicDiscVo mvLicDiscVo ) {
        // Show Progress Dialog
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();
        StringEntity entity = null;

        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        String appURL = "";
        //String appURL = AppConstants.BACK_OFFICE_APP_URL+ "v1/enatis/X1001Vehicle/" + mvLicDiscVo.getMvRegN();

        if (searhType.equalsIgnoreCase(Validator.VIN_N)) {
            //appURL = "http://172.20.10.3:8080/backoffice/svs/v1/enatis/X1007VehicleVin/vinOrChassis/" + mvLicDiscVo.getMvRegN();
            appURL = AppConstants.BACK_OFFICE_APP_URL+ "v1/enatis/X1007VehicleVin/vinOrChassis/" + mvLicDiscVo.getMvRegN();
        } else {
            //appURL =  "http://172.20.10.3:8080/backoffice/svs/v1/enatis/X1001Vehicle/" + mvLicDiscVo.getMvRegN();
            appURL = AppConstants.BACK_OFFICE_APP_URL+ "v1/enatis/X1001Vehicle/" + mvLicDiscVo.getMvRegN();
        }


//                AsyncHttpClient client = new AsyncHttpClient();
//                String appURL = AppConstants.BACK_OFFICE_LOCAL_APP_URL+ "v1/enatis/X1001Vehicle/" + mvLicDiscVo.getMvRegN();
        client.get(appURL, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    // JSON Object
                    pDialog.hide();
                    System.out.println(new String(responseBody));
                    Gson gson = new GsonBuilder().create();
                    vehicle = gson.fromJson(responseBody, Vehicle.class);
                    // When the JSON response has status boolean value assigned with true

                    if ( responseBody.isEmpty() ) {
                        mvLicDiscVo.setEnatisNoData(true);
                        mvLicDiscVo.setMvDescCdDesc( null );
                        Toast.makeText(getApplicationContext(), "Data not found on eNaTIS..!", Toast.LENGTH_LONG).show();
//                        showFail( mvLicDiscVo );
                    } else {
                        ObjectMapper mapper = new ObjectMapper();
                        vehicle = mapper.readValue( responseBody , Vehicle.class);
                        mvLicDiscVo.setMvDescCdDesc(  vehicle.getVehicleDet().getMvcat().getDesc());
                        mvLicDiscVo.setMvRegN(vehicle.getVehicleDet().getLicN());
                        mvLicDiscVo.setRwStatus(vehicle.getVehicleDet().getRwstat().getDesc());
                        mvLicDiscVo.setSapMark(vehicle.getVehicleDet().getSapmark().getDesc());
                        mvLicDiscVo.setColor(vehicle.getVehicleDet().getMainColour().getDesc());
                        mvLicDiscVo.setVin(vehicle.getVehicleDet().getVinOrChassis());
                        mvLicDiscVo.setMake(vehicle.getVehicleDet().getMake().getDesc());
                        mvLicDiscVo.setModel(vehicle.getVehicleDet().getModelName().getDesc());


                        mvLicDiscVo.setExpiry(vehicle.getVehicleDet().getMvlicExpiryD());
                        mvLicDiscVo.setMvState(vehicle.getVehicleDet().getMvstate().getDesc());

                        mvLicDiscVo.setMvDescCdDesc( vehicle.getVehicleDet().getMvcat().getDesc() );
                        mvLicDiscVo.setRwStatus(vehicle.getVehicleDet().getRwstat().getDesc());
                        mvLicDiscVo.setSapMark(vehicle.getVehicleDet().getSapmark().getDesc());

                        if ( !vehicle.getVehicleDet().getEngineN().equalsIgnoreCase(mvLicDiscVo.getEngineNo()) ||
                                !vehicle.getVehicleDet().getVinOrChassis().equalsIgnoreCase(mvLicDiscVo.getVin())  ||
                                !vehicle.getVehicleDet().getMake().getDesc().equalsIgnoreCase(mvLicDiscVo.getMake())) {
                            mvLicDiscVo.setEnatisNoMatch(false);



//                            showWarning(mvLicDiscVo);

                        }

                        //set the owner details for display in the next page
                        if ( vehicle.getOwner() != null) {

                            mvLicDiscVo.setBusOrSurname(vehicle.getOwner().getPerDet().getPerId().getBusOrSurname());
                            mvLicDiscVo.setIdDocN(vehicle.getOwner().getPerDet().getPerId().getIdDocN());
                            mvLicDiscVo.setInitials(vehicle.getOwner().getPerDet().getPerId().getInitials());
                            mvLicDiscVo.setDesc(vehicle.getOwner().getPerDet().getPerId().getIdDocType().getDesc());

                            mvLicDiscVo.setAddr1(vehicle.getOwner().getPerDet().getStreetAddr().getAddr1());
                            mvLicDiscVo.setAddr2(vehicle.getOwner().getPerDet().getStreetAddr().getAddr2());
                            mvLicDiscVo.setAddr3(vehicle.getOwner().getPerDet().getStreetAddr().getAddr3());
                            mvLicDiscVo.setPostalCd(vehicle.getOwner().getPerDet().getStreetAddr().getPostalCd().getCode());
                        }

                        navigateToManualForm( mvLicDiscVo );
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Vehicle not found on eNaTIS..!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Internal Server error.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 202) {
                    Toast.makeText(getApplicationContext(), "Vehicle not found on eNaTIS..!", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }

                MvLicDiscVo  mvLicDiscVo = new MvLicDiscVo();
                mvLicDiscVo.setMvRegN(mvLicDiscVo.getRegtN());
                mvLicDiscVo.setMvDescCdDesc( null );
                // navigateToManualForm( mvLicDiscVo );

            }
        });

    }

    private void navigateToManualForm(MvLicDiscVo vo) {
        Intent intent = new Intent(this, VehicleSearchResultsActivity.class);
        intent.putExtra("discVo", vo);
        intent.putExtra("rdtstrptRwsVo", rwsVo);
        startActivity(intent);

    }


}
