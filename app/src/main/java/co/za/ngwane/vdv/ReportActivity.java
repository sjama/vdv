package co.za.ngwane.vdv;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.codehaus.jackson.map.ObjectMapper;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.rtmc.dto.RoadSafety;
import co.za.rtmc.enums.AppConstants;
import co.za.rtmc.service.LocationAddress;
import co.za.rtmc.service.SessionManager;
import co.za.rtmc.service.Validator;
import co.za.rtmc.vo.AppLocationService;
import co.za.rtmc.vo.BoRoadSafetyInspection;
import co.za.rtmc.vo.Lmidtypecd;
import co.za.rtmc.vo.MvLicDiscVo;
import co.za.rtmc.vo.RdtstrptRwsVo;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.READ_PHONE_STATE;

/**
 * Created by Jama on 2016/04/26.
 */
public class ReportActivity extends MainActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener, View.OnFocusChangeListener {


    // Progress Dialog Object
    ProgressDialog prgDialog;
    // Error Msg TextView Object
    TextView errorMsg;
    AppLocationService appLocationService;


    // Name Edit View Object
    EditText surnameET;
    // Name Edit View Object
    EditText nameET;
    // Email Edit View Object
    EditText driverLicCodeET;
    // Passwprd Edit View Object
    EditText idNumberET;
    EditText mvTypeET;
    EditText expDateET;
    String licCode = "";
    String dlIdType = "";
    String action = "";
    private Context mContext;
    EditText prdpExpDateET;
    EditText prdpET;

    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    MvLicDiscVo discVo = new MvLicDiscVo();
    private CoordinatorLayout coordinatorLayout;
    BoRoadSafetyInspection roadSafetyInspection;
            SweetAlertDialog pDialog;
    Location gpsLocation;
    String[] addDetails;


    String regNumber = "";
    private RdtstrptRwsVo rwsVo = new RdtstrptRwsVo();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        appLocationService = new AppLocationService( ReportActivity.this);
//        if (locationActivity.requestLocationPerm()) {
//        } else {
//            locationActivity.showSettingsAlert();
//        }
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        Intent intent = getIntent();
        gpsLocation = getLocation();

        try {
            discVo = (MvLicDiscVo) intent.getSerializableExtra("discVo");
            rwsVo = (RdtstrptRwsVo) intent.getSerializableExtra("rdtstrptRwsVo");
            roadSafetyInspection = (BoRoadSafetyInspection) intent.getSerializableExtra("inspectionData");


        expDateET = (EditText) findViewById(R.id.regNumber);
        if ( rwsVo.getDriLicValidToD() != null ) {
            expDateET.setText(rwsVo.getDriLicValidToD());
            expDateET.setEnabled(false);

            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date targetD = sdf.parse(rwsVo.getDriLicValidToD());
            Date date2 = new Date();

            if ( Validator.isDateOutOfRange(targetD, new Date(), Calendar.MONTH, 3 ) ) {
                expDateET.setBackgroundColor(Color.GREEN);
            } else if (  Validator.isDateOutOfRange(targetD, new Date(), Calendar.MONTH, 2 )) {
                expDateET.setBackgroundColor(Color.YELLOW);
            } else if ( targetD.compareTo(date2) < 0 ) {
                expDateET.setBackgroundColor( Color.RED );
            }

            setDateTimeField();

        }
            prdpET = (EditText) findViewById(R.id.driverPdps);
            prdpET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});
            prdpET.setEnabled(false);

            if ( rwsVo.getDriPrDPs() != null  ) {
                prdpET.setText( rwsVo.getDriPrDPs() );
                prdpET.setEnabled(false);
            } else {
                prdpET.setText( "N/A" );
            }

            prdpExpDateET = (EditText) findViewById(R.id.prdpEDate);
            if ( rwsVo.getDriPrDPValidToD() != null ) {
                prdpExpDateET.setText(rwsVo.getDriPrDPValidToD());
                prdpExpDateET.setEnabled(false);

                SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date targetD = sdf.parse(rwsVo.getDriPrDPValidToD());
                Date date2 = new Date();

                if ( Validator.isDateOutOfRange(targetD, new Date(), Calendar.MONTH, 3 ) ) {
                    prdpExpDateET.setBackgroundColor(Color.GREEN);
                } else if (  Validator.isDateOutOfRange(targetD, new Date(), Calendar.MONTH, 2 )) {
                    prdpExpDateET.setBackgroundColor(Color.YELLOW);
                } else if ( targetD.compareTo(date2) < 0 ) {
                    prdpExpDateET.setBackgroundColor( Color.RED );
                }

                setDateTimeField();

            } else {
                prdpExpDateET.setText( "N/A" );
                prdpExpDateET.setEnabled(false);

            }

        }catch (Exception e){
            e.printStackTrace();

        }

        errorMsg = (TextView) findViewById(R.id.register_error);
        // Find Name Edit View control by ID

        surnameET = (EditText) findViewById(R.id.driverSurName);
        surnameET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(25)});

        if ( rwsVo.getDrisurname() != null  ) {
            surnameET.setText(rwsVo.getDrisurname());
            surnameET.setEnabled(false);
        }


        nameET = (EditText) findViewById(R.id.driverName);
        nameET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(3)});
        if ( rwsVo.getDriname() != null  ) {
            nameET.setText(rwsVo.getDriname());
            nameET.setEnabled(false);
        }

        driverLicCodeET = (EditText) findViewById(R.id.driverLics);
        driverLicCodeET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(20)});

        if ( rwsVo.getLiccd() != null  ) {
            driverLicCodeET.setText( rwsVo.getLiccd() );
            driverLicCodeET.setEnabled(false);
        }

        // driverLicCodeET = (EditText) findViewById(R.id.driverLicCode);
        idNumberET = (EditText) findViewById(R.id.idNumber);
        idNumberET.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        if ( rwsVo.getDriidn() != null  ) {
            idNumberET.setText(rwsVo.getDriidn());
            idNumberET.setEnabled(false);
        }

        final Spinner spinner1 = (Spinner) findViewById(R.id.idTypeSpinner);
        // Spinner click listener
        spinner1.setOnItemSelectedListener(this);
        // Create an ArrayAdapter using the string array and a default spinner layout
        final ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this,
                R.array.idType_array, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner1.setAdapter(adapter1);

        spinner1.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(
                            AdapterView<?> parent, View view, int position, long id) {
                        System.out.print(parent.getItemAtPosition(position));
                        dlIdType = parent.getItemAtPosition(position).toString();
                        if (  rwsVo.getDrividtypecd() != null ) {
                            int spinnerPosition = adapter1.getPosition(rwsVo.getDrividtypecd());
                            spinner1.setSelection(spinnerPosition);
                            spinner1.setEnabled(false);
                        }
                        // showToast("Spinner1: position=" + position + " id=" + id);
                        // idnumber input status is changed from number to text.
                        if (dlIdType.equalsIgnoreCase(Validator.RSA_IDENTITY_DOCUMENT)) {
                            // show password
                            idNumberET.setInputType(InputType.TYPE_CLASS_NUMBER);
                        } else {
                            // hide password
                            idNumberET.setInputType(InputType.TYPE_CLASS_TEXT);
                        }
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        // showToast("Spinner1: unselected");
                    }
                });

        final Spinner actionSpinner = (Spinner) findViewById(R.id.actionTypeSpinner);

        final ArrayAdapter<CharSequence> actionAdapter = ArrayAdapter.createFromResource(this,
                R.array.action_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        actionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        actionSpinner.setAdapter(actionAdapter);
        actionSpinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    public void onItemSelected(
                            AdapterView<?> parent, View view, int position, long id) {
                        System.out.print(parent.getItemAtPosition(position));
                        action = parent.getItemAtPosition(position).toString();
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                        // showToast("Spinner1: unselected");
                    }
                });

        Button mEmailSignInButton = (Button) findViewById(R.id.btnSubmit);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                populateVo(rwsVo);
            }
        });



    }

    private void navigateToHomeForm() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }


    private void navigateToTableForm() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    private void navigateToLocationForm() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);

    }

    private void setDateTimeField() {
        expDateET.setOnClickListener(this);
        expDateET.setOnFocusChangeListener(this);
        expDateET.setImeOptions(EditorInfo.IME_ACTION_DONE);


        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                expDateET.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


    }

    private void populateVo(RdtstrptRwsVo rwsVo) {

        View focusView = null;
        boolean cancel = false;
        String idNumber = idNumberET.getText().toString();
        String driverName = nameET.getText().toString();
        // String licCode = driverLicCodeET.getText().toString();


        if (TextUtils.isEmpty(driverName)) {
            nameET.setError("Please enter driver name");
            focusView = nameET;
            cancel = true;
        }

        if (TextUtils.isEmpty(surnameET.getText().toString())) {
            surnameET.setError("Please enter driver Surname.");
            focusView = surnameET;
            cancel = true;
        }

        if (TextUtils.isEmpty(idNumber)) {
            idNumberET.setError("Driver Id Number Required");
            focusView = idNumberET;
            cancel = true;
        } else {
            if (!Validator.isIdDocNValidLength( dlIdType.trim() , idNumber )) {
                idNumberET.setError("This type of identification number requires 13 characters.");
                focusView = idNumberET;
                focusView.requestFocus();
                return;
            }

            if (!Validator.isCheckDigitValid( dlIdType.trim() , idNumber )) {
                idNumberET.setError("Identification number invalid.");
                focusView = idNumberET;
                focusView.requestFocus();
                return;
            }

        }


       /* if (TextUtils.isEmpty(licCode) || licCode.trim().equalsIgnoreCase("Select DL Code")) {
            Toast.makeText(ReportActivity.this,
                    "Please select DL Code.!!", Toast.LENGTH_LONG)
                    .show();
            return;
        }
*/


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            rwsVo.setMvreg(discVo != null? discVo.getMvRegN(): regNumber);
            rwsVo.setDriname(driverName);

            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SessionUser", MODE_PRIVATE);
            rwsVo.setUsername(sharedPref.getString("phephaUser",null));
            //proceedOption();
            invokeWS(idNumber, regNumber, licCode, driverName);

        }
    }

    private void initScan(RdtstrptRwsVo vo) {
        Intent intent = new Intent(this, SimpleScannerActivity.class);
        intent.putExtra("rdtstrptRwsVo", vo);
        startActivity(intent);
        //intentIntegrator.initiateScan(IntentIntegrator.ALL_CODE_TYPES)

    }

    public void proceedOption(){
        String message =" How would you like to proceed?";
        new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
                .setTitleText("")
                .setContentText(message)
                .setConfirmText("Scan Disk")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        initScan(rwsVo);
                    }
                })
                .setCancelText("Manual Capture")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        navigateToVehicleDetailsForm(rwsVo);
                    }
                })
                .show();

    }

    private void navigateToVehicleDetailsForm(RdtstrptRwsVo vo) {
        Intent intent = new Intent(this, VehicleSearch.class);
        intent.putExtra("rdtstrptRwsVo", vo);
        startActivity(intent);
    }



    private void navigateToManualForm(RoadSafety vo) {
        Intent intent = new Intent(this, HistoryActivity.class);
        intent.putExtra("inspectionDataList", vo);
        startActivity(intent);



    }

    public void invokeWS(String idNumber, String regNumber, String licCode, String driverName) {
// Show Progress Dialog
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();
        StringEntity entity = null;
        try {

            roadSafetyInspection.setDriverSurname( rwsVo.getDrisurname());
            roadSafetyInspection.setDriverName( rwsVo.getDriname() );
            roadSafetyInspection.setIdNumber( rwsVo.getDriidn() );
            roadSafetyInspection.setActionStatus(action);
            roadSafetyInspection.setInfrastructureNumber(rwsVo.getUsername());

            double latitude = gpsLocation.getLatitude();

            roadSafetyInspection.setGpsXCoordinates( String.valueOf(latitude));
            roadSafetyInspection.setGpsYCoordinates( String.valueOf(gpsLocation.getLongitude()));
            roadSafetyInspection.setAddressData(Arrays.toString(addDetails));
            roadSafetyInspection.setLocationName(addDetails[3]);

            Lmidtypecd lmidtypecd = new Lmidtypecd();
            lmidtypecd.setCode( rwsVo.getDrividtypecd() );


            roadSafetyInspection.setIdTypeCd( lmidtypecd );

            //entity = new ByteArrayEntity(new JSONObject().toString().getBytes("UTF-8"));
            Gson gson = new GsonBuilder().create();
            entity = new StringEntity(gson.toJson(roadSafetyInspection));
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        //AsyncHttpClient client = new AsyncHttpClient();

        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        // client.post(getApplicationContext(),"http://10.0.2.2:9090/phepha/svs/userspRws/doLogin", entity,"application/json", new AsyncHttpResponseHandler() {

        client.post(getApplicationContext(), AppConstants.BACK_OFFICE_BO_APP_URL + "/saveRoadInspection", entity, "application/json", new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    // JSON Object
                    pDialog.hide();
                    System.out.println(new String(responseBody));
                    Gson gson = new GsonBuilder().create();
                    ObjectMapper mapper = new ObjectMapper();
                    RoadSafety roadSafetyInspection = mapper.readValue( responseBody , RoadSafety.class);
                    //RoadSafety roadSafetyInspection = gson.fromJson(responseBody, RoadSafety.class);
                   // RdtstrptRwsVo rwsVo = gson.fromJson(responseBody, RdtstrptRwsVo.class);
                    // When the JSON response has status boolean value assigned with true
                    if (!roadSafetyInspection.getRoadSafetyInspections().isEmpty() ) {
                        navigateToManualForm(roadSafetyInspection);
                    }
                    // Else display error message
                    else {
                        showSuccess();
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
               // pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to BO.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to BO.", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Could not get connection to BO.", Toast.LENGTH_LONG).show();
                }

                navigateToHomeForm();

            }
        });
    }

    private void  showSuccess(){

        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Road Test Report")
                .setContentText("Road Test Results Added Successfully")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        navigateToHomeForm();
                    }
                })
                .show();


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        System.out.print(parent.getItemAtPosition(position));
        licCode = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public Location getLocation(){

        if(requestLocationPerm()) {
            gpsLocation = appLocationService.getLocation(LocationManager.GPS_PROVIDER, getApplicationContext());
            if (gpsLocation != null) {
                double latitude = gpsLocation.getLatitude();
                double longitude = gpsLocation.getLongitude();
                String result = "Latitude: " + gpsLocation.getLatitude() +
                        " Longitude: " + gpsLocation.getLongitude();
                LocationAddress.getAddressFromLocation(latitude,longitude,getApplicationContext(),new GeocoderHandler());
            } else {
                // showSettingsAlert();
            }
        }

        return gpsLocation;

    }

    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    addDetails =  locationAddress.split("\n");
                    //String prov= addDetails[9];
                    //setProvinceCD(prov);

                    break;
                default:
                    locationAddress = null;
            }
            // tvAddress.setText(locationAddress);
        }
    }

    @Override
    public void onBackPressed() {
    }

    public void onPreviousPressed() {
        new SessionManager(getApplicationContext()).SavePreferences(true);
        super.onBackPressed();
        return;
    }

    @Override
    public void onClick(View v) {

        if(v == expDateET) {
            fromDatePickerDialog.show();
        }
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(v == expDateET && hasFocus) {
            fromDatePickerDialog.show();
        }
    }

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private boolean requestLocationPerm() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission = checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showMessageOKCancel("You need to allow access to Location",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                                                REQUEST_CODE_ASK_PERMISSIONS);
                                    }
                                }
                            });
                }
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
        // insertDummyContact();
        return false;
    }


    /// final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private boolean requestPhoneStatePerm() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission = checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.READ_PHONE_STATE)) {
                    showMessageOKCancel("You need to allow access to phone state",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                                                REQUEST_CODE_ASK_PERMISSIONS);
                                    }
                                }
                            });
                }
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
        // insertDummyContact();
        return false;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(ReportActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    getLocation();
                } else {
                    // Permission Denied
                    Toast.makeText(ReportActivity.this, "LOCATION ACCESS Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void showSettingsAlert() {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Location Service")
                .setContentText("Unable to determine your location : Please ensure your GPS is turned on")
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        ReportActivity.this.startActivity(intent);
                    }
                })
                .show();

    }


}
