package co.za.ngwane.vdv;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;
import java.util.List;

import co.za.ngwane.vdv.recycleview.InfringementAdapter;
import co.za.ngwane.vdv.recycleview.InfringementGroupAdapter;
import co.za.ngwane.vdv.recycleview.RecyclerTouchListener;
import co.za.rtmc.vo.GroupDTO;
import co.za.rtmc.vo.Infringement;
import co.za.rtmc.vo.InfringementsGroup;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class InfringeGroupActivity extends AppCompatActivity {
    private List<Infringement> infringementList = new ArrayList<>();
    private List<GroupDTO> groupDTOS = new ArrayList<>();
    private RecyclerView recyclerView;
    private InfringementGroupAdapter mAdapter;
    private InfringementsGroup infringementsGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inf_group);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        groupDTOS = this.getIntent().getExtras().getParcelableArrayList("groupDTOS");
        Gson gson = new GsonBuilder().create();
        infringementsGroup = gson.fromJson(this.getIntent().getExtras().getString("infringementsGroup"), InfringementsGroup.class);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_group);

        mAdapter = new InfringementGroupAdapter(groupDTOS,this);

        recyclerView.setHasFixedSize(true);

        // vertical RecyclerView
        // keep movie_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        // horizontal RecyclerView
        // keep movie_list_row.xml width to `wrap_content`
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        // adding custom divider line with padding 16dp
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);

        // row click listener
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                GroupDTO groupDTO = groupDTOS.get(position);
                switch (groupDTO.getGroupName()){
                    case "Infringement notice" :
                        if (infringementsGroup.getInfringements().size() > 0){
                            navigateToInfringeList(infringementsGroup.getInfringements());
                        }
                        else{
                            Toast.makeText(getApplicationContext(), groupDTO.getGroupName() + " has no records to show", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case "Warrant of execution" :
                        if (infringementsGroup.getWarrantStatusList().size() > 0) {
                            navigateToInfringeList(infringementsGroup.getWarrantStatusList());
                        }else{
                            Toast.makeText(getApplicationContext(), groupDTO.getGroupName() + " has no records to show", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case "Enforcement order" :
                        if(infringementsGroup.getEnforcementStatusList().size() > 0) {
                            navigateToInfringeList(infringementsGroup.getEnforcementStatusList());
                        }else{
                            Toast.makeText(getApplicationContext(), groupDTO.getGroupName() + " has no records to show", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    case "Court case" :
                        if(infringementsGroup.getCourtStatusList().size() > 0) {

                            navigateToInfringeList(infringementsGroup.getCourtStatusList());
                        }else{
                            Toast.makeText(getApplicationContext(), groupDTO.getGroupName() + " has no records to show", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:

                }
                Toast.makeText(getApplicationContext(), groupDTO.getGroupName() + " is selected!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

      //  getInfringements();
    }

    private void navigateToInfringeList(List<Infringement>  infringements) {
        Intent intent = new Intent(this, MainActivity.class);
        Bundle bundle = new Bundle();
        Gson gson = new GsonBuilder().create();
        bundle.putParcelableArrayList("infringements", (ArrayList<? extends Parcelable>) infringements);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    /**
     * Prepares sample data to provide data set to adapter
     * Tobe Replaced with Webservice Call
     */
    private void getInfringements() {
        Infringement movie = new Infringement("Notice Number: 001010101010",
                "Date : 2015-01-23", "Status : Warrant","Lic No : XYZ137GP");
        infringementList.add(movie);

        movie = new Infringement("Notice Number: 828282828282",
                "Date : 2015-01-23", "Status : Infrigment","Lic No : XYZ137GP");
        infringementList.add(movie);
        getWebserviceResposnse();


        // notify adapter about data set changes
        // so that it will render the list with new data
        mAdapter.notifyDataSetChanged();
    }


    public void getWebserviceResposnse( ) {

        StringEntity entity = null;
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
       // String appURL = AppConstants.BACK_OFFICE_APP_URL+ "v1/enatis/X1001Vehicle/" + mvLicDiscVo.getMvRegN();
        String appURL = "http://192.168.2.4:8080/backoffice/infringeList/02/8512265513081/40630010Z7RT";
        int[] covers = new int[]{
                R.drawable.courthouse,
                R.drawable.law,
                R.drawable.policestation,
                R.drawable.handcuffs,
               };

        client.get(appURL, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    System.out.println(new String(responseBody));
                    Gson gson = new GsonBuilder().create();
                    InfringementsGroup infringementsGroup = gson.fromJson(responseBody,InfringementsGroup.class);
                    GroupDTO groupDTO = new GroupDTO();
                    groupDTO.setGroupName("Infringement notice");
                    groupDTO.setGroupTotal(infringementsGroup.getInfringements().size()+"");
                    groupDTOS.add(groupDTO);
                    groupDTO.setGroupName("Warrant of execution");
                    groupDTO.setGroupTotal(infringementsGroup.getWarrantStatusList().size()+"");
                    groupDTOS.add(groupDTO);
                    groupDTO.setGroupName("Enforcement order");
                    groupDTO.setGroupTotal(infringementsGroup.getEnforcementStatusList().size()+"");
                    groupDTOS.add(groupDTO);
                    groupDTO.setGroupName("Court case");
                    groupDTO.setGroupTotal(infringementsGroup.getCourtStatusList().size()+"");
                    groupDTOS.add(groupDTO);


                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
//                pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

}
