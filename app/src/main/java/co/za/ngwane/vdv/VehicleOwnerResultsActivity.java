package co.za.ngwane.vdv;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import co.za.rtmc.dto.VehicleDet;
import co.za.rtmc.service.SessionManager;
import co.za.rtmc.vo.AppLocationService;
import co.za.rtmc.vo.MvLicDiscVo;
import co.za.rtmc.vo.RdtstrptRwsVo;

/**
 * Created by Jama on 2016/06/09.
 */
public class VehicleOwnerResultsActivity extends MainActivity  {
    AppLocationService appLocationService;
    Location gpsLocation;
    String[] addDetails;
    EditText mvOwnIDTypeET;
    EditText mvOwnerIdNET;
    EditText mvOwnerNameET;
    EditText mvOwnerSurNameET;
    EditText mvOwnerAddLine1ET;
    EditText mvOwnerAddLine2ET;
    EditText mvOwnerAddLine3ET;
    EditText mvOwnerAddLine4ET;

    private String menuCd;

    MvLicDiscVo mvLicDiscVo = new MvLicDiscVo();

    private VehicleDet vehicle = null;
    SessionManager session;


    private RdtstrptRwsVo rwsVo = new RdtstrptRwsVo();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vehicle_owner_search_result);
        session = new SessionManager(getApplicationContext());
        menuCd = session.getMenuCd();

        Intent intent = getIntent();

        rwsVo = (RdtstrptRwsVo) intent.getSerializableExtra("rdtstrptRwsVo");
        final MvLicDiscVo discVo  = (MvLicDiscVo) intent.getSerializableExtra("discVo");


        mvOwnIDTypeET = (EditText) findViewById(R.id.mvOwnIDTypeET);
//        mvOwnIDTypeET.setFilters(new InputFilter[] {new InputFilter.AllCaps()});

        mvOwnerIdNET = (EditText) findViewById(R.id.mvOwnerIdNET);
        mvOwnerIdNET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(30)});


        mvOwnerNameET = (EditText) findViewById(R.id.mvOwnerNameET);
        mvOwnerNameET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(50)});

        mvOwnerSurNameET = (EditText) findViewById(R.id.mvOwnerSurNameET);
        mvOwnerSurNameET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(15)});

        mvOwnerAddLine1ET = (EditText) findViewById(R.id.mvOwnerAddLine1ET);
        mvOwnerAddLine1ET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(50)});

        mvOwnerAddLine2ET = (EditText) findViewById(R.id.mvOwnerAddLine2ET);
        mvOwnerAddLine2ET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(50)});

        mvOwnerAddLine3ET = (EditText) findViewById(R.id.mvOwnerAddLine3ET);
        mvOwnerAddLine3ET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(50)});

        mvOwnerAddLine4ET = (EditText) findViewById(R.id.mvOwnerAddLine4ET);
        // mvOwnerAddLine4ET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(50)});




        try {
            mvOwnIDTypeET.setText(discVo.getDesc());
            mvOwnIDTypeET.setEnabled(false);

            mvOwnerIdNET.setText(String.valueOf(discVo.getIdDocN()));
            mvOwnerIdNET.setEnabled(false);



            mvOwnerNameET.setText(discVo.getInitials());
            mvOwnerNameET.setEnabled(false);

            mvOwnerSurNameET.setText(discVo.getBusOrSurname());
            mvOwnerSurNameET.setEnabled(false);

            mvOwnerAddLine1ET.setText(discVo.getAddr1());
            mvOwnerAddLine1ET.setEnabled(false);

            mvOwnerAddLine2ET.setText(discVo.getAddr2());
            mvOwnerAddLine2ET.setEnabled(false);

            mvOwnerAddLine3ET.setText(discVo.getAddr3());
            mvOwnerAddLine3ET.setEnabled(false);

            mvOwnerAddLine4ET.setText(discVo.getPostalCd());
            mvOwnerAddLine4ET.setEnabled(false);

        }catch (Exception e){
            e.printStackTrace();

        }



        Button mEmailSignInButton = (Button) findViewById(R.id.btnVDContinue);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_QUERY_DRIVER ) ) {
                    navigateToHomeForm();
                } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_ISSUE_FINE) )  {
                    navigateToCharge();
                } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_VEHICLE) )  {
                    navigateToHomeForm();
                } else if (menuCd.equalsIgnoreCase(SessionManager.KEY_MENU_VERIFY_DRIVER) )  {
                    navigateToHomeForm();
                }

            }
        });

    }

    private void navigateToCharge() {

        Intent intent = new Intent(this,ChargeCodeActivity.class);
        startActivity(intent);

    }

    @Override
    public void onBackPressed() {
    }


    private void navigateToHomeForm() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }


    private void navigateToTableForm() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

    private void navigateToLocationForm() {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);

    }


}
