package co.za.ngwane.vdv.recycleview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import co.za.ngwane.vdv.R;
import co.za.rtmc.vo.GroupDTO;
import co.za.rtmc.vo.Infringement;

public class InfringementGroupAdapter extends RecyclerView.Adapter<InfringementGroupAdapter.MyViewHolder> {

    private List<GroupDTO> groupDTOS;
    private Context mContext;
    int[] images = new int[]{
            R.drawable.courthouse,
            R.drawable.law,
            R.drawable.policestation,
            R.drawable.handcuffs,
    };

    public InfringementGroupAdapter(List<GroupDTO> groupDTOS, Context mContext) {
        this.groupDTOS = groupDTOS;
        this.mContext = mContext;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView groupName, groupTotal;
        public ImageView groupImg;

        public MyViewHolder(View view) {
            super(view);
            groupName = (TextView) view.findViewById(R.id.group_name);
            groupTotal = (TextView) view.findViewById(R.id.group_total);
            groupImg = (ImageView)  view.findViewById(R.id.image);
        }
    }


    public InfringementGroupAdapter(List<GroupDTO> groupDTOS) {
        this.groupDTOS = groupDTOS;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.infrigement_group_row_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        GroupDTO groupDTO = groupDTOS.get(position);
        holder.groupName.setText(groupDTO.getGroupName());
        holder.groupTotal.setText(groupDTO.getGroupTotal());
        switch (groupDTO.getGroupName()){
            case "Infringement notice" :
                Glide.with(mContext).load(images[1]).into(holder.groupImg);
                break;
            case "Warrant of execution" :
                Glide.with(mContext).load(images[3]).into(holder.groupImg);
                break;
            case "Enforcement order" :
                Glide.with(mContext).load(images[2]).into(holder.groupImg);
                break;
            case "Court case" :
                Glide.with(mContext).load(images[0]).into(holder.groupImg);
                break;
            default:
                System.out.print("Failed Image");
        }

    }

    @Override
    public int getItemCount() {
        return groupDTOS.size();
    }
}