package co.za.ngwane.vdv;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.rtmc.vo.ChargeDetails;
import co.za.rtmc.vo.GroupDTO;
import co.za.rtmc.vo.InfringementsGroup;
import co.za.rtmc.vo.Ltcnt;
import co.za.rtmc.vo.MvLicDiscVo;
import co.za.rtmc.vo.RdtstrptRwsVo;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ChargeCodeActivity extends AppCompatActivity {


    private EditText mChargeCodeView;
    private EditText mSpeedET;
    private TextView mSpeedTV;
    private SweetAlertDialog pDialog;
    private String cntcode;
    private String speadReading;
    private Ltcnt ltcnt;
    private ChargeDetails chargeDetails;
    RadioButton speedFine,normalFine;
    RadioGroup radioGroup;
    MvLicDiscVo discVo = new MvLicDiscVo();
    private RdtstrptRwsVo rwsVo = new RdtstrptRwsVo();
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    EditText emailId;
    EditText cellNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charge_code);
        Intent intent = getIntent();

        mChargeCodeView = (EditText) findViewById(R.id.chargecodeET);
        mChargeCodeView.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(5)});
        mChargeCodeView.setTextColor(Color.BLACK);
        mChargeCodeView.setEnabled(true);

        mSpeedET = (EditText) findViewById(R.id.speedET);
        mSpeedET.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(5)});
        mSpeedET.setTextColor(Color.BLACK);
        mSpeedET.setEnabled(true);
        mSpeedET.setVisibility(View.GONE);

        discVo = (MvLicDiscVo) intent.getSerializableExtra("discVo");
        rwsVo = (RdtstrptRwsVo) intent.getSerializableExtra("rdtstrptRwsVo");
        mSpeedTV = (TextView) findViewById(R.id.speedTV);
        mSpeedTV.setVisibility(View.GONE);

        speedFine = (RadioButton)findViewById(R.id.radMale);
        normalFine = (RadioButton)findViewById(R.id.radFemale);
        radioGroup = (RadioGroup)findViewById(R.id.radGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(speedFine.isChecked())
                {
                    mSpeedET.setVisibility(View.VISIBLE);
                    mSpeedTV.setVisibility(View.VISIBLE);
                }
                else {
                    mSpeedET.setVisibility(View.GONE);
                    mSpeedTV.setVisibility(View.GONE);
                }


            }
        });

        emailId = findViewById(R.id.emailET);
        cellNo = findViewById(R.id.cellNoET);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                View focusView = null;
                boolean cancel= false;
                switch (item.getItemId()) {
                    case R.id.action_recents:
                        ChargeCodeActivity.super.onBackPressed();

                        break;
//                    case R.id.action_favorites:
//                        Toast.makeText(ChargeCodeActivity.this, "Favorites", Toast.LENGTH_SHORT).show();
//                        break;
                    case R.id.action_nearby:
                        cntcode = mChargeCodeView.getText().toString();
                        if (TextUtils.isEmpty(cntcode)) {
                            mChargeCodeView.setError("Please enter code");
                            focusView = mChargeCodeView;
                            cancel = true;
                        }
                        if (speedFine.isChecked()){
                            speadReading= mSpeedET.getText().toString();
                            if (TextUtils.isEmpty(speadReading)) {
                                mSpeedET.setError("Please enter speed reading");
                                focusView = mSpeedET;
                                cancel = true;
                            }

                        }

                        if(!emailId.getText().toString().isEmpty()) {
                            if (emailId.getText().toString().trim().matches(emailPattern)) {
                                focusView = emailId;
                                Toast.makeText(getApplicationContext(),"valid email address",Toast.LENGTH_SHORT).show();
                                emailId.setError("valid email address");
                                cancel =true;
                            } else {
                                Toast.makeText(getApplicationContext(),"Invalid email address", Toast.LENGTH_SHORT).show();
                                focusView = emailId;
                                emailId.setError("Invalid email address");
                                cancel =true;
                            }
                        }

                        if (cancel) {
                            focusView.requestFocus();
                        }else{
                            getAPIResponse();
                        }


                        break;

                }

                return true;
            }
        });

    }


    public void getAPIResponse() {

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        StringEntity entity = null;
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        String appURL = "http://10.10.0.3:8080/backoffice/cntcode/" + cntcode;

        client.get(appURL, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    pDialog.hide();
                    System.out.println(new String(responseBody));
                    Gson gson = new GsonBuilder().create();
                    chargeDetails = gson.fromJson(responseBody,ChargeDetails.class);


                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                if(chargeDetails.getLtcnt() == null ){
                    View focusView = null;
                    boolean cancel= false;
                    mChargeCodeView.setError("Invalid Charge Code entered");
                    focusView = mChargeCodeView;
                    focusView.requestFocus();
                    return;
                }else{
                    navigateToGroup();
                }



            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private void navigateToGroup() {
        Intent intent = new Intent(this, ChargeDetailsActivity.class);
        Bundle bundle = new Bundle();
       // bundle.putParcelableArrayList("groupDTOS", (ArrayList<? extends Parcelable>) groupDTOS);
        Gson gson = new GsonBuilder().create();
        bundle.putString("chargeDetails",gson.toJson(chargeDetails));
        intent.putExtras(bundle);
        intent.putExtra("discVo", discVo);
        intent.putExtra("rdtstrptRwsVo", rwsVo);
        startActivity(intent);
    }
}
