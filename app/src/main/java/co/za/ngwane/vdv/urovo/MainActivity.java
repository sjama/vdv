package co.za.ngwane.vdv.urovo;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.device.ScanManager;
import android.device.scanner.configuration.PropertyID;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.codehaus.jackson.map.ObjectMapper;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.ngwane.vdv.DriverSummaryActivity;
import co.za.ngwane.vdv.HomeActivity;
import co.za.ngwane.vdv.MenuActivity;
import co.za.ngwane.vdv.R;
import co.za.rtmc.dto.per.Licence;
import co.za.rtmc.dto.per.RootObject;
import co.za.rtmc.enums.AppConstants;
import co.za.rtmc.service.LicenceDecoder;
import co.za.rtmc.service.Validator;
import co.za.rtmc.vo.BoRoadSafetyInspection;
import co.za.rtmc.vo.RdtstrptRwsVo;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

public class MainActivity extends Activity {

    private final static String SCAN_ACTION = ScanManager.ACTION_DECODE;//default action
    private ActionBar actionBar;
    private EditText showScanResult;
    private Button btn;
    private Button mScan;
    private Button mClose;
    private int type;
    private int outPut;
    
    private Vibrator mVibrator;
    private ScanManager mScanManager;
    private SoundPool soundpool = null;
    private int soundid;
    private String barcodeStr;
    private boolean isScaning = false;
    private SweetAlertDialog pDialog;
    private BoRoadSafetyInspection roadSafetyInspection;

    private BroadcastReceiver mScanReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
            isScaning = false;
            soundpool.play(soundid, 1, 1, 0, 0, 1);
            showScanResult.setText("");
            mVibrator.vibrate(100);

            byte[] barcode = intent.getByteArrayExtra(ScanManager.DECODE_DATA_TAG);
            int barcodelen = intent.getIntExtra(ScanManager.BARCODE_LENGTH_TAG, 0);
            byte temp = intent.getByteExtra(ScanManager.BARCODE_TYPE_TAG, (byte) 0);
            android.util.Log.i("debug", "----codetype--" + temp);
            barcodeStr = new String(barcode, 0, barcodelen);
            showScanResult.append(" length："  +barcodelen);
            showScanResult.append(" barcode："  +barcodeStr);
            showScanResult.append(" barcode："  +bytesToHexString(barcode));
            //showScanResult.setText(barcodeStr);

            if(barcode!=null) {
                String hex = bytesToHexString(barcode);
                System.out.print(hex.toString());
                try {
                    String idN = LicenceDecoder.readLic(hex);
                    invokeWS(idN, "RSA ID", new RdtstrptRwsVo());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }else{

                Toast.makeText(getApplicationContext(), "Barcode Not getting.", Toast.LENGTH_LONG).show();
            }

        }

    };

    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main_urovo);
        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
//        actionBar = getActionBar();
//        actionBar.show();
        setupView();
        
    }

    private void initScan() {
        // TODO Auto-generated method stub
        mScanManager = new ScanManager();
        mScanManager.openScanner(); 
      
        mScanManager.switchOutputMode( 0);
        soundpool = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 100); // MODE_RINGTONE
        soundid = soundpool.load("/etc/Scan_new.ogg", 1);
    }

    private void setupView() {
        // TODO Auto-generated method stub
        showScanResult = findViewById(R.id.scan_result);
//        btn = findViewById(R.id.manager);
//        btn.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View arg0) {
//                // TODO Auto-generated method stub
//               // mScanManager.setOutputParameter(7, 2);
//               /* if(mScanManager.getTriggerMode() != Triggering.CONTINUOUS)
//                    mScanManager.setTriggerMode(Triggering.CONTINUOUS);*/
//                //mScanManager.lockTrigger();
//                /*mScanManager.enableAllSymbologies(false);
//                if(mScanManager.isSymbologySupported(Symbology.QRCODE) && !mScanManager.isSymbologyEnabled(Symbology.QRCODE)) {
//                    mScanManager.enableSymbology(Symbology.QRCODE, true);
//                }*/
//                /*for(int i =0; i< 1000;i++) {
//                    mScanManager.openScanner();
//                    try {
//                        Thread.sleep(100);
//                    } catch (InterruptedException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace();
//                    }
//                    mScanManager.closeScanner();
//                }*/
//            }
//        });
        
        mScan = (Button) findViewById(R.id.scan);
        mScan.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                //if(type == 3)
                    mScanManager.stopDecode();
                    isScaning = true;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    mScanManager.startDecode();
            }
        });
        
        mClose = (Button) findViewById(R.id.close);
        mClose.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
               // if(isScaning) {
                  //  isScaning = false;
                    mScanManager.stopDecode();
                    //mScanManager.unlockTrigger();
                //} 
            }
        });
        
        //btn.setVisibility(View.GONE);
        
    }
    
    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if(mScanManager != null) {
            mScanManager.stopDecode();
            isScaning = false;
        }
        unregisterReceiver(mScanReceiver);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        initScan();
        showScanResult.setText("");
        IntentFilter filter = new IntentFilter();
        int[] idbuf = new int[]{PropertyID.WEDGE_INTENT_ACTION_NAME, PropertyID.WEDGE_INTENT_DATA_STRING_TAG};
        String[] value_buf = mScanManager.getParameterString(idbuf);
        if(value_buf != null && value_buf[0] != null && !value_buf[0].equals("")) {
            filter.addAction(value_buf[0]);
        } else {
            filter.addAction(SCAN_ACTION);
        }
       
        registerReceiver(mScanReceiver, filter);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        return super.onKeyDown(keyCode, event);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.activity_main, menu);
        MenuItem settings = menu.add(0, 1, 0, R.string.menu_settings).setIcon(R.drawable.ic_home_black_24dp);
        // 绑定到actionbar  
        //SHOW_AS_ACTION_IF_ROOM 显示此项目在动作栏按钮如果系统决定有它。 可以用1来代替
        MenuItem version = menu.add(0, 2, 0, R.string.menu_settings);
        settings.setShowAsAction(1);
        version.setShowAsAction(0);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case 1:
                try{
                    Intent intent = new Intent("android.intent.action.SCANNER_SETTINGS");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                break;
            case 2:
                PackageManager pk = getPackageManager();
                PackageInfo pi;
                try {
                    pi = pk.getPackageInfo(getPackageName(), 0);
                    Toast.makeText(this, "V" +pi.versionName , Toast.LENGTH_SHORT).show();
                } catch (NameNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void navigateToManualForm(RdtstrptRwsVo vo ) {
        Intent intent = new Intent(this, DriverSummaryActivity.class);
        intent.putExtra("rdtstrptRwsVo", vo);
        intent.putExtra("inspectionData", roadSafetyInspection);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void navigateToHome() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }

    private void  showFail(final RdtstrptRwsVo rwsVo){

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("NTP 733 Road Test Report")
                .setContentText("Driver not found on eNatis")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        navigateToManualForm(rwsVo);
                    }
                })
                .show();


    }


    public void invokeWS(String idNumber, String dlIdType, final RdtstrptRwsVo rwsVo ) {
        // Show Progress Dialog

        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();

        // Make RESTful webservice call using AsyncHttpClient object
        //ByteArrayEntity entity = new ByteArrayEntity(jsonObject.toString().getBytes("UTF-8"));
        //  entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

        StringEntity entity = null;
        try {
            //entity = new ByteArrayEntity(new JSONObject().toString().getBytes("UTF-8"));
            Gson gson = new GsonBuilder().create();

            rwsVo.setDriidn(idNumber);
            rwsVo.setDrividtypecd(dlIdType);
//            rwsVo.setUsername("BHEKIL");
            entity = new StringEntity(gson.toJson(rwsVo));
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        } catch (Exception e) {
            e.printStackTrace();
        }


        //AsyncHttpClient client = new AsyncHttpClient();
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);

        String idtypecd = "";
        if (dlIdType.equalsIgnoreCase(Validator.RSA_IDENTITY_DOCUMENT)) {
            // show password
            idtypecd = "02";
        } else if (dlIdType.equalsIgnoreCase(Validator.FOREIGN_IDENTITY_DOCUMENT)){
            idtypecd = "03";

        } else {
            idtypecd = "03";
        }
        rwsVo.setDrividtypecd(idtypecd);
        RequestParams params = new RequestParams();
        params.put("idTypeCd", idtypecd);
        params.put("idNumber", idNumber);
        client.get(AppConstants.BACK_OFFICE_APP_URL + "v1/enatis/X1002Driver/"+ idtypecd + "/"+ idNumber, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    // JSON Object
                    pDialog.hide();
                    System.out.println(responseBody);
                    Gson gson = new GsonBuilder().create();


                    //Person person = gson.fromJson(responseBody, Person.class);
                    // When the JSON response has status boolean value assigned with true

                    if ( responseBody.isEmpty() ) {
                        showFail(rwsVo);
                    }
                    // Else display error message
                    else {
                        ObjectMapper mapper = new ObjectMapper();
                        RootObject per = mapper.readValue( responseBody , RootObject.class);
                        rwsVo.setDriname( per.getPerDet().getPerId().getInitials() );
                        rwsVo.setDrisurname( per.getPerDet().getPerId().getBusOrSurname() );

                        String lics = "";
                        if (per.getLicence() != null) {
                            for (Licence lic :  per.getLicence()) {

                                if ( lic.getDriLic().getCode() != null && lic.getDriLic().getCode() != "") {
                                    lics = lics + lic.getDriLic().getCode() + " , ";
                                }

                                //rwsVo.setLiccd(lic.getDriLic().getCode());
                                //rwsVo.setLiccd(person.getLicence().getDriLic().getCode());
                            }

                            rwsVo.setLiccd( lics );
                            rwsVo.setDriLicValidToD(per.getLicence().get(0).getDriLicValidToD());
                        }

                        String prdp = "";
                        if (per.getPrDP() != null) {

                            if (per.getPrDP().getPrDPCatG() != null ) {
                                prdp = per.getPrDP().getPrDPCatG().getCode();
                            }

                            if (per.getPrDP().getPrDPCatD() != null ) {
                                prdp =  prdp + prdp != "" ? ", " : "" + per.getPrDP().getPrDPCatD().getCode();
                            }

                            if (per.getPrDP().getPrDPCatP() != null ) {
                                prdp =   prdp + prdp != "" ? ", " : "" + per.getPrDP().getPrDPCatP().getCode();
                            }

                            rwsVo.setDriPrDPs( prdp );
                            rwsVo.setDriPrDPValidToD( per.getPrDP().getPrDPExpiryD() );
                        }


                        navigateToManualForm( rwsVo );
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "No Data Found on eNaTIS!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                    navigateToHome();

                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Could not establish connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Could not establish connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Could not establish connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }

            }
        });
    }
}
