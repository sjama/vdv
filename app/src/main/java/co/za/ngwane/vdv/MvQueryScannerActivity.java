package co.za.ngwane.vdv;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.zxing.Result;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.codehaus.jackson.map.ObjectMapper;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.rtmc.dto.VehicleDet;
import co.za.rtmc.enums.AppConstants;
import co.za.rtmc.vo.MvLicDiscVo;
import co.za.rtmc.vo.RdtstrptRwsVo;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by Jama on 2016/04/26.
 */
public class MvQueryScannerActivity extends Activity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    private CountDownTimer timer;
    private  RdtstrptRwsVo rwsVo = new RdtstrptRwsVo();
    private VehicleDet vehicle = null;

    private SweetAlertDialog pDialog;
    public static final String FORMATS = "SCAN_FORMATS";

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

        Intent intent = getIntent();
        intent.putExtra("SCAN_FORMATS", "PDF_417");
        rwsVo = (RdtstrptRwsVo) intent.getSerializableExtra("rdtstrptRwsVo");
        scanNow();

        // Set the scanner view as the content view
    }


    private void scanNow(){
        requestCameraPerm();
        mScannerView = new ZXingScannerView(this);
        mScannerView.getFlash();// Programmatically initialize the scanner view
        setContentView(mScannerView);
        countDown();
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.setFlash(true);
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here

        mScannerView.setFlash(false);
        System.out.println(rawResult.getText());
        //    Log.v(TAG, rawResult.getText()); // Prints scan results
        System.out.println(rawResult.getBarcodeFormat().toString());

        //TODO: Capture Result and open new view to display results
        final MvLicDiscVo vo = readLicDiskBarCode(rawResult.getText());
        System.out.println("Registration Number = " + vo.getMvRegN());
        vo.setScanned(true);
        finish();


        checkEnatisForVehicle(vo);

    }

    public void navigatetoReportActivity(MvLicDiscVo discVo){

        Intent intent = new Intent(this, VehicleSearchResultsActivity.class);
        intent.putExtra("rdtstrptRwsVo", rwsVo);
        intent.putExtra("discVo",discVo);
        startActivity(intent);
    }


    public MvLicDiscVo readLicDiskBarCode(String barCdString) {
        MvLicDiscVo mvLicDiscVo = new MvLicDiscVo();
        try {

            String delims = "[%]+";
            String[] tokens = barCdString.split( delims );

            for ( int i = 0; i < tokens.length; i++ ) {
                // System.out.println( tokens[ i ] );

                switch ( i ) {
                    case 0:
                        break;
                    case 1:
                        mvLicDiscVo.setCertN( tokens[ i ] );
                        break;
                    case 2:
                        //mvLicDiscVo.setCertN( tokens[ i ] );
                        break;
                    case 3:
                        mvLicDiscVo.setUsernumber( tokens[ i ] );
                        break;

                    case 4:
                        //mvLicDiscVo.setUsernumber( tokens[ i ] );
                        break;

                    case 5:
                        mvLicDiscVo.setDocCtrlN( tokens[ i ] );
                        break;
                    case 6:
                        mvLicDiscVo.setMvRegN( tokens[ i ] );
                        break;

                    case 7:
                        mvLicDiscVo.setRegtN( tokens[ i ] );
                        break;

                    case 8:
                        mvLicDiscVo.setMvDescCdDesc( tokens[ i ] );
                        break;

                    case 9:
                        mvLicDiscVo.setMake( tokens[ i ] );
                        break;
                    case 10:
                        mvLicDiscVo.setModel( tokens[ i ] );
                        break;
                    case 11:
                        mvLicDiscVo.setColor( tokens[ i ] );
                        break;
                    case 12:
                        mvLicDiscVo.setVin( tokens[ i ] );
                        break;

                    case 13:
                        mvLicDiscVo.setEngineNo( tokens[ i ] );
                        break;
                    case 14:
                        mvLicDiscVo.setExpiry( tokens[ i ] );
                        break;
                }
            }

        } catch ( Exception e ) {
            e.printStackTrace();;
            System.out.println( "Exception ----" );
        }
        return mvLicDiscVo;
    }

    final private int REQUEST_CODE_ASK_PERMISSIONS = 124;
    private void requestCameraPerm(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasCameraPerm = checkSelfPermission(Manifest.permission.CAMERA);
            if (hasCameraPerm != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    showMessageOKCancel("You need to allow access to your camera",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                                REQUEST_CODE_ASK_PERMISSIONS);
                                    }
                                }
                            });
                    return;
                }
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        REQUEST_CODE_ASK_PERMISSIONS);
                return;
            }
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(MvQueryScannerActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    scanNow();
                } else {
                    // Permission Denied
                    Toast.makeText(MvQueryScannerActivity.this, "LOCATION ACCESS Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Count down timer to finish activity when qr is idle/can't read
     * the stamp.Timer set to 20s.
     */

    public void countDown() {
        timer=new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
//                setupEvent(R.string.disc_scan,"Disk Data","Scanning Cancelled");
                Toast.makeText(MvQueryScannerActivity.this, "Scanner Timeout.Please try again!", Toast.LENGTH_LONG).show();
                finish();
            }
        }.start();
    }

    private void  showNoData(final MvLicDiscVo vo){

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("NTP 733 Road Test Report")
                .setContentText("Vehicle not found on eNatis")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        navigatetoReportActivity( vo );
                    }
                })
                .show();


    }

    public void checkEnatisForVehicle(final MvLicDiscVo mvLicDiscVo ) {

        StringEntity entity = null;
        //AsyncHttpClient client = new AsyncHttpClient();
        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        String appURL = AppConstants.BACK_OFFICE_APP_URL+ "v1/enatis/X1001Vehicle/" + mvLicDiscVo.getMvRegN();


        client.get(appURL, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    // JSON Object
//                    pDialog.hide();
                    System.out.println(new String(responseBody));
                    Gson gson = new GsonBuilder().create();
                    vehicle = gson.fromJson(responseBody, VehicleDet.class);
                    // When the JSON response has status boolean value assigned with true

                    if ( responseBody.isEmpty() ) {
                        mvLicDiscVo.setEnatisNoData(true);
                        mvLicDiscVo.setMvDescCdDesc( null );
//                        showFail( mvLicDiscVo );
                    } else {
                        ObjectMapper mapper = new ObjectMapper();
                        vehicle = mapper.readValue( responseBody , VehicleDet.class);
                        mvLicDiscVo.setMvDescCdDesc(  vehicle.getMvcat().getDesc());

                        mvLicDiscVo.setMvDescCdDesc( vehicle.getMvcat().getDesc() );
                        mvLicDiscVo.setRwStatus(vehicle.getRwstat().getDesc());
                        mvLicDiscVo.setSapMark(vehicle.getSapmark().getDesc());
                        mvLicDiscVo.setMvState(vehicle.getMvstate().getDesc());
                        if ( !vehicle.getEngineN().equalsIgnoreCase(mvLicDiscVo.getEngineNo()) ||
                                !vehicle.getVinOrChassis().equalsIgnoreCase(mvLicDiscVo.getVin())  ||
                                !vehicle.getMake().getDesc().equalsIgnoreCase(mvLicDiscVo.getMake())) {
                            mvLicDiscVo.setEnatisNoMatch(false);



//                            showWarning(mvLicDiscVo);

                        }
                    }
                    navigatetoReportActivity( mvLicDiscVo );

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
//                pDialog.hide();
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Could not get connection to eNaTIS.", Toast.LENGTH_LONG).show();
                }

                mvLicDiscVo.setMvDescCdDesc( null );
                navigatetoReportActivity(mvLicDiscVo);
            }
        });

    }

    @Override
    public void finish()
    {
        if(timer != null) {
            timer.cancel();         //Cancel Timer when Activity Closes!
            timer = null;
        }
        super.finish();
    }

    public void showWarning(final MvLicDiscVo mvLicDiscVo) {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("NTP 733 Road Test Report")
                .setContentText("Vehicle details do not match with the details on eNatis")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        navigatetoReportActivity( mvLicDiscVo );
                    }
                })
                .show();
    }
}