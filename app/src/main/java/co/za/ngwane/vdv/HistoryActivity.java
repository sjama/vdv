package co.za.ngwane.vdv;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.rtmc.dto.RoadSafety;
import co.za.rtmc.vo.BoRoadSafetyInspection;

import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TableRow.LayoutParams;

/**
 * Created by Jay.
 */
public class HistoryActivity extends AppCompatActivity {
    String companies[] = {"Google","Windows","iPhone","Nokia","Samsung",
            "Google","Windows","iPhone","Nokia","Samsung",
            "Google","Windows","iPhone","Nokia","Samsung"};
    String os[]       =  {"Android","Mango","iOS","Symbian","Bada",
            "Android","Mango","iOS","Symbian","Bada",
            "Android","Mango","iOS","Symbian","Bada"};

    TableLayout tl;
    TableRow tr;
    TextView companyTV,valueTV, dateTime, venueTV, actionTV ;
    RoadSafety roadSafetyInspection;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        tl = (TableLayout) findViewById(R.id.maintable);
        Intent intent = getIntent();

        roadSafetyInspection = (RoadSafety) intent.getSerializableExtra("inspectionDataList");

        addHeaders();
        addData();

        Button mEmailSignInButton = (Button) findViewById(R.id.btnFinish);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showSuccess();
            }
        });
    }

    private void  showSuccess(){

        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText("Road Test Report")
                .setContentText("Road Test Results Added Successfully")
                .setConfirmText("OK!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        navigateToHomeForm();
                    }
                })
                .show();


    }

    private void navigateToHomeForm() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }

    /** This function add the headers to the table **/
    public void addHeaders(){

        /** Create a TableRow dynamically **/
        tr = new TableRow(this);
        tr.setLayoutParams(new LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));

        /** Creating a TextView to add to the row **/
        TextView dateTime = new TextView(this);
        dateTime.setText("Date Stopped");
        dateTime.setTextColor(Color.GRAY);
        dateTime.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
//        dateTime.setPadding(5, 5, 5, 0);
        tr.addView(dateTime);  // Adding textView to tablerow.

        /** Creating another textview **/
        TextView venueTV = new TextView(this);
        venueTV.setText("Location");
        venueTV.setTextColor(Color.GRAY);
//        venueTV.setPadding(3, 3, 3, 0);
        venueTV.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(venueTV); // Adding textView to tablerow.

        /** Creating another textview **/
        TextView actionTV = new TextView(this);
        actionTV.setText("Action");
        actionTV.setTextColor(Color.GRAY);
//        venueTV.setPadding(3, 3, 3, 0);
        actionTV.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(actionTV); // Adding textView to tablerow.

        // Add the TableRow to the TableLayout
        tl.addView(tr, new TableLayout.LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));

        // we are adding two textviews for the divider because we have two columns
        tr = new TableRow(this);
        tr.setLayoutParams(new LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));

        /** Creating another textview **/
        TextView divider = new TextView(this);
        divider.setText("-----------------");
        divider.setTextColor(Color.GREEN);
        divider.setPadding(5, 0, 0, 0);
        divider.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(divider); // Adding textView to tablerow.

        TextView divider2 = new TextView(this);
        divider2.setText("-------------");
        divider2.setTextColor(Color.GREEN);
        divider2.setPadding(5, 0, 0, 0);
        divider2.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(divider2); // Adding textView to tablerow.

        TextView divider3 = new TextView(this);
        divider3.setText("-------------------");
        divider3.setTextColor(Color.GREEN);
        divider3.setPadding(3, 0, 0, 0);
        divider3.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tr.addView(divider3); // Adding textView to tablerow.

        // Add the TableRow to the TableLayout
        tl.addView(tr, new TableLayout.LayoutParams(
                LayoutParams.FILL_PARENT,
                LayoutParams.WRAP_CONTENT));
    }

    /** This function add the data to the table **/
    public void addData(){

        for (BoRoadSafetyInspection roadIns : roadSafetyInspection.getRoadSafetyInspections())
        {

            /** Create a TableRow dynamically **/
            tr = new TableRow(this);
            tr.setLayoutParams(new LayoutParams(
                    LayoutParams.FILL_PARENT,
                    LayoutParams.WRAP_CONTENT));

            /** Creating a TextView to add to the row **/
            dateTime = new TextView(this);
            dateTime.setText(roadIns.getDateTime());
            dateTime.setTextColor(Color.RED);
            dateTime.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            dateTime.setPadding(5, 5, 5, 5);
            tr.addView(dateTime);  // Adding textView to tablerow.

            /** Creating another textview **/
            venueTV = new TextView(this);
            venueTV.setText(roadIns.getLocationName());
            venueTV.setTextColor(Color.GREEN);
            venueTV.setPadding(3, 3, 3, 3);
            venueTV.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            tr.addView(venueTV); // Adding textView to tablerow.

            /** Creating another textview **/
            actionTV = new TextView(this);
            actionTV.setText(roadIns.getActionStatus());
            actionTV.setTextColor(Color.GREEN);
            venueTV.setPadding(3, 3, 3, 3);

            actionTV.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            tr.addView(actionTV); // Adding textView to tablerow.

            // Add the TableRow to the TableLayout
            tl.addView(tr, new TableLayout.LayoutParams(
                    LayoutParams.FILL_PARENT,
                    LayoutParams.WRAP_CONTENT));
        }
    }
}
