package co.za.ngwane.vdv;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import org.json.JSONException;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import cn.pedant.SweetAlert.SweetAlertDialog;
import co.za.rtmc.enums.AppConstants;
import co.za.rtmc.enums.QuickstartPreferences;
import co.za.rtmc.service.ReportsDatabaseHelper;
import co.za.rtmc.service.SessionManager;
import co.za.rtmc.vo.User;
import cz.msebera.android.httpclient.Header;
import za.co.technovolve.dlcserializerrsa.DrivingLicenseCard;

import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_PHONE_STATE;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;
    // Session Manager Class
    SessionManager session;
    String imeiNo = null;
    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

//    static
//    {
//        System.loadLibrary("dlcserializerrsa");
//    }
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    SweetAlertDialog pDialog;


    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;
    private TextView mInformationTextView;
    private boolean isReceiverRegistered;
    BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
    BluetoothDevice mmDevice;
    // needed for communication to bluetooth device / network
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;

    byte[] readBuffer;
    int readBufferPosition;
    volatile boolean stopWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //String uniqueKey = DrivingLicenseCard.getUniqueDeviceId();

        // Session Manager
        session = new SessionManager(getApplicationContext());
        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);
        mEmailView.setFilters(new InputFilter[] {new InputFilter.AllCaps(), new InputFilter.LengthFilter(8)});
        mEmailView.setTextColor(Color.BLACK);

        populateAutoComplete();
        imeiNo = getImeiNumber();
        boolean isDevicePaired = isDevicePaired();

        Button mEmailSignInButton = (Button) findViewById( R.id.email_sign_in_button);
        mEmailSignInButton.setVisibility(isDevicePaired ? View.VISIBLE : View.GONE);
        mEmailSignInButton.setEnabled(isDevicePaired ? true:false);
                mEmailSignInButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        attemptLogin();
            }
        });


        Button linkButton = (Button) findViewById( R.id.link_button);
        linkButton.setVisibility(isDevicePaired ? View.GONE : View.VISIBLE);
        linkButton.setEnabled(isDevicePaired ? false : true );
        linkButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                linkDeviceToOfiicer();
            }
        });

        mProgressView = findViewById(R.id.login_progress);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
//                mRegistrationProgressBar.setVisibility(ProgressBar.GONE);
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                //    mInformationTextView.setText(getString(R.string.gcm_send_message));
                } else {
                  //  mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };
        registerReceiver();
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private String getImeiNumber(){

        if(!requestPhoneStatePerm()){
            return "";
        }
        session.storeIMEI(getDeviceID());
        return getDeviceID();

    }


    private boolean isDevicePaired(){
        ReportsDatabaseHelper  databaseHelper  = ReportsDatabaseHelper.getInstance(getApplicationContext());
        return databaseHelper.isDevicePaired();

    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView,  R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }


    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private boolean requestPhoneStatePerm() {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasWriteContactsPermission = checkSelfPermission(android.Manifest.permission.READ_PHONE_STATE);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(android.Manifest.permission.READ_PHONE_STATE)) {
                    showMessageOKCancel("You need to allow access to phone state",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                                                REQUEST_CODE_ASK_PERMISSIONS);
                                    }
                                }
                            });
                }
                requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                        REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
        // insertDummyContact();
        return false;
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LoginActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public String getDeviceID() {
        String devcieId;
        TelephonyManager mTelephony = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (mTelephony.getDeviceId() != null) {
            devcieId = mTelephony.getDeviceId();
        } else {
            devcieId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return devcieId;
    }


    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    private void linkDeviceToOfiicer(){

        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
/*
        mPasswordView.setError(null);
*/

        // Store values at the time of the login attempt.
        String username = mEmailView.getText().toString();
/*
        String password = mPasswordView.getText().toString();
*/

        boolean cancel = false;
        View focusView = null;

        // Check for a valid InfN/Username, if the user entered one.
        if (TextUtils.isEmpty(username) || !isUserNameValid(username)) {
            mEmailView.setError(getString( R.string.error_invalid_infN));
            focusView = mEmailView;
            cancel = true;
        }

        // Check for a valid username address.


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            // showProgress(true);
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Loading");
            pDialog.setCancelable(false);
            pDialog.show();
            JSONObject jsonParams = new JSONObject();
            try {
                jsonParams.put("username", "Test api support");
                jsonParams.put("username", "Test api support");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestParams requestParams = new RequestParams();
            requestParams.put("username",username);
/*
            requestParams.put("password",password);
*/
            // navigatetoReportActivity();
            doLinkDevice(username, imeiNo);
            // mAuthTask = new UserLoginTask(username, password);
            // mAuthTask.execute((Void) null);
        }

    }

    /******
     * Link Device to INF Number
     * Do webservice call to Backoffice
     * Store user information locally for authentication purposes
     *
     */
    private void doLinkDevice(final String username, String imei) {

        ReportsDatabaseHelper  databaseHelper  = ReportsDatabaseHelper.getInstance(getApplicationContext());
        User user = new User(username,imeiNo);
        databaseHelper.addOrUpdateUser(user);
        navigatetoLoginActivity();

//        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
//
//        imeiNo = getImeiNumber();
//        http://192.168.2.12:8080/backoffice/linkDevice/29550DXK/358851060044280
//        client.get(AppConstants.APP_URL+ username + "/" + imeiNo, new AsyncHttpResponseHandler() {
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                showProgress(false);
//                pDialog.hide();
//                ReportsDatabaseHelper  databaseHelper  = ReportsDatabaseHelper.getInstance(getApplicationContext());
//                User user = new User(username,imeiNo);
//                databaseHelper.addOrUpdateUser(user);
//                navigatetoLoginActivity();
//
//            }
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                showProgress(false);
//                pDialog.hide();
//
//                Toast.makeText(getApplicationContext(), "Failed to link the device", Toast.LENGTH_LONG).show();
//            }
//        });
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
/*
        mPasswordView.setError(null);
*/

        // Store values at the time of the login attempt.
        String username = mEmailView.getText().toString();
/*
        String password = mPasswordView.getText().toString();
*/

        boolean cancel = false;
        View focusView = null;

        // Check for a valid InfN/Username, if the user entered one.
        if (TextUtils.isEmpty(username) || !isUserNameValid(username)) {
            mEmailView.setError(getString( R.string.error_invalid_infN));
            focusView = mEmailView;
            cancel = true;
        }

        // Check for a valid username address.


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
           // showProgress(true);
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
            pDialog.setTitleText("Loading");
            pDialog.setCancelable(false);
            pDialog.show();
            JSONObject jsonParams = new JSONObject();
            try {
                jsonParams.put("username", "Test api support");
                jsonParams.put("username", "Test api support");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            RequestParams requestParams = new RequestParams();
            requestParams.put("username",username);
/*
            requestParams.put("password",password);
*/
           // navigatetoReportActivity();
            doLogin(username);
           // mAuthTask = new UserLoginTask(username, password);
           // mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    private boolean isUserNameValid(String infN) {
        //TODO: Replace this with your own logic
        return infN.length() > 4;
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

          /*  mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            }); */
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

//        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    /**
     * Method which navigates from Login Activity to Home Activity
     */
    public void navigatetoReportActivity(){
        Intent intent = new Intent(this, MenuActivity.class);
        //Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }


    public void navigatetoLoginActivity(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }




    public void navigatetoChangePasswordActivity(){
        Intent intent = new Intent(this, ChangePassword.class);
        startActivity(intent);
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.


           /*
            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(mEmail)) {
                    // Account exists, return true if the password matches.
                    return pieces[1].equals(mPassword);
                }
            } */

            // TODO: register the new account here.
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
              //  finish();
                //navigatetoReportActivity();
            } else {
                mEmailView.setError(getString( R.string.error_invalid_infN));
                mEmailView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    public void doLogin(String username){
        ReportsDatabaseHelper  databaseHelper  = ReportsDatabaseHelper.getInstance(getApplicationContext());
        User user =  databaseHelper.getUser(username);




        if (!user.userName.trim().isEmpty()){
            // Creating user login session
            // For testing i am stroing name, email as follow
            // Use user real data
            session.createLoginSession(username, user.IMEINumber);
            navigatetoReportActivity();


/*                        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences("SessionUser",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("phephaUser", loginName);
                        editor.apply();*/

        }else{
            //errorMsg.setText(obj.getString("error_msg"));
            showProgress(false);
            pDialog.hide();

            Toast.makeText(getApplicationContext(), "Mobile device not linked to officer", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }

    private void registerReceiver(){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }
    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


}

