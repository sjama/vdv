package co.za.ngwane.vdv;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import co.za.rtmc.vo.UserspRwsVo;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;


public class ForgotPasswordActivity extends Activity {

    // Progress Dialog Object
    private View mProgressView;
    private View mLoginFormView;
    protected EditText mEmailText;
    protected EditText mIdNumberText;

    protected Button mSubmitButton;
    protected Button bCancelButton;
    protected String userName;
    protected String idNumber;
    protected String email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_forgot_password);

        bCancelButton = (Button)findViewById(R.id.forgot_password_cancel_button);

        Intent intent = getIntent();
        userName = (String) intent.getSerializableExtra("userName");
        bCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToLoginForm();
            }
        });


        mSubmitButton = (Button)findViewById(R.id.forgot_password_submit_button);

        mSubmitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                View focusView = null;
                boolean cancel = false;
                mEmailText = (EditText)findViewById(R.id.emailAddress);
                mIdNumberText = (EditText)findViewById(R.id.idNumber);

                email = mEmailText.getText().toString();
                idNumber = mIdNumberText.getText().toString();

                if(!isValidEmail(email))
                {
                    mEmailText.setError(getString(R.string.error_forgot_psword_email_info));
                    focusView = mEmailText;
                    cancel = true;
                   /* AlertDialog.Builder builder = new AlertDialog.Builder(ForgotPasswordActivity.this);
                    builder.setMessage(R.string.error_forgot_psword_email_info)
                            .setTitle(R.string.forgot_psword_error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();*/
                }

                if(idNumber.isEmpty())
                {
                    mIdNumberText.setError(getString(R.string.error_forgot_psword_id_number_info));
                    focusView = mIdNumberText;
                    cancel = true;

/*
                    AlertDialog.Builder builder = new AlertDialog.Builder(ForgotPasswordActivity.this);
                    builder.setMessage(R.string.error_forgot_psword_id_number_info)
                            .setTitle(R.string.forgot_psword_error_title)
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
*/
                }

                if (cancel) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                }else {
                    invokeWS();
                    /*ParseUser.requestPasswordResetInBackground(email,
                            new RequestPasswordResetCallback() {

                                @Override
                                public void done(com.parse.ParseException e) {
                                    // TODO Auto-generated method stub
                                    setProgressBarIndeterminateVisibility(false);
                                    if (e == null) {

                                        AlertDialog.Builder builder = new AlertDialog.Builder(ForgotPasswordActivity.this);
                                        builder.setMessage("An email was sent. Please check your email within the next few minutes for more instructions")
                                                .setTitle("Success")
                                                .setPositiveButton(android.R.string.ok, null);
                                        AlertDialog dialog = builder.create();
                                        dialog.show();
                                    } else {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(ForgotPasswordActivity.this);
                                        builder.setMessage(R.string.forgot_password_label)
                                                .setTitle(R.string.forgot_psword_error_title)
                                                .setPositiveButton(android.R.string.ok, null);
                                        AlertDialog dialog = builder.create();
                                        dialog.show();
                                        // Something went wrong. Look at the ParseException to see what's up.
                                    }
                                }
                            });*/
                }

            }
        });

        mLoginFormView = findViewById(R.id.email_forgot_password_form);
        mProgressView = findViewById(R.id.login_progress);


    }

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
    public void invokeWS() {
        StringEntity entity = null;
        try {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            Gson gson = new GsonBuilder().create();
            UserspRwsVo vo = new UserspRwsVo();
            vo.setUsername(userName);
            vo.setIdn(idNumber);
            vo.setEmailAddress(email);

            entity = new StringEntity(gson.toJson(vo));
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        } catch (Exception e) {
            e.printStackTrace();
        }


        AsyncHttpClient client = new AsyncHttpClient();
        client.post(getApplicationContext(),"http://192.168.0.156:8080/phepha/svs/userspRws/passwordReset", entity, "application/json", new TextHttpResponseHandler() {
            //        client.post(getApplicationContext(), "http://192.168.8.101:9090/phepha/svs/rdtstrptresRws/add", entity, "application/json", new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    // JSON Object
                    System.out.println("Success Password reset, return true");
                    showProgress(false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ForgotPasswordActivity.this);
                    builder.setMessage("An email was sent. Please check your email within the next few minutes for more instructions")
                            .setTitle("Success")
                            .setIcon(R.drawable.tick)
                            .setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            navigateToLoginForm();
                                        }
                                    });

                    AlertDialog dialog = builder.create();
                    dialog.show();


                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                showProgress(false);
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Method which navigates from Login Activity to Home Activity
     */
    public void navigateToLoginForm(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


}