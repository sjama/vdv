package co.za.rtmc.enums;

/**
 * Created by jamasithole on 2018/02/03.
 */

public enum InfringementStatus {

    INFRINGEMENT_NOTICE( "03", "Infringement notice" ), INFRINGEMENT_STATUS_WARRANT( "08", "Warrant of execution" ), INFRINGEMENT_STATUS_ENFORCEMENT( "07",
            "Enforcement order"), INFRINGEMENT_STATUS_COURT( "10","Court case" );

    private String code;
    private String description;

    InfringementStatus( String code, String description ) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode( String code ) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public static String getDescByCode( String code ) {
        for ( InfringementStatus resultsEnum : InfringementStatus.values() ) {
            if ( code.equalsIgnoreCase( resultsEnum.getCode() ) ) {
                return resultsEnum.description;
            }
        }
        return null;
    }

    public static String getCodeByDesc( String desc ) {
        for ( InfringementStatus resultsEnum : InfringementStatus.values() ) {
            if ( desc.equalsIgnoreCase( resultsEnum.getDescription() ) ) {
                return resultsEnum.code;
            }
        }
        return null;
    }

}
