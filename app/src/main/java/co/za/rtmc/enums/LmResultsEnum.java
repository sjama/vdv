package co.za.rtmc.enums;

public enum LmResultsEnum {

    IN_ORDER( "1", "IN ORDER" ), DEFECTIVE( "2", "DEFECTIVE" ), NOT_TESTED( "3",
            "NOT TESTED" );

    private String code;
    private String description;

    LmResultsEnum( String code, String description ) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode( String code ) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public static String getDescByCode( String code ) {
        for ( LmResultsEnum resultsEnum : LmResultsEnum.values() ) {
            if ( code.equalsIgnoreCase( resultsEnum.getCode() ) ) {
                return resultsEnum.description;
            }
        }
        return null;
    }

    public static String getCodeByDesc( String desc ) {
        for ( LmResultsEnum resultsEnum : LmResultsEnum.values() ) {
            if ( desc.equalsIgnoreCase( resultsEnum.getDescription() ) ) {
                return resultsEnum.code;
            }
        }
        return null;
    }

}