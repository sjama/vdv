package co.za.rtmc.enums;

/**
 * Created by Jama on 2016/07/16.
 */
public class QuickstartPreferences {

        public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
        public static final String REGISTRATION_COMPLETE = "registrationComplete";

}
