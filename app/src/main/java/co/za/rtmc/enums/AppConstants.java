package co.za.rtmc.enums;

/**
 * Created by ngwane1 on 7/11/2016.
 */
public class AppConstants {

    public static final String APP_URL              = "https://services.rtmc.co.za/bo/linkDevice/";
    public static final String ENATIS_MV_QUERY_URL        = "https://services.rtmc.co.za/enatis-gateway-1.0-SNAPSHOT/rest/xml/X1001?licN=";
    public static final String BACK_OFFICE_APP_URL              = "https://services.rtmc.co.za/backoffice/svs/";
    public static final String BACK_OFFICE_BO_APP_URL              = "https://services.rtmc.co.za/bo/";
    //public static final String BACK_OFFICE_BO_APP_URL              = "http://10.10.1.65:8080/backoffice/svs/";




    // Motor Vehicle Category Code
    public static final String MV_CAT_MOTORCYCLE              = "1";
    public static final String MV_CAT_LIGHT_PASSENGER_MV      = "2";
    public static final String MV_CAT_HEAVY_PASSENGER_MV      = "3";
    public static final String MV_CAT_LIGHT_LOAD_VEH_NOT_DRAW = "4";
    public static final String MV_CAT_HEAVY_LOAD_VEH_NOT_DRAW = "5";
    public static final String MV_CAT_HEAVY_LOAD_DRAW_VEHICLE = "6";
    public static final String MV_CAT_SPECIAL_VEHICLE         = "7";
    public static final String MV_CAT_UNKNOWN                 = "8";

    public static final String NONE                 = "NONE";
    public static final String ROAD_WORTHY                 = "ROADWORTHY";

    // Motor Vehicle Category Code  Description
    public static final String MV_CAT_MOTORCYCLE_DESC              = "Motorcycle / Motortricycle / Quadrucycle";
    public static final String MV_CAT_LIGHT_PASSENGER_MV_DESC      = "Light passenger mv(less than 12 persons)";
    public static final String MV_CAT_HEAVY_PASSENGER_MV_DESC      = "Heavy passenger mv (12 or more persons)";
    public static final String MV_CAT_LIGHT_LOAD_VEH_NOT_DRAW_DESC = "Light load vehicle (GVM 3500Kg or less)";
    public static final String MV_CAT_HEAVY_LOAD_VEH_NOT_DRAW_DESC = "Heavy load veh(GVM>3500Kg, not to draw)";
    public static final String MV_CAT_HEAVY_LOAD_DRAW_VEHICLE_DESC = "Heavy load veh(GVM>3500Kg equip to draw)";
    public static final String MV_CAT_SPECIAL_VEHICLE_DESC		   = "Special Vehicle";
    public static final String MV_CAT_UNKNOWN_DESC                 = "Unknown";

    // Prosecutions

/*    -	AARTO 01
            -	Section 56
            -	Discontinue Notice
    -	Warning*/

    public static final String PROC_AARTO_DESC                = "AARTO 01";
    public static final String PROC_SECT_56_DESC              = "Section 56";
    public static final String PROC_DISCON_DESC               = "Discontinue Notice";
    public static final String PROC_WARNING_DESC              = "Warning";

    public static final String WINDSCREEN_WIPERS = "WINDSCREEN AND WIPERS";
    public static final String TYRES= "TYRES";
    public static final String INDICATORS = "INDICATORS";
    public static final String LIGHTS = "LIGHTS";
    public static final String REG_PLATES = "REGISTRATION PLATES";

    public static final String OIL_LEAKS = "OIL LEAKS";
    public static final String X_HAUST = "EXHAUST SYSTEM";;
    public static final String SERVICE_BREAKS_DISTANT = "SERVICE BRAKES STOPPING DISTANCE";
    public static final String EMERGENCY_BRAKES_DISTANT = "EMERGENCE BRAKES STOPPING DISTANCE";
    public static final String STEERING_MECHANISM = "STEERING MECHANISM";

    public static final String WARNIG_DEVICE="WARNING DEVICE";
    public static final String REAR_VIEW_MIRROR = "REARVIEW MIRROR";
    public static final String LOAD_PROJECTIONS = "LOAD AND PROJECTIONS";

    public static final String MP_CODE = "1";
    public static final String GAUTENG_CODE = "2";
    public static final String LIMPOPO_CODE = "3";
    public static final String NORTH_WEST_CODE = "4";
    public static final String NORTHERN_CAPE_CODE = "5";
    public static final String EASTERN_CAPE_CODE = "6";
    public static final String FREE_STATE_CODE = "7";
    public static final String WESTERN_CAPE_CODE = "8";
    public static final String KZN_CODE = "9";

    public static final String MP = "Mpumalanga";
    public static final String GAUTENG = "Gauteng";
    public static final String LIMPOPO = "Limpopo";
    public static final String NORTH_WEST = "North West";
    public static final String NORTHERN_CAPE = "Northern Cape";
    public static final String EASTERN_CAPE = "Eastern Cape";
    public static final String FREE_STATE = "Free State";
    public static final String WESTERN_CAPE = "Western Cape";
    public static final String KZN = "kwaZulu-Natal";

}
