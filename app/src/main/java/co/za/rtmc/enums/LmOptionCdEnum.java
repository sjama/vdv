package co.za.rtmc.enums;

/**
 *
 * @author Ngwane
 */
public enum LmOptionCdEnum {

    CHEVRON( "1", "CHEVRON" ),
    EMERGENCE_BRAKES_STOPPING_DISTANCE( "2",
            "EMERGENCE BRAKES STOPPING DISTANCE" ),
    EMERGENCE_WARNING_SIGNS( "3", "EMERGENCE WARNING SIGNS" ),
    ENTRANCE_AND_EXITS( "4", "ENTRANCE AND EXITS" ),
    EXHAUST_SYSTEM( "5", "EXHAUST SYSTEM" ),
    INDICATORS( "6", "INDICATORS" ),
    LIGHTS( "7", "LIGHTS" ),
    LOAD_AND_PROJECTIONS( "8", "LOAD AND PROJECTIONS" ),
    OIL_LEAKS( "9", "OIL LEAKS" ),
    REARVIEW_MIRROR( "10", "REARVIEW MIRROR" ),
    REFLECTORS( "11", "REFLECTORS" ),
    REGISTRATION_PLATES( "12", "REGISTRATION PLATES" ),
    SERVICE_BRAKES_STOPPING_DISTANCE( "13", "SERVICE BRAKES STOPPING DISTANCE" ),
    STEERING_MECHANISM( "14", "STEERING MECHANISM" ),
    TYRES( "15", "TYRES" ),
    VACUUM_PRESSURE_GAUGE( "16", "VACUUM PRESSURE GAUGE" ),
    WARNING_DEVICE( "17", "WARNING DEVICE" ),
    WINDSCREEN_AND_WIPERS( "18", "WINDSCREEN AND WIPERS" );

    private String code;
    private String description;

    LmOptionCdEnum( String code, String description ) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode( String code ) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public static String getDescByCode( String code ) {
        for ( LmOptionCdEnum optionCdEnum : LmOptionCdEnum.values() ) {
            if ( code.equalsIgnoreCase( optionCdEnum.getCode() ) ) {
                return optionCdEnum.description;
            }
        }

        return null;
    }

    public static String getCodeByDesc( String desc ) {
        for ( LmOptionCdEnum optionCdEnum : LmOptionCdEnum.values() ) {
            if ( desc.equalsIgnoreCase( optionCdEnum.getDescription() ) ) {
                return optionCdEnum.code;
            }
        }

        return null;
    }
}
