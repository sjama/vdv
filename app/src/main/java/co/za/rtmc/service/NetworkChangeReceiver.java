package co.za.rtmc.service;

/**
 * Created by Jama on 2016/07/02.
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import java.util.List;

import co.za.rtmc.vo.RdtstrptresVoRwsVo;

public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {

        int status = NetworkUtil.getConnectivityStatus(context);


        if(status == NetworkUtil.TYPE_MOBILE ||  status == NetworkUtil.TYPE_WIFI ){
            ReportsDatabaseHelper databaseHelper = ReportsDatabaseHelper.getInstance(context);

            for(RdtstrptresVoRwsVo report : databaseHelper.getAllReports() ){
               new WebServiceClient(context).syncToRemoteDB(report);
            }

        }

       // Toast.makeText(context, status, Toast.LENGTH_LONG).show();
    }
}
