package co.za.rtmc.service;

/**
 * Created by ngwane1 on 6/9/2016.
 */

import java.util.HashMap;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import co.za.ngwane.vdv.LoginActivity;

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    public static final String PREF_NAME = "SessionUser";

    // All Shared Preferences Keys
    public static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "phephaUser";

    public static final String KEY_IME = "imei";

    // Email address (make variable public to access from outside)
    public static final String KEY_SURNAME = "officerName";

    public static final String KEY_MENU_VERIFY_DRIVER = "01";
    public static final String KEY_MENU_QUERY_DRIVER = "02";
    public static final String KEY_MENU_VERIFY_VEHICLE = "03";
    public static final String KEY_MENU_ISSUE_FINE = "04";
    public static final String KEY_MENU_QUERY_FINE = "05";


    public static final String KEY_MENU_CD = "KEY_MENU_CD";


    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String name, String officerName){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_NAME, name);

        // Storing email in pref
        editor.putString(KEY_SURNAME, officerName);

        // commit changes
        editor.commit();
    }

    public void storeIMEI(String imei){
        // Storing login value as TRUE

        // Storing name in pref
        editor.putString(KEY_IME, imei);

        // Storing email in pref

        // commit changes
        editor.commit();
    }

    public void storeMenuCode(String menu){
        // Storing login value as TRUE

        // Storing name in pref
        editor.putString(KEY_MENU_CD, menu);

        // commit changes
        editor.commit();
    }

    /**
     * Get stored session data
     * */
    public String getMenuCd(){
        String menu = pref.getString(KEY_MENU_CD, null);
         // return menuCD
        return menu;
    }


    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }



    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));


        // user email id
        user.put(KEY_SURNAME, pref.getString(KEY_SURNAME, null));


        // return user
        return user;
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){

        return pref.getBoolean(IS_LOGIN, false);
    }

    public void SavePreferences( boolean back){

        editor.putBoolean("state", back);
        editor.commit();   // I missed to save the data to preference here,.
    }

    public boolean LoadPreferences(){
        Boolean  state = pref.getBoolean("state", false);
        return state;
    }


}
