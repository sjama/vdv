package co.za.rtmc.service;

/**
 * Created by Jay on 2019/03/10.
 */


import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.DLSequence;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.Security;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;


public class LicenceDecoder {

    public static int byte3ToInt(byte[] byte3){
        int res = 0;
        for (int i = 0; i < 3; i++)
        {
            res += res * 0xFF + byte3[i];
            if (byte3[i] < 0x7F)
            {
                break;
            }
        }
        return res;
    }

    public static byte[] decode128(byte[] cipherText, Cipher asymmetricCipher) throws Exception {

        // asuming, cipherText is a byte array containing your encrypted message
        byte[] out =  asymmetricCipher.doFinal(cipherText);

        StringBuilder sb = new StringBuilder();
        for (byte b : out) {
            sb.append(String.format("%02X ", b));
        }

        System.out.print(out.length + " ");
        System.out.println(sb.toString());
        // return new String(plainText);
        return out;
    }

    public static BigInteger[] parseASN1RsaPublicKey(byte [] encoded) throws IOException {
        ASN1InputStream asn1_is = new ASN1InputStream(encoded);
        DLSequence dlSeq = (DLSequence) asn1_is.readObject();
        ASN1Integer asn1_n = (ASN1Integer) dlSeq.getObjectAt(0);
        ASN1Integer asn1_e = (ASN1Integer) dlSeq.getObjectAt(1);
        asn1_is.close();
        return new BigInteger[]{ asn1_n.getPositiveValue(), asn1_e.getPositiveValue()};
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public static void main1(String[] args) throws Exception {


        InputStream is = new FileInputStream("/Users/ashrafmoosa/image.wi");
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));

        String line = buf.readLine();
        StringBuilder sb = new StringBuilder();

        while(line != null){
            sb.append(line).append("\n");
            line = buf.readLine();
        }

        String fileAsString = sb.toString();
        fileAsString = fileAsString.replaceAll("\\s+","");
        byte[] array = hexStringToByteArray(fileAsString);
        FileOutputStream fous = new FileOutputStream("/Users/ashrafmoosa/sample.wi");
        //for (int i =0; i < 602; i++)
        fous.write(array);
        fous.close();

    }

    public static String  readLic(byte[] array) throws Exception {

        Security.addProvider(new BouncyCastleProvider());

        Cipher asymmetricCipher = Cipher.getInstance("RSA/NONE/NoPadding", "BC");


        String pubKey = "MIGWAoGBAMqfGO9sPz+kxaRh/qVKsZQGul7NdG1gonSS3KPXTjtcHTFfexA4MkGAmwKeu9XeTRFgMMxX99WmyaFvNzuxSlCFI/foCkx0TZCFZjpKFHLXryxWrkG1Bl9++gKTvTJ4rWk1RvnxYhm3n/Rxo2NoJM/822Oo7YBZ5rmk8NuJU4HLAhAYcJLaZFTOsYU+aRX4RmoF";
        String pubKey2 = "MF8CSwC0BKDfEdHKz/GhoEjU1XP5U6YsWD10klknVhpteh4rFAQlJq9wtVBUc5DqbsdI0w/bga20kODDahmGtASy9fae9dobZj5ZUJEw5wIQMJz+2XGf4qXiDJu0R2U4Kw==";


        KeyFactory kf = KeyFactory.getInstance("RSA");
        byte[] decodedPublicKey = Base64.decode(pubKey);
        byte[] decodedPublicKey2 = Base64.decode(pubKey2);
        BigInteger[] arr = parseASN1RsaPublicKey(decodedPublicKey);

        RSAPublicKeySpec keySpecX509 = new RSAPublicKeySpec(arr[0], arr[1]);
        RSAPublicKey publicKey = (RSAPublicKey) kf.generatePublic(keySpecX509);

        byte[] mykey = publicKey.getEncoded();
        String myenc = new String(Base64.encode(mykey));
        System.out.println(myenc);

        BigInteger[] arr2 = parseASN1RsaPublicKey(decodedPublicKey2);

        RSAPublicKeySpec keySpecX5092 = new RSAPublicKeySpec(arr2[0], arr2[1]);
        RSAPublicKey publicKey2 = (RSAPublicKey) kf.generatePublic(keySpecX5092);
        // initialize your cipher
        asymmetricCipher.init(Cipher.DECRYPT_MODE, publicKey);

        System.out.println("---PUBLIC KEY 1----");
        System.out.println(publicKey);
        System.out.println("-----------------");

        System.out.println("---PUBLIC KEY 2----");
        System.out.println(publicKey2);
        System.out.println("-----------------");

        File root = android.os.Environment.getExternalStorageDirectory();

        //byte[] array = hexStringToByteArray(hex);

        System.out.println("DATA");

        ByteArrayOutputStream bout = new ByteArrayOutputStream(714);

        byte[] input1 = new byte[128];
        byte[] input2 = new byte[128];
        byte[] input3 = new byte[128];
        byte[] input4 = new byte[128];
        byte[] input5 = new byte[128];
        byte[] input6 = new byte[74];

        byte[] result1 = new byte[128];
        byte[] result2 = new byte[128];
        byte[] result3 = new byte[128];
        byte[] result4 = new byte[128];
        byte[] result5 = new byte[128];
        byte[] result6 = new byte[74];

        int j = 0;
        for (int i =0; i < 128; i++){
            input1[j++] = array[(i+6)];
        }

        result1 = decode128(input1, asymmetricCipher);
        bout.write( result1);
        // System.out.print(decode128(result, asymmetricCipher));
        j = 0;
        for (int i =128; i < 256; i++){
            input2[j++] = array[(i+6)];

//            j++;
        }

        result2 =  decode128(input2, asymmetricCipher);
        bout.write( result2);
        j = 0;
        for (int i =256; i < 384; i++){
            input3[j++] = array[(i+6)];
        }
        result3 = decode128(input3, asymmetricCipher);

        bout.write( result3);
        j = 0;
        for (int i =384; i < 512; i++){
            input4[j++] = array[(i+6)];
        }
        result4 = decode128(input4, asymmetricCipher);
        bout.write( result4);
        j = 0;
        for (int i =512; i < 640; i++){
            input5[j++] = array[(i+6)];
        }
        result5 = decode128(input5, asymmetricCipher);
        bout.write(result5);

        j = 0;
        for (int i =640; i < 714; i++){
            input6[j++] = array[(i+6)];
        }

        asymmetricCipher.init(Cipher.DECRYPT_MODE, publicKey2);

        result6 = decode128(input6, asymmetricCipher);
        bout.write(result6 );
        //System.out.println(decode128(result2, asymmetricCipher2));

//        FileOutputStream fous = new FileOutputStream("/Users/Jay/Desktop/data/data.out.base64");
//        fous.write(Base64.encode(bout.toByteArray()));
//        fous.close();
//        bout.close();
//
//        FileOutputStream fou1 = new FileOutputStream("/Users/Jay/Desktop/data/data.out");
//        fou1.write(bout.toByteArray());
//        fou1.close();




//        FileOutputStream fous2= new FileOutputStream("/Users/Jay/Desktop/data/sample1.wi");
//        for (int i = 87; i < 128; i++)
//            fous2.write(result1[i]);
//        for (int i = 5; i < 128; i++)
//            fous2.write(result2[i]);
//        for (int i = 5; i < 128; i++)
//            fous2.write(result3[i]);
//        for (int i = 5; i < 128; i++)
//            fous2.write(result4[i]);
//        for (int i = 5; i < 128; i++)
//            fous2.write(result5[i]);
//        for (int i = 5; i < 74; i++)
//            fous2.write(result6[i]);
//        fous2.close();

        System.out.println("type:" + result1[5]);
        System.out.println("length section 1:" + result1[10]);
        System.out.println("length section 2:" + result1[12]);

        // Header section -- first 10 bytes:


        int stringSectionLength = result1[10];
        int        binarySectionLength = result1[12];
        int stringSectionStartPostion = 15;
        int stringSectionEndPostion = stringSectionStartPostion + stringSectionLength;

        byte[] slice = Arrays.copyOfRange(result1, stringSectionStartPostion, stringSectionEndPostion);
        String stringSection = new String(slice);
        System.out.println(stringSection);
        String s = stringSection.replaceAll("\\ufffd", ",");
        String[] res = s.split(",");
        System.out.println(res[11]);


        char[] arr1 = new char[50];

        for (int ind= 0; ind < stringSection.length(); ind++) {

            if (stringSection.charAt(ind) != 225 && stringSection.charAt(ind) != 224) {

                System.out.println(stringSection.charAt(ind));

            }
        }

        return res[11];

    }


    public static String  readLic(String hex) throws Exception {

        Security.addProvider(new BouncyCastleProvider());

        Cipher asymmetricCipher = Cipher.getInstance("RSA/NONE/NoPadding", "BC");


        String pubKey = "MIGWAoGBAMqfGO9sPz+kxaRh/qVKsZQGul7NdG1gonSS3KPXTjtcHTFfexA4MkGAmwKeu9XeTRFgMMxX99WmyaFvNzuxSlCFI/foCkx0TZCFZjpKFHLXryxWrkG1Bl9++gKTvTJ4rWk1RvnxYhm3n/Rxo2NoJM/822Oo7YBZ5rmk8NuJU4HLAhAYcJLaZFTOsYU+aRX4RmoF";
        String pubKey2 = "MF8CSwC0BKDfEdHKz/GhoEjU1XP5U6YsWD10klknVhpteh4rFAQlJq9wtVBUc5DqbsdI0w/bga20kODDahmGtASy9fae9dobZj5ZUJEw5wIQMJz+2XGf4qXiDJu0R2U4Kw==";


        KeyFactory kf = KeyFactory.getInstance("RSA");
        byte[] decodedPublicKey = Base64.decode(pubKey);
        byte[] decodedPublicKey2 = Base64.decode(pubKey2);
        BigInteger[] arr = parseASN1RsaPublicKey(decodedPublicKey);

        RSAPublicKeySpec keySpecX509 = new RSAPublicKeySpec(arr[0], arr[1]);
        RSAPublicKey publicKey = (RSAPublicKey) kf.generatePublic(keySpecX509);

        byte[] mykey = publicKey.getEncoded();
        String myenc = new String(Base64.encode(mykey));
        System.out.println(myenc);

        BigInteger[] arr2 = parseASN1RsaPublicKey(decodedPublicKey2);

        RSAPublicKeySpec keySpecX5092 = new RSAPublicKeySpec(arr2[0], arr2[1]);
        RSAPublicKey publicKey2 = (RSAPublicKey) kf.generatePublic(keySpecX5092);
        // initialize your cipher
        asymmetricCipher.init(Cipher.DECRYPT_MODE, publicKey);

        System.out.println("---PUBLIC KEY 1----");
        System.out.println(publicKey);
        System.out.println("-----------------");

        System.out.println("---PUBLIC KEY 2----");
        System.out.println(publicKey2);
        System.out.println("-----------------");

        File root = android.os.Environment.getExternalStorageDirectory();

        byte[] array = hexStringToByteArray(hex);

        System.out.println("DATA");

        ByteArrayOutputStream bout = new ByteArrayOutputStream(714);

        byte[] input1 = new byte[128];
        byte[] input2 = new byte[128];
        byte[] input3 = new byte[128];
        byte[] input4 = new byte[128];
        byte[] input5 = new byte[128];
        byte[] input6 = new byte[74];

        byte[] result1 = new byte[128];
        byte[] result2 = new byte[128];
        byte[] result3 = new byte[128];
        byte[] result4 = new byte[128];
        byte[] result5 = new byte[128];
        byte[] result6 = new byte[74];

        int j = 0;
        for (int i =0; i < 128; i++){
            input1[j++] = array[(i+6)];
        }

        result1 = decode128(input1, asymmetricCipher);
        bout.write( result1);
        // System.out.print(decode128(result, asymmetricCipher));
        j = 0;
        for (int i =128; i < 256; i++){
            input2[j++] = array[(i+6)];

//            j++;
        }

        result2 =  decode128(input2, asymmetricCipher);
        bout.write( result2);
        j = 0;
        for (int i =256; i < 384; i++){
            input3[j++] = array[(i+6)];
        }
        result3 = decode128(input3, asymmetricCipher);

        bout.write( result3);
        j = 0;
        for (int i =384; i < 512; i++){
            input4[j++] = array[(i+6)];
        }
        result4 = decode128(input4, asymmetricCipher);
        bout.write( result4);
        j = 0;
        for (int i =512; i < 640; i++){
            input5[j++] = array[(i+6)];
        }
        result5 = decode128(input5, asymmetricCipher);
        bout.write(result5);

        j = 0;
        for (int i =640; i < 714; i++){
            input6[j++] = array[(i+6)];
        }

        asymmetricCipher.init(Cipher.DECRYPT_MODE, publicKey2);

        result6 = decode128(input6, asymmetricCipher);
        bout.write(result6 );
        //System.out.println(decode128(result2, asymmetricCipher2));

//        FileOutputStream fous = new FileOutputStream("/Users/Jay/Desktop/data/data.out.base64");
//        fous.write(Base64.encode(bout.toByteArray()));
//        fous.close();
//        bout.close();
//
//        FileOutputStream fou1 = new FileOutputStream("/Users/Jay/Desktop/data/data.out");
//        fou1.write(bout.toByteArray());
//        fou1.close();




//        FileOutputStream fous2= new FileOutputStream("/Users/Jay/Desktop/data/sample1.wi");
//        for (int i = 87; i < 128; i++)
//            fous2.write(result1[i]);
//        for (int i = 5; i < 128; i++)
//            fous2.write(result2[i]);
//        for (int i = 5; i < 128; i++)
//            fous2.write(result3[i]);
//        for (int i = 5; i < 128; i++)
//            fous2.write(result4[i]);
//        for (int i = 5; i < 128; i++)
//            fous2.write(result5[i]);
//        for (int i = 5; i < 74; i++)
//            fous2.write(result6[i]);
//        fous2.close();

        System.out.println("type:" + result1[5]);
        System.out.println("length section 1:" + result1[10]);
        System.out.println("length section 2:" + result1[12]);

        // Header section -- first 10 bytes:


        int stringSectionLength = result1[10];
        int        binarySectionLength = result1[12];
        int stringSectionStartPostion = 15;
        int stringSectionEndPostion = stringSectionStartPostion + stringSectionLength;

        byte[] slice = Arrays.copyOfRange(result1, stringSectionStartPostion, stringSectionEndPostion);
        String stringSection = new String(slice);
        System.out.println(stringSection);
        String s = stringSection.replaceAll("\\ufffd", ",");
        String[] res = s.split(",");
        System.out.println(res[11]);


        char[] arr1 = new char[50];

        for (int ind= 0; ind < stringSection.length(); ind++) {

            if (stringSection.charAt(ind) != 225 && stringSection.charAt(ind) != 224) {

                System.out.println(stringSection.charAt(ind));

            }
        }

        return res[11];

    }
}


