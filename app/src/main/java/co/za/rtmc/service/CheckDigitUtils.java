package co.za.rtmc.service;

public final class CheckDigitUtils 
{
  /**
	 * Verhoeff dihedral addition matrix A + B = a[A][B]
	 */ 
	private static final int verhoeffAdditionMatrix[][] = { 
    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 2, 3, 4, 0, 6, 7, 8, 9, 5}, 
    {2, 3, 4, 0, 1, 7, 8, 9, 5, 6},
		{3, 4, 0, 1, 2, 8, 9, 5, 6, 7}, 
    {4, 0, 1, 2, 3, 9, 5, 6, 7, 8},
		{5, 9, 8, 7, 6, 0, 4, 3, 2, 1}, 
    {6, 5, 9, 8, 7, 1, 0, 4, 3, 2},
		{7, 6, 5, 9, 8, 2, 1, 0, 4, 3}, 
    {8, 7, 6, 5, 9, 3, 2, 1, 0, 4},
		{9, 8, 7, 6, 5, 4, 3, 2, 1, 0}};

	/**
	 *  Verhoeff dihedral inverse map, A + inverse[A] = 0
	 */
	private static final int verhoeffInverse[] = {0, 4, 3, 2, 1, 5, 6, 7, 8, 9};

	/**
	 * Verhoeff permutation weighting matrix P[position][value]
	 */
	private static final int verhoeffPermutationMatrix[][] = { 
    {0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		{1, 5, 7, 6, 2, 8, 3, 0, 9, 4}, 
    {5, 8, 0, 3, 7, 9, 6, 1, 4, 2},
		{8, 9, 1, 6, 0, 4, 3, 5, 2, 7}, 
    {9, 4, 5, 3, 1, 2, 6, 8, 7, 0},
		{4, 2, 8, 6, 5, 7, 3, 9, 0, 1}, 
    {2, 7, 9, 3, 8, 0, 6, 4, 1, 5},
		{7, 0, 4, 6, 9, 1, 3, 2, 5, 8}};
	
  /**
   * Private constructor to prevent instances of this utility class
   */
  private CheckDigitUtils()
  {
  }

  /**
   * Calculates the last digit of a supplied string as a South African ID type 
   * check digit. The method of calculating the check digit follows the 
   * following algorithm:
   * 
   * Using string 911108501000 as input
   * 1. Use the method calcCheckDigitTotalC to calculate Total C of the input
   *    i.e. Total C = 27
   * 2. Use the method genCheckDigitStr to calculate the check digit from 
   *    Total C, i.e. Check digit = 3
   * 
   * @param   rsaId   The South African ID to calculate the check digit for
   * @return  String  The calculated check digit
   */
  public static final String calcRSACheckDigit(String rsaId) 
  {    
    int     totalC      = calcCheckDigitTotalC(rsaId);
    
    // Calculate the Check Digit
    return genCheckDigitStr(totalC);
  }

  /**
   * Calculates the last digit of a supplied string as a TRN type check digit.
   * The method of calculating the check digit follows the following algorithm:
   * 
   * Using string 911108501000 as input
   * 1. Use the method calcCheckDigitTotalC to calculate Total C of the input
   *    i.e. Total C = 27
   * 2. Add one to the result of Total C, i.e. 27 + 1 = 28 Total D
   * 3. Use the method genCheckDigitStr to calculate the check digit from 
   *    Total D , i.e. Check Digit = 2
   * 
   * @param   trn     The TRN to calculate the check digit for
   * @return  String  The calculated check digit
   */
  public static final String calcTRNCheckDigit(String trn) 
  {
    int     totalD      = 0;   
    
    // Calculate Total D: Add 1 to total C
    totalD = calcCheckDigitTotalC(trn) + 1;

    // Calculate the Check Digit
    return genCheckDigitStr(totalD);
  }

  /**
   * Calculates the last digit of a supplied string as a licence type 
   * check digit. The method of calculating the check digit follows the 
   * following algorithm:
   * 
   * Using string 911108501000 as input
   * 1. Use the method calcCheckDigitTotalC to calculate Total C of the input
   *    i.e. Total C = 27
   * 2. Use the method genCheckDigitStr to calculate the check digit from 
   *    Total C, i.e. Check digit = 3
   * 
   * @param   licN    The licence number to calculate the check digit for
   * @return  String  The calculated check digit
   */
  public static final String calcLicCheckDigit(String licN) 
  {
    int totalC = calcCheckDigitTotalC(licN);

    // Calculate the Check Digit
    return genCheckDigitStr(totalC);
  }
  
  /**
   * Checks if the check digit of a given South African ID is valid.
   * 
   * @return  boolean true if the check digit is valid
   */
  public static boolean isValidRSACheckDigit(String rsaID) 
  {
    // The last digit is the check digit of the number
    int     idLen     = rsaID.length();
    String  checkStr  = rsaID.substring(0, idLen - 1);
    String  checkDgt  = rsaID.substring(idLen -1);
    String  calcDgt   = calcRSACheckDigit(checkStr);

    return checkDgt.equals(calcDgt);
  }
  
  /**
   * Checks if the check digit of a given TRN type number is valid.
   * 
   * @param   trn     The TRN type number to check
   * @return  boolean true if the check digit is valid
   */
  public static boolean isValidTRNCheckDigit(String trn) 
  {
    // The last digit is the check digit of the number
    int     idLen     = trn.length();
    String  checkStr  = trn.substring(0, idLen - 1);
    String  checkDgt  = trn.substring(idLen -1);
    String  calcDgt   = calcTRNCheckDigit(checkStr);

    return checkDgt.equals(calcDgt);
  }
  
  /**
   * Checks if the check digit of a given licence number is valid.
   * 
   * @param   licN      The licence number to check
   * @return  boolean   True if the check digit is valid
   */
  public static boolean isValidLicCheckDigit(String licN, String checkDgt) 
  {
    String calcDgt = calcLicCheckDigit(licN);

    return checkDgt.equals(calcDgt);
  }
  
  /**
   * Private method to process the portions of the check digit formulas common 
   * to TRN and RSA ID type check digits.
   * 
   * Using string 911108501000 as input
   * 1. Calculate Total A by summing all digits within the string which appear 
   *    in odd postions (all but position 13 and omit zero's) 
   *      i.e. 9+1+5+1 = 16 = Total A
   * 2. Calculate Total B as follows:
   *    a) Use all digits within the string which appear at even positions as a
   *        constant (but omit zero's)  and multiply the latter by 2 
   *        i.e. 118 * 2 = 236 
   *    b) All all the individual digits of the product to one another.  
   *        i.e. 2+3+6 = 11 = Total B
   * 3. Calculate Total C by summing totals A and B i.e. 11+16 = 27 = Total C
   * 
   * All alphabetic characters are converted to numeric by  using the unit
   * digit of the decimal representation of the ASCII character.
   * 
   * @param   inputStr  The string to calculate Total C of the check digit 
   *                    formula for.
   * @return  int       Total C of the Check Digit formula.
   */
  private static int calcCheckDigitTotalC(String inputStr) 
  {
    String  workStr     = null;
    int     workLen     = 0,
            totalA      = 0,
            totalB      = 0,
            totalC      = 0;

    // Check digit calculation must not include trailing whitespace
    workStr = inputStr.trim();
    workLen = workStr.length();

    // Calculate Total A: The sum of all digits in odd positions
    for (int i=0; i < workLen; i = i + 2)
    {
      char oddChar = workStr.charAt(i);

      // Use decimal value of digits, or ASCII value mod 10 for alphabetic 
      // characters
      if (Character.isDigit(oddChar)) {

        totalA += oddChar - '0';
      }
      else {                           /* Not a number */

        totalA += oddChar % 10;
      }                                
    }

    // Calculate Total B: 
    StringBuffer calcBStr = new StringBuffer();

    // Create a number from digits in odd positions (ingnoring 0's)
    for (int i=1; i < workLen; i = i + 2) 
    {
      char evenChar = workStr.charAt(i);
      if (evenChar != '0') 
      {
        if (!Character.isDigit(evenChar)) 
        {
          evenChar = (char)((evenChar % 10) + '0');
        }
        calcBStr.append(evenChar);
      }
    }

    // Multiply the new number by 2
    totalB = (calcBStr.length() == 0) ? 0 : Integer.parseInt(
                                              calcBStr.toString()) * 2;

    // Add the individual digits in the result
    String totBStr  = String.valueOf(totalB);
    totalB    = 0;
    for (int i=0; i < totBStr.length(); i++) 
      totalB += totBStr.charAt(i) - '0';
    
    // Calculate Total C: Sum of Totals A and B
    totalC = totalA + totalB;

    // Return total C
    return totalC;  
  }

  /**
   * Generate the check digit string from a given total as follows
   * Determine next higher multiple of 10 for the total and deduct the total
   * Should the result be 10, then the check digit is 0
   * e.g. 30 - 27 = check digit 3
   */
  private static String genCheckDigitStr(int total) 
  {
    int calcDigit  = 10 - (total % 10);

    if (calcDigit == 10) calcDigit = 0;
      
    return String.valueOf(calcDigit);
  }

  /**
   * Checks if the check digit of a given infringement notice number is valid.
   * 
   * @param   infringeNoticeN   The infringement notice number to check.
   * @return  boolean           True if the check digit is valid.
   */
  public static boolean isValidInfringeNoticeNCheckDigit(String infringeNoticeN) 
  {
    return isValidTRNCheckDigit(infringeNoticeN);
  }
  
  /**
   * Adds a checkDigit to the back of a number calculated with Verhoeff's 
   * algorithm.
   * 
   * @param number The number to which a checkDigit will be added.
   */
  public static String AddVerHoeffCheckDigit(String number) 
  {
		return number + Integer.toString(calculateVerhoeffCheckDigit(number));
	}

  /**
   * calculates a checkDigit for a given number according to 
   * Verhoeff's algorithm.
   * 
   * @param number for which to calculate the checkDigit.
   */
	public static int calculateVerhoeffCheckDigit(String number) 
  {
		int checkDigit = 0;
    
    StringBuffer numbuff = new StringBuffer(number);
    numbuff = numbuff.reverse();
    
		for(int i = 0; i < numbuff.length(); ++i) 
    {
			int digit = numbuff.charAt(i) - '0';
      
	   	if (digit < 0 || digit > 9) 
      {
        throw new NumberFormatException(
          "CheckDigit calculation requires only numeric values: " + 
          "[" + number + "]" +
          " contains a non numeric value: " +
          "[" + number.charAt(i) +  "].");
	    }
      
			checkDigit = 
        verhoeffAdditionMatrix
          [checkDigit][verhoeffPermutationMatrix[(i + 1) % 8][digit]];
		}
    
		return verhoeffInverse[checkDigit];
	}
	
  /**
   * Verify a number to have the correct checkDigit.
   * 
   * @param number The number to check.
   */
	public static boolean isValidVerhoeffCheckDigit(String number) 
  {
		int checkDigit = 0;
    int digitToCheck = Integer.parseInt(number.substring(number.length() - 1));
    
    checkDigit = 
      calculateVerhoeffCheckDigit(number.substring(0, number.length() - 1));
      
    return checkDigit == digitToCheck;
	}

  /**
   * Calculates the check digit using the Luhn algorithm described below:
   * 
   * 1. Exclude the right-most digit from the calculation as this is the actual 
   *    check-digit to be examined for validity.
   * 2. Starting with the second last digit and moving right to left, 
   *    alternatively multiply each successive digit by 2 and 1 respectively.
   * 3. Sum the integers comprising the product obtained from each of the 
   *    calculations.
   * 4. Subtract the resulting sum from the next higher multiple of ten (10). 
   *    The resulting value is the desired account number check-digit.
   * 
   * The following example uses the number of: 4287 9478
   * 
   * 1. Example account number: 4 2 8 7 9 4 7 (8)
   * 2. Multiplier products: 8(4x2) 2(2x1) 16(8x2) 7(7x1) 18(9x2) 4(4x1) 14(7x2)
   * 3. Sum the integers: 8 + 2 + (1+6) + 7 + (1+8) + 4 + (1+4) = 42
   * 4. Derive the check digit: 50 - 42 = 8
   * 
   * @param number for which to calculate the check digit.
   */
  public static int calculateLuhnCheckDigit(String number) 
  {
    String products = "";
    StringBuffer numbuff = new StringBuffer(number);   
    numbuff = numbuff.reverse();   
    
    int multiplier = 2;
    for (int i = 0; i < numbuff.length(); ++i) 
    {
      int digit = numbuff.charAt(i) - '0';      
      
      if (digit < 0 || digit > 9) 
      {
        throw new NumberFormatException(
          "CheckDigit calculation requires only numeric values: " + 
          "[" + number + "]" + " contains a non numeric value: " +
          "[" + number.charAt(i) + "].");
      }

      products = (digit * multiplier) + products;
      
      if (multiplier == 2)
      {
        multiplier = 1;
      }
      else
      {
        multiplier = 2;
      }
    }
    
    int total = 0;    
    for (int i = 0; i < products.length(); i++) 
    {
      int digit = products.charAt(i) - '0';
      
      total += digit;
    }
          
    int checkDigit  = 10 - (total % 10);

    if (checkDigit == 10) checkDigit = 0;
    
    return checkDigit;
  }
  
  /**
   * Credit card numbers are checked for validity using the Luhn check-digit 
   * algorithm (also known as "Mod-10 checking"). 
   * 
   * If the calculated check digit does not correspond to the last digit of the 
   * credit card number, the number is invalid.
   * 
   * @param number The number to check.
   */
  public static boolean isValidCrediCardNCheckDigit(String number) 
  {
    int checkDigit = 0;
    int digitToCheck = Integer.parseInt(number.substring(number.length() - 1));
    
    checkDigit = calculateLuhnCheckDigit(number.substring(0, 
                                                          number.length() - 1));
      
    return checkDigit == digitToCheck;
  }
}