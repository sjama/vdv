package co.za.rtmc.service;

import java.util.Calendar;
import java.util.regex.Pattern;

/**
 * Created by ngwane1 on 6/20/2016.
 */
public class Validator {
    public static String RSA_IDENTITY_DOCUMENT    = "RSA ID";
    public static String FOREIGN_IDENTITY_DOCUMENT      = "Passport";
    public static String TRAFFIC_REGISTER_NUMBER  = "Traffic Register Number";
    //regular expressions
    /** Wild card character */
    public static final String WILD_CARD_CHAR = "%";
    public static final String REGEX_ALPHA_NUMERIC_WILD = "[^A-Z0-9]+[^"+ WILD_CARD_CHAR + "_" +"]";
    public static final String REGEX_ALPHA_NUMERIC = "[^A-Z0-9]";
    public static String VIN_N      = "Vin/Chassis Number";

    /**
     * Method to validate that an identification number is of the correct lenth.
     * @param idDocTypeCd The type of the identification document number.
     * @param idDocN The identification document number to be validated.
     * @return boolean A flag indicating the result of the validation.
     */
    public static boolean isIdDocNValidLength(String idDocTypeCd, String idDocN)
    {
        boolean result = true;

        // if is RSA/TRN/BRN it must be 13 digits long
        if (TRAFFIC_REGISTER_NUMBER.equals(idDocTypeCd) ||
                RSA_IDENTITY_DOCUMENT.equals(idDocTypeCd))
        {
            if (!isValidLength(idDocN, 13))
            {
                result = false;
            }
        }
        return result;
    }

    /**
     * Validate if the length of a String is correct.
     *
     * @param   field   The field value to tested.
     * @param   length  The required length of the field.
     * @return  boolean A flag indicating the result of the validation.
     */
    public static boolean isValidLength(String field, int length)
    {
        return isEmpty(field) ? false : (field.length() == length);
    }

    /**
     * Checks whether the given field is mandatory or not.
     *
     * @param  value   The value that was entered.
     * @return boolean A flag indicating the result of the validation.
     */
    public static boolean isEmpty(String value)
    {
        return (value == null || "".equals(value));
    }


    /**
     * Method to validate that an identification number is alpha numeric for a specific
     * idDocTypeCd.
     * @param idDocTypeCd The type of the identification document number.
     * @param idDocN The identification document number to be validated.
     * @return boolean A flag indicating the result of the validation.
     */
    public static boolean isIdDocNAlphaNumeric(String idDocTypeCd, String idDocN)
    {
        // if is RSA,TRN or PSEUDO person id then it must be numeric
        if (TRAFFIC_REGISTER_NUMBER.equals(idDocTypeCd) ||
            FOREIGN_IDENTITY_DOCUMENT.equals(idDocTypeCd) )
        {
            return isAlphaNumeric(idDocN);
        }
        return true;
    }

    /**
     * Validate is given string is alphanumeric, not catering for wildcard
     * special character(%)
     *
     * @param text
     * @return true if it is apha numeric, i.e. contains no special characters
     */
    public static boolean isAlphaNumeric(String text)
    {
        return isAlphaNumeric(text,false);
    }

    /**
     * Validate is given string is alphanumeric also catering for wildcard
     * special character(%), if allowWildCrad is true.
     *
     * @param text the string to be evaluated.
     * @param allowWildCard the indicator of whether or not ot allow wildcard special
     *        character.
     * @return true if it is apha numeric, i.e. contains no special characters
     *         excluding the wildcard character, when allowingWilcard
     */
    public static boolean isAlphaNumeric(String text, boolean allowWildCard)
    {
        //search for any character which is not in A-Z, 0-9 inclusive
        if(text != null)
        {
            String regex = allowWildCard ? REGEX_ALPHA_NUMERIC_WILD : REGEX_ALPHA_NUMERIC;
            return !Pattern.compile(regex).matcher(text).find();
        }
        return true;
    }

    /**
     * Method to validate that an identification number contains the correct
     * check digit.
     * @param idDocTypeCd The type of the identification document number.
     * @param idDocN The identification document number to be validated.
     * @return boolean A flag indicating the result of the validation.
     */
    public static boolean isCheckDigitValid(String idDocTypeCd, String idDocN)
    {
        boolean valid = true;

        if (RSA_IDENTITY_DOCUMENT.equals(idDocTypeCd))
        {
            if (!CheckDigitUtils.isValidRSACheckDigit(idDocN))
            {
                valid = false;
            }
        }
        else if (TRAFFIC_REGISTER_NUMBER.equals(idDocTypeCd))
        {
            if (!CheckDigitUtils.isValidTRNCheckDigit(idDocN))
            {
                valid = false;
            }
        }


        return valid;
    }

    /**
     * Check if date is too far into in the future or into the past from a given
     * date.
     *
     * @param   date       The java.util.Date date to be checked.
     * @param   effectiveD The effective date.
     * @param   field      The Calendar constant indicating the date field to be
     *                     checked.
     * @param   numberOfUnits     The number of date units to be checked for.
     *                     If the number is positive, the date will be treated as
     *                     a future date, if the number is negative the date will
     *                     be treated as a date in the past.
     * @return  boolean    A flag indicating the result of the validation.
     */
    public static boolean isDateOutOfRange(
            java.util.Date date,
            java.util.Date effectiveD,
            int field,
            int numberOfUnits)
    {

        java.util.Date compareDate;
        boolean        result = false;


        Calendar dateCal = Calendar.getInstance();
        dateCal.setTime(date);

        //Set to midnight
        dateCal.set(dateCal.get(Calendar.YEAR),
                dateCal.get(Calendar.MONTH),
                dateCal.get(Calendar.DATE),
                0,
                0,
                0);

        dateCal.set(Calendar.MILLISECOND, 0);
        dateCal.getTimeInMillis();

        Calendar  effectiveDCal = Calendar.getInstance();
        effectiveDCal.setTime(effectiveD);

        //Set to midnight
        effectiveDCal.set(effectiveDCal.get(Calendar.YEAR),
                effectiveDCal.get(Calendar.MONTH),
                effectiveDCal.get(Calendar.DATE),
                0,
                0,
                0);

        effectiveDCal.set(Calendar.MILLISECOND, 0);
        effectiveDCal.getTimeInMillis();

        // Create a date that has the number of time units (<<number>>) added to it
        compareDate = add(effectiveDCal.getTime(), field, numberOfUnits);
        effectiveDCal.clear();
        effectiveDCal.setTime(compareDate);

        if (numberOfUnits > 0)
        {
            // Compare the future date with the compare date
            if (dateCal.after(effectiveDCal))
            {
                result = true;
            }
        }
        else
        {
            // Compare the old date with the compare date
            if (dateCal.before(effectiveDCal))
            {
                result = true;
            }
        }

        return result;
    }

    /**
     * Returns a java.util.Date to which the number of date units specified has
     * been added. If a negative number was specified, then they will be subtracted.
     *
     * @param  date           The java.util.Date date which must be added to
     *                        (or subtracted from).
     * @param  field          The Calendar constant indicating which date field
     *                        to modify.
     * @param  number         The number of units which must added (or subtracted).
     * @return java.util.Date The date to which the number of units has been added
     */
    private static java.util.Date add(java.util.Date date, int field, int number)
    {
        // Create a Calendar instance and set its date
        Calendar cDate = Calendar.getInstance();
        cDate.setTime(date);

        // Add the specified number of time units
        cDate.add(field, number);

        return cDate.getTime();
    }


}
