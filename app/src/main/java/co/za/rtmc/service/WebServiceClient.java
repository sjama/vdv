package co.za.rtmc.service;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONObject;

import co.za.rtmc.enums.AppConstants;
import co.za.rtmc.vo.RdtstrptresVoRwsVo;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;
import cz.msebera.android.httpclient.protocol.HTTP;

/**
 * Created by Jama on 2016/07/07.
 */
public class WebServiceClient {

   /// public final String TOKEN_REG_URL = "http://172.20.10.8:9090/backoffice/svs/cloudMessages/registerToken";
    public final String TOKEN_REG_URL = "https://services.rtmc.co.za/backoffice/svs/cloudMessages/registerToken";
    public final String CONTENT_TYPE_JSON = "appliction/json";
    public final String GCM_SENDER_ID = "749603800113";
    private Context applicationContext;
    boolean result = false;

    public WebServiceClient(Context context) {
        this.applicationContext = context;
    }

    public boolean registerDeviceToken(String obj){
        StringEntity entity = null;

        try {
            Gson gson = new GsonBuilder().create();
            JSONObject jsonParams = new JSONObject();


            //voRwsVo.getRdtstrptRwsVo().set
            entity = new StringEntity(obj);
            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, CONTENT_TYPE_JSON));
        } catch (Exception e) {
            e.printStackTrace();
        }


       // AsyncHttpClient client = new AsyncHttpClient();

        AsyncHttpClient client = new AsyncHttpClient(true, 80, 443);
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        // client.post(getApplicationContext(),"http://41.185.29.249:9090/phepha/svs/userspRws/doLogin", entity,"application/json", new AsyncHttpResponseHandler() {
        client.post(applicationContext, TOKEN_REG_URL, entity, CONTENT_TYPE_JSON, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                try {
                    // JSON Object
                    System.out.println(responseBody);
                    result = Boolean.parseBoolean(responseBody);

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    ///Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }

            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                //if (NetworkUtil.getConnectivityStatus(getApplicationContext()) == NetworkUtil.TYPE_NOT_CONNECTED ){
                // }
                if (statusCode == 404) {
                   // Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    //Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                   /// Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });

        return  result;
    }

    public void syncToRemoteDB(RdtstrptresVoRwsVo voRwsVo){
        {
            StringEntity entity = null;

            try {
                //entity = new ByteArrayEntity(new JSONObject().toString().getBytes("UTF-8"));
                Gson gson = new GsonBuilder().create();
                JSONObject jsonParams = new JSONObject();


                //voRwsVo.getRdtstrptRwsVo().set
                entity = new StringEntity(gson.toJson(voRwsVo));
                entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, CONTENT_TYPE_JSON));
            } catch (Exception e) {
                e.printStackTrace();
            }


            AsyncHttpClient client = new AsyncHttpClient();
            // client.post(getApplicationContext(),"http://41.185.29.249:9090/phepha/svs/userspRws/doLogin", entity,"application/json", new AsyncHttpResponseHandler() {
            client.post(applicationContext,  AppConstants.APP_URL + "rdtstrptresRws/addVo", entity, CONTENT_TYPE_JSON, new TextHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseBody) {
                    try {
                        // JSON Object
                        System.out.println(responseBody);
                        result = Boolean.parseBoolean(responseBody);

                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        ///Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                        e.printStackTrace();

                    }

                }


                @Override
                public void onFailure(int statusCode, Header[] headers, String responseBody, Throwable error) {
                    //if (NetworkUtil.getConnectivityStatus(getApplicationContext()) == NetworkUtil.TYPE_NOT_CONNECTED ){
                    // }
                    if (statusCode == 404) {
                        // Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code is '500'
                    else if (statusCode == 500) {
                        //Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                    }
                    // When Http response code other than 404, 500
                    else {
                        /// Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                    }
                }
            });

        }

    }
}
