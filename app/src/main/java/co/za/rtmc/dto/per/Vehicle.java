package co.za.rtmc.dto.per;

import co.za.rtmc.dto.PrevOwner;

public class Vehicle
{
    private co.za.rtmc.dto.Owner Owner;

    public PrevOwner getPrevOwner() {
        return prevOwner;
    }

    public void setPrevOwner(PrevOwner prevOwner) {
        this.prevOwner = prevOwner;
    }

    private PrevOwner prevOwner;

    private co.za.rtmc.dto.VehicleDet VehicleDet;

    public co.za.rtmc.dto.Owner getOwner ()
    {
        return Owner;
    }

    public void setOwner (co.za.rtmc.dto.Owner Owner)
    {
        this.Owner = Owner;
    }

    public co.za.rtmc.dto.VehicleDet getVehicleDet ()
    {
        return VehicleDet;
    }

    public void setVehicleDet (co.za.rtmc.dto.VehicleDet VehicleDet)
    {
        this.VehicleDet = VehicleDet;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Owner = "+Owner+", VehicleDet = "+VehicleDet+"]";
    }
}