package co.za.rtmc.dto;

public class Owner
{
    private co.za.rtmc.dto.per.PerDet PerDet;

    public co.za.rtmc.dto.per.PerDet getPerDet ()
    {
        return PerDet;
    }

    public void setPerDet (co.za.rtmc.dto.per.PerDet PerDet)
    {
        this.PerDet = PerDet;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [PerDet = "+PerDet+"]";
    }
}