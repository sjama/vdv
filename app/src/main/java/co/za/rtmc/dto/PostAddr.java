
package co.za.rtmc.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class PostAddr {

    @SerializedName("Addr1")
    @Expose
    private String addr1;
    @SerializedName("Addr2")
    @Expose
    private String addr2;
    @SerializedName("Addr4")
    @Expose
    private String addr4;
    @SerializedName("Addr5")
    @Expose
    private String addr5;
    @SerializedName("PostalCd")
    @Expose
    private co.za.rtmc.dto.per.PostalCd postalCd;

    /**
     * 
     * @return
     *     The addr1
     */
    public String getAddr1() {
        return addr1;
    }

    /**
     * 
     * @param addr1
     *     The Addr1
     */
    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    /**
     * 
     * @return
     *     The addr2
     */
    public String getAddr2() {
        return addr2;
    }

    /**
     * 
     * @param addr2
     *     The Addr2
     */
    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    /**
     * 
     * @return
     *     The addr4
     */
    public String getAddr4() {
        return addr4;
    }

    /**
     * 
     * @param addr4
     *     The Addr4
     */
    public void setAddr4(String addr4) {
        this.addr4 = addr4;
    }

    /**
     * 
     * @return
     *     The addr5
     */
    public String getAddr5() {
        return addr5;
    }

    /**
     * 
     * @param addr5
     *     The Addr5
     */
    public void setAddr5(String addr5) {
        this.addr5 = addr5;
    }

    /**
     * 
     * @return
     *     The postalCd
     */
    public co.za.rtmc.dto.per.PostalCd getPostalCd() {
        return postalCd;
    }

    /**
     * 
     * @param postalCd
     *     The PostalCd
     */
    public void setPostalCd(co.za.rtmc.dto.per.PostalCd postalCd) {
        this.postalCd = postalCd;
    }

}
