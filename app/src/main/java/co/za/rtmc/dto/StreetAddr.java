
package co.za.rtmc.dto;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StreetAddr {

    @SerializedName("Addr1")
    @Expose
    private String addr1;
    @SerializedName("Addr2")
    @Expose
    private String addr2;
    @SerializedName("Addr3")
    @Expose
    private String addr3;
    @SerializedName("Addr4")
    @Expose
    private String addr4;
    @SerializedName("PostalCd")
    @Expose
    private PostalCd_ postalCd;

    /**
     * 
     * @return
     *     The addr1
     */
    public String getAddr1() {
        return addr1;
    }

    /**
     * 
     * @param addr1
     *     The Addr1
     */
    public void setAddr1(String addr1) {
        this.addr1 = addr1;
    }

    /**
     * 
     * @return
     *     The addr2
     */
    public String getAddr2() {
        return addr2;
    }

    /**
     * 
     * @param addr2
     *     The Addr2
     */
    public void setAddr2(String addr2) {
        this.addr2 = addr2;
    }

    /**
     * 
     * @return
     *     The addr3
     */
    public String getAddr3() {
        return addr3;
    }

    /**
     * 
     * @param addr3
     *     The Addr3
     */
    public void setAddr3(String addr3) {
        this.addr3 = addr3;
    }

    /**
     * 
     * @return
     *     The addr4
     */
    public String getAddr4() {
        return addr4;
    }

    /**
     * 
     * @param addr4
     *     The Addr4
     */
    public void setAddr4(String addr4) {
        this.addr4 = addr4;
    }

    /**
     * 
     * @return
     *     The postalCd
     */
    public PostalCd_ getPostalCd() {
        return postalCd;
    }

    /**
     * 
     * @param postalCd
     *     The PostalCd
     */
    public void setPostalCd(PostalCd_ postalCd) {
        this.postalCd = postalCd;
    }

}
