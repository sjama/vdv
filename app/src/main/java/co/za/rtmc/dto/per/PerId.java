package co.za.rtmc.dto.per;

/**
 * Created by Jay on 2017/12/08.
 */
public class PerId
{
    private IdDocType idDocType;

    public IdDocType getIdDocType() { return this.idDocType; }

    public void setIdDocType(IdDocType idDocType) { this.idDocType = idDocType; }

    private String idDocN;

    public String getIdDocN() { return this.idDocN; }

    public void setIdDocN(String idDocN) { this.idDocN = idDocN; }

    private String initials;

    public String getInitials() { return this.initials; }

    public void setInitials(String initials) { this.initials = initials; }

    private String busOrSurname;

    public String getBusOrSurname() { return this.busOrSurname; }

    public void setBusOrSurname(String busOrSurname) { this.busOrSurname = busOrSurname; }
}
