package co.za.rtmc.dto.per;

/**
 * Created by Jay on 2017/12/08.
 */
public class PostAddr
{
    private String addr1;

    public String getAddr1() { return this.addr1; }

    public void setAddr1(String addr1) { this.addr1 = addr1; }

    private String addr2;

    public String getAddr2() { return this.addr2; }

    public void setAddr2(String addr2) { this.addr2 = addr2; }

    private String addr4;

    public String getAddr4() { return this.addr4; }

    public void setAddr4(String addr4) { this.addr4 = addr4; }

    private PostalCd postalCd;

    public PostalCd getPostalCd() { return this.postalCd; }

    public void setPostalCd(PostalCd postalCd) { this.postalCd = postalCd; }
}
