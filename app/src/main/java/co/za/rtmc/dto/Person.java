
package co.za.rtmc.dto;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import co.za.rtmc.dto.per.Learner;
import co.za.rtmc.dto.per.Licence;

public class Person {

    @SerializedName("PerDet")
    @Expose
    private co.za.rtmc.dto.per.PerDet perDet;
    @SerializedName("Licence")
    @Expose
    private List<co.za.rtmc.dto.per.Licence> licence = new ArrayList<co.za.rtmc.dto.per.Licence>();
    @SerializedName("LicCard")
    @Expose
    private co.za.rtmc.dto.per.LicCard licCard;
    @SerializedName("PrDP")
    @Expose
    private PrDP prDP;
    @SerializedName("Learner")
    @Expose
    private co.za.rtmc.dto.per.Learner learner;

    /**
     * 
     * @return
     *     The perDet
     */
    public co.za.rtmc.dto.per.PerDet getPerDet() {
        return perDet;
    }

    /**
     * 
     * @param perDet
     *     The PerDet
     */
    public void setPerDet(co.za.rtmc.dto.per.PerDet perDet) {
        this.perDet = perDet;
    }

    /**
     * 
     * @return
     *     The licence
     */
    public List<co.za.rtmc.dto.per.Licence> getLicence() {
        return licence;
    }

    /**
     * 
     * @param licence
     *     The Licence
     */
    public void setLicence(List<Licence> licence) {
        this.licence = licence;
    }

    /**
     * 
     * @return
     *     The licCard
     */
    public co.za.rtmc.dto.per.LicCard getLicCard() {
        return licCard;
    }

//    /**
//     *
//     * @param licCard
//     *     The LicCard
//     */
//    public void setLicCard(LicCard licCard) {
//        this.licCard = licCard;
//    }

    /**
     * 
     * @return
     *     The prDP
     */
    public PrDP getPrDP() {
        return prDP;
    }

    /**
     * 
     * @param prDP
     *     The PrDP
     */
    public void setPrDP(PrDP prDP) {
        this.prDP = prDP;
    }

    /**
     * 
     * @return
     *     The learner
     */
    public co.za.rtmc.dto.per.Learner getLearner() {
        return learner;
    }

    /**
     * 
     * @param learner
     *     The Learner
     */
    public void setLearner(Learner learner) {
        this.learner = learner;
    }

}
