package co.za.rtmc.dto.per;

/**
 * Created by Jay on 2017/12/08.
 */
public class RegAuthCd
{
    private int code;

    public int getCode() { return this.code; }

    public void setCode(int code) { this.code = code; }

    private String desc;

    public String getDesc() { return this.desc; }

    public void setDesc(String desc) { this.desc = desc; }
}
