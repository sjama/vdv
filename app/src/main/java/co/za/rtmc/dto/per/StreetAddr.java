package co.za.rtmc.dto.per;

/**
 * Created by Jay on 2017/12/08.
 */
public class StreetAddr
{
    private String addr1;

    public String getAddr1() { return this.addr1; }

    public void setAddr1(String addr1) { this.addr1 = addr1; }

    private String addr2;

    public String getAddr2() { return this.addr2; }

    public void setAddr2(String addr2) { this.addr2 = addr2; }

    private String addr3;

    public String getAddr3() { return this.addr3; }

    public void setAddr3(String addr3) { this.addr3 = addr3; }

    private PostalCd2 postalCd;

    public PostalCd2 getPostalCd() { return this.postalCd; }

    public void setPostalCd(PostalCd2 postalCd) { this.postalCd = postalCd; }
}
