package co.za.rtmc.dto.per;

public class LicCard
{
    private String driLicNr;

    public String getDriLicNr() { return this.driLicNr; }

    public void setDriLicNr(String driLicNr) { this.driLicNr = driLicNr; }

    private String cardExpireD;

    public String getCardExpireD() { return this.cardExpireD; }

    public void setCardExpireD(String cardExpireD) { this.cardExpireD = cardExpireD; }
}


