package co.za.rtmc.dto;

public class Vehicle
{
    private Owner Owner;

    public PrevOwner getPrevOwner() {
        return prevOwner;
    }

    public void setPrevOwner(PrevOwner prevOwner) {
        this.prevOwner = prevOwner;
    }

    private PrevOwner prevOwner;

    private VehicleDet VehicleDet;

    public Owner getOwner ()
    {
        return Owner;
    }

    public void setOwner (Owner Owner)
    {
        this.Owner = Owner;
    }

    public VehicleDet getVehicleDet ()
    {
        return VehicleDet;
    }

    public void setVehicleDet (VehicleDet VehicleDet)
    {
        this.VehicleDet = VehicleDet;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Owner = "+Owner+", VehicleDet = "+VehicleDet+"]";
    }
}