/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.rtmc.dto;

/**
 *
 * @author Ngwane
 */
public class MvLicDiscVo {
    private static final long serialVersionUID = 1L;
    private String certN  = "";
    private String userNumber  = "";
    private String docCtrlN  = "";
    private String mvRegN  = "";
    private String regtN  = "";
    private String mvDescCdDesc  = "";
    private String make  = "";
    private String model  = "";
    private String color  = "";
    private String vin  = "";
    private String engineNo  = "";
    private String Expiry  = "";

    public String getCertN() {
        return certN;
    }

    public void setCertN(String certN) {
        this.certN = certN;
    }

    public String getUsernumber() {
        return userNumber;
    }

    public void setUsernumber(String usernumber) {
        this.userNumber = usernumber;
    }

    public String getDocCtrlN() {
        return docCtrlN;
    }

    public void setDocCtrlN(String docCtrlN) {
        this.docCtrlN = docCtrlN;
    }

    public String getMvRegN() {
        return mvRegN;
    }

    public void setMvRegN(String mvRegN) {
        this.mvRegN = mvRegN;
    }

    public String getRegtN() {
        return regtN;
    }

    public void setRegtN(String regtN) {
        this.regtN = regtN;
    }

    public String getMvDescCdDesc() {
        return mvDescCdDesc;
    }

    public void setMvDescCdDesc(String mvDescCdDesc) {
        this.mvDescCdDesc = mvDescCdDesc;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getExpiry() {
        return Expiry;
    }

    public void setExpiry(String expiry) {
        Expiry = expiry;
    }
    
}
