
package co.za.rtmc.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import co.za.rtmc.dto.per.VehRestr;

public class Licence {

    @SerializedName("DriLic")
    @Expose
    private co.za.rtmc.dto.per.DriLic driLic;
    @SerializedName("DateFirstIss")
    @Expose
    private String dateFirstIss;
    @SerializedName("DriLicAuthD")
    @Expose
    private String driLicAuthD;
    @SerializedName("VehRestr")
    @Expose
    private co.za.rtmc.dto.per.VehRestr vehRestr;
    @SerializedName("DriLicValidFromD")
    @Expose
    private String driLicValidFromD;

    /**
     * 
     * @return
     *     The driLic
     */
    public co.za.rtmc.dto.per.DriLic getDriLic() {
        return driLic;
    }

    /**
     * 
     * @param driLic
     *     The DriLic
     */
    public void setDriLic(co.za.rtmc.dto.per.DriLic driLic) {
        this.driLic = driLic;
    }

    /**
     * 
     * @return
     *     The dateFirstIss
     */
    public String getDateFirstIss() {
        return dateFirstIss;
    }

    /**
     * 
     * @param dateFirstIss
     *     The DateFirstIss
     */
    public void setDateFirstIss(String dateFirstIss) {
        this.dateFirstIss = dateFirstIss;
    }

    /**
     * 
     * @return
     *     The driLicAuthD
     */
    public String getDriLicAuthD() {
        return driLicAuthD;
    }

    /**
     * 
     * @param driLicAuthD
     *     The DriLicAuthD
     */
    public void setDriLicAuthD(String driLicAuthD) {
        this.driLicAuthD = driLicAuthD;
    }

    /**
     * 
     * @return
     *     The vehRestr
     */
    public co.za.rtmc.dto.per.VehRestr getVehRestr() {
        return vehRestr;
    }

    /**
     * 
     * @param vehRestr
     *     The VehRestr
     */
    public void setVehRestr(VehRestr vehRestr) {
        this.vehRestr = vehRestr;
    }

    /**
     * 
     * @return
     *     The driLicValidFromD
     */
    public String getDriLicValidFromD() {
        return driLicValidFromD;
    }

    /**
     * 
     * @param driLicValidFromD
     *     The DriLicValidFromD
     */
    public void setDriLicValidFromD(String driLicValidFromD) {
        this.driLicValidFromD = driLicValidFromD;
    }

}
