
package co.za.rtmc.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StdRespHeader {

    @SerializedName("TxanName")
    @Expose
    private String txanName;
    @SerializedName("ESTxanID")
    @Expose
    private String eSTxanID;
    @SerializedName("ESUID")
    @Expose
    private String eSUID;
    @SerializedName("ESTermID")
    @Expose
    private String eSTermID;

    /**
     * 
     * @return
     *     The txanName
     */
    public String getTxanName() {
        return txanName;
    }

    /**
     * 
     * @param txanName
     *     The TxanName
     */
    public void setTxanName(String txanName) {
        this.txanName = txanName;
    }

    /**
     * 
     * @return
     *     The eSTxanID
     */
    public String getESTxanID() {
        return eSTxanID;
    }

    /**
     * 
     * @param eSTxanID
     *     The ESTxanID
     */
    public void setESTxanID(String eSTxanID) {
        this.eSTxanID = eSTxanID;
    }

    /**
     * 
     * @return
     *     The eSUID
     */
    public String getESUID() {
        return eSUID;
    }

    /**
     * 
     * @param eSUID
     *     The ESUID
     */
    public void setESUID(String eSUID) {
        this.eSUID = eSUID;
    }

    /**
     * 
     * @return
     *     The eSTermID
     */
    public String getESTermID() {
        return eSTermID;
    }

    /**
     * 
     * @param eSTermID
     *     The ESTermID
     */
    public void setESTermID(String eSTermID) {
        this.eSTermID = eSTermID;
    }

}
