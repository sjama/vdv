package co.za.rtmc.dto.per;

/**
 * Created by Jay on 2017/12/14.
 */

public class PrDPCatP {
    private String code;

    public String getCode() { return this.code; }

    public void setCode(String code) { this.code = code; }

    private String desc;

    public String getDesc() { return this.desc; }

    public void setDesc(String desc) { this.desc = desc; }
}
