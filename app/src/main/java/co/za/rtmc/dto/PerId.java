
package co.za.rtmc.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PerId {

    @SerializedName("IdDocType")
    @Expose
    private co.za.rtmc.dto.per.IdDocType idDocType;
    @SerializedName("IdDocN")
    @Expose
    private String idDocN;
    @SerializedName("Initials")
    @Expose
    private String initials;
    @SerializedName("BusOrSurname")
    @Expose
    private String busOrSurname;

    /**
     * 
     * @return
     *     The idDocType
     */
    public co.za.rtmc.dto.per.IdDocType getIdDocType() {
        return idDocType;
    }

    /**
     * 
     * @param idDocType
     *     The IdDocType
     */
    public void setIdDocType(co.za.rtmc.dto.per.IdDocType idDocType) {
        this.idDocType = idDocType;
    }

    /**
     * 
     * @return
     *     The idDocN
     */
    public String getIdDocN() {
        return idDocN;
    }

    /**
     * 
     * @param idDocN
     *     The IdDocN
     */
    public void setIdDocN(String idDocN) {
        this.idDocN = idDocN;
    }

    /**
     * 
     * @return
     *     The initials
     */
    public String getInitials() {
        return initials;
    }

    /**
     * 
     * @param initials
     *     The Initials
     */
    public void setInitials(String initials) {
        this.initials = initials;
    }

    /**
     * 
     * @return
     *     The busOrSurname
     */
    public String getBusOrSurname() {
        return busOrSurname;
    }

    /**
     * 
     * @param busOrSurname
     *     The BusOrSurname
     */
    public void setBusOrSurname(String busOrSurname) {
        this.busOrSurname = busOrSurname;
    }

}
