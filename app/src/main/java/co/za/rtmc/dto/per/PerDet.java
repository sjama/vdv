package co.za.rtmc.dto.per;

import java.util.ArrayList;

/**
 * Created by Jay on 2017/12/08.
 */
public class PerDet
{
    private PerId perId;

    public PerId getPerId() { return this.perId; }

    public void setPerId(PerId perId) { this.perId = perId; }

    private String perIdN;

    public String getPerIdN() { return this.perIdN; }

    public void setPerIdN(String perIdN) { this.perIdN = perIdN; }

    private NatOfPer natOfPer;

    public NatOfPer getNatOfPer() { return this.natOfPer; }

    public void setNatOfPer(NatOfPer natOfPer) { this.natOfPer = natOfPer; }

    private NatPopGrp natPopGrp;

    public NatPopGrp getNatPopGrp() { return this.natPopGrp; }

    public void setNatPopGrp(NatPopGrp natPopGrp) { this.natPopGrp = natPopGrp; }

    private String birthDate;

    public String getBirthDate() { return this.birthDate; }

    public void setBirthDate(String birthDate) { this.birthDate = birthDate; }

    private int age;

    public int getAge() { return this.age; }

    public void setAge(int age) { this.age = age; }

    private ArrayList<DriRestr> driRestr;

    public ArrayList<DriRestr> getDriRestr() { return this.driRestr; }

    public void setDriRestr(ArrayList<DriRestr> driRestr) { this.driRestr = driRestr; }

    private PostAddr postAddr;

    public PostAddr getPostAddr() { return this.postAddr; }

    public void setPostAddr(PostAddr postAddr) { this.postAddr = postAddr; }

    private StreetAddr streetAddr;

    public StreetAddr getStreetAddr() { return this.streetAddr; }

    public void setStreetAddr(StreetAddr streetAddr) { this.streetAddr = streetAddr; }

    private String chgOfAddrD;

    public String getChgOfAddrD() { return this.chgOfAddrD; }

    public void setChgOfAddrD(String chgOfAddrD) { this.chgOfAddrD = chgOfAddrD; }

    private LocAuthCd locAuthCd;

    public LocAuthCd getLocAuthCd() { return this.locAuthCd; }

    public void setLocAuthCd(LocAuthCd locAuthCd) { this.locAuthCd = locAuthCd; }

    private RegAuthCd regAuthCd;

    public RegAuthCd getRegAuthCd() { return this.regAuthCd; }

    public void setRegAuthCd(RegAuthCd regAuthCd) { this.regAuthCd = regAuthCd; }

    private int dceeaddr;

    public int getDceeaddr() { return this.dceeaddr; }

    public void setDceeaddr(int dceeaddr) { this.dceeaddr = dceeaddr; }
}
