
package co.za.rtmc.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PrDP {

    @SerializedName("PrDPCatD")
    @Expose
    private PrDPCatD prDPCatD;
    @SerializedName("PrDPCatG")
    @Expose
    private PrDPCatG prDPCatG;
    @SerializedName("PrDPExpiryD")
    @Expose
    private String prDPExpiryD;
    @SerializedName("PrDPAuthD")
    @Expose
    private String prDPAuthD;

    /**
     * 
     * @return
     *     The prDPCatD
     */
    public PrDPCatD getPrDPCatD() {
        return prDPCatD;
    }

    /**
     * 
     * @param prDPCatD
     *     The PrDPCatD
     */
    public void setPrDPCatD(PrDPCatD prDPCatD) {
        this.prDPCatD = prDPCatD;
    }

    /**
     * 
     * @return
     *     The prDPCatG
     */
    public PrDPCatG getPrDPCatG() {
        return prDPCatG;
    }

    /**
     * 
     * @param prDPCatG
     *     The PrDPCatG
     */
    public void setPrDPCatG(PrDPCatG prDPCatG) {
        this.prDPCatG = prDPCatG;
    }

    /**
     * 
     * @return
     *     The prDPExpiryD
     */
    public String getPrDPExpiryD() {
        return prDPExpiryD;
    }

    /**
     * 
     * @param prDPExpiryD
     *     The PrDPExpiryD
     */
    public void setPrDPExpiryD(String prDPExpiryD) {
        this.prDPExpiryD = prDPExpiryD;
    }

    /**
     * 
     * @return
     *     The prDPAuthD
     */
    public String getPrDPAuthD() {
        return prDPAuthD;
    }

    /**
     * 
     * @param prDPAuthD
     *     The PrDPAuthD
     */
    public void setPrDPAuthD(String prDPAuthD) {
        this.prDPAuthD = prDPAuthD;
    }

}
