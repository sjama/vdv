
package co.za.rtmc.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import co.za.rtmc.dto.per.LocAuthCd;
import co.za.rtmc.dto.per.NatOfPer;
import co.za.rtmc.dto.per.NatPopGrp;
import co.za.rtmc.dto.per.PostAddr;
import co.za.rtmc.dto.per.RegAuthCd;
import co.za.rtmc.dto.per.StreetAddr;

public class PerDet {

    @SerializedName("PerId")
    @Expose
    private co.za.rtmc.dto.per.PerId perId;
    @SerializedName("PerIdN")
    @Expose
    private String perIdN;
    @SerializedName("NatOfPer")
    @Expose
    private co.za.rtmc.dto.per.NatOfPer natOfPer;
    @SerializedName("NatPopGrp")
    @Expose
    private co.za.rtmc.dto.per.NatPopGrp natPopGrp;
    @SerializedName("BirthDate")
    @Expose
    private String birthDate;
    @SerializedName("Age")
    @Expose
    private String age;
    @SerializedName("PostAddr")
    @Expose
    private co.za.rtmc.dto.per.PostAddr postAddr;
    @SerializedName("StreetAddr")
    @Expose
    private co.za.rtmc.dto.per.StreetAddr streetAddr;
    @SerializedName("DCEEAddr")
    @Expose
    private String dCEEAddr;
    @SerializedName("ChgOfAddrD")
    @Expose
    private String chgOfAddrD;
    @SerializedName("LocAuthCd")
    @Expose
    private co.za.rtmc.dto.per.LocAuthCd locAuthCd;
    @SerializedName("RegAuthCd")
    @Expose
    private co.za.rtmc.dto.per.RegAuthCd regAuthCd;

    /**
     * 
     * @return
     *     The perId
     */
    public co.za.rtmc.dto.per.PerId getPerId() {
        return perId;
    }

    /**
     * 
     * @param perId
     *     The PerId
     */
    public void setPerId(co.za.rtmc.dto.per.PerId perId) {
        this.perId = perId;
    }

    /**
     * 
     * @return
     *     The perIdN
     */
    public String getPerIdN() {
        return perIdN;
    }

    /**
     * 
     * @param perIdN
     *     The PerIdN
     */
    public void setPerIdN(String perIdN) {
        this.perIdN = perIdN;
    }

    /**
     * 
     * @return
     *     The natOfPer
     */
    public co.za.rtmc.dto.per.NatOfPer getNatOfPer() {
        return natOfPer;
    }

    /**
     * 
     * @param natOfPer
     *     The NatOfPer
     */
    public void setNatOfPer(NatOfPer natOfPer) {
        this.natOfPer = natOfPer;
    }

    /**
     * 
     * @return
     *     The natPopGrp
     */
    public co.za.rtmc.dto.per.NatPopGrp getNatPopGrp() {
        return natPopGrp;
    }

    /**
     * 
     * @param natPopGrp
     *     The NatPopGrp
     */
    public void setNatPopGrp(NatPopGrp natPopGrp) {
        this.natPopGrp = natPopGrp;
    }

    /**
     * 
     * @return
     *     The birthDate
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * 
     * @param birthDate
     *     The BirthDate
     */
    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    /**
     * 
     * @return
     *     The age
     */
    public String getAge() {
        return age;
    }

    /**
     * 
     * @param age
     *     The Age
     */
    public void setAge(String age) {
        this.age = age;
    }

    /**
     * 
     * @return
     *     The postAddr
     */
    public co.za.rtmc.dto.per.PostAddr getPostAddr() {
        return postAddr;
    }

    /**
     * 
     * @param postAddr
     *     The PostAddr
     */
    public void setPostAddr(PostAddr postAddr) {
        this.postAddr = postAddr;
    }

    /**
     * 
     * @return
     *     The streetAddr
     */
    public co.za.rtmc.dto.per.StreetAddr getStreetAddr() {
        return streetAddr;
    }

    /**
     * 
     * @param streetAddr
     *     The StreetAddr
     */
    public void setStreetAddr(StreetAddr streetAddr) {
        this.streetAddr = streetAddr;
    }

    /**
     * 
     * @return
     *     The dCEEAddr
     */
    public String getDCEEAddr() {
        return dCEEAddr;
    }

    /**
     * 
     * @param dCEEAddr
     *     The DCEEAddr
     */
    public void setDCEEAddr(String dCEEAddr) {
        this.dCEEAddr = dCEEAddr;
    }

    /**
     * 
     * @return
     *     The chgOfAddrD
     */
    public String getChgOfAddrD() {
        return chgOfAddrD;
    }

    /**
     * 
     * @param chgOfAddrD
     *     The ChgOfAddrD
     */
    public void setChgOfAddrD(String chgOfAddrD) {
        this.chgOfAddrD = chgOfAddrD;
    }

    /**
     * 
     * @return
     *     The locAuthCd
     */
    public co.za.rtmc.dto.per.LocAuthCd getLocAuthCd() {
        return locAuthCd;
    }

    /**
     * 
     * @param locAuthCd
     *     The LocAuthCd
     */
    public void setLocAuthCd(LocAuthCd locAuthCd) {
        this.locAuthCd = locAuthCd;
    }

    /**
     * 
     * @return
     *     The regAuthCd
     */
    public co.za.rtmc.dto.per.RegAuthCd getRegAuthCd() {
        return regAuthCd;
    }

    /**
     * 
     * @param regAuthCd
     *     The RegAuthCd
     */
    public void setRegAuthCd(RegAuthCd regAuthCd) {
        this.regAuthCd = regAuthCd;
    }

}
