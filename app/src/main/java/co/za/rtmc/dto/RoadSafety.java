package co.za.rtmc.dto;

import java.io.Serializable;
import java.util.ArrayList;

import co.za.rtmc.vo.BoRoadSafetyInspection;

public class RoadSafety  implements Serializable {
	private ArrayList<BoRoadSafetyInspection> roadSafetyInspections;

	public ArrayList<BoRoadSafetyInspection> getRoadSafetyInspections() {
		return roadSafetyInspections;
	}

	public void setRoadSafetyInspections(ArrayList<BoRoadSafetyInspection> roadSafetyInspections) {
		this.roadSafetyInspections = roadSafetyInspections;
	}
}
