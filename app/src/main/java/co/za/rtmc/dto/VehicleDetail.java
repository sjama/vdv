package co.za.rtmc.dto;

import java.io.Serializable;

public class VehicleDetail implements Serializable
{
    private String unitN;
    private String vinOrChassis;
    private String nModelN;
    private String engineN;
    private String mvStateCd;
    private String mvStateChgD;
    private String mainColourCd;
    private String nOfWheels;
    private String tare;
    private String gearboxN;
    private String transmissionCd;
    private String differentialN;
    private String mvRegtTypeQualCd;
    private String mvRegtTypeQualChgD;
    private String cntryOfExpCd;
    private String cntryOfImpCd;
    private String fuelTypeCd;
    private String capSit;
    private String capStand;
    private String gvm;
    private String gcm;
    private String netPower;
    private String overallLen;
    private String overallHeight;
    private String overallWidth;
    private String axTot;
    private String axDri;
    private String engineDisp;
    private String mvRegN;
    private String mvCertN;

    public String getUnitN()
    {
        return unitN;
    }

    public void setUnitN(String unitN)
    {
        this.unitN = unitN;
    }

    public String getVinOrChassis()
    {
        return vinOrChassis;
    }

    public void setVinOrChassis(String vinOrChassis)
    {
        this.vinOrChassis = vinOrChassis;
    }

    public String getNModelN()
    {
        return nModelN;
    }

    public void setNModelN(String nModelN)
    {
        this.nModelN = nModelN;
    }

    public String getEngineN()
    {
        return engineN;
    }

    public void setEngineN(String engineN)
    {
        this.engineN = engineN;
    }

    public String getMvStateCd()
    {
        return mvStateCd;
    }

    public void setMvStateCd(String mvStateCd)
    {
        this.mvStateCd = mvStateCd;
    }

    public String getMvStateChgD()
    {
        return mvStateChgD;
    }

    public void setMvStateChgD(String mvStateChgD)
    {
        this.mvStateChgD = mvStateChgD;
    }

    public String getMainColourCd()
    {
        return mainColourCd;
    }

    public void setMainColourCd(String mainColourCd)
    {
        this.mainColourCd = mainColourCd;
    }

    public String getNOfWheels()
    {
        return nOfWheels;
    }

    public void setNOfWheels(String nOfWheels)
    {
        this.nOfWheels = nOfWheels;
    }

    public String getTare()
    {
        return tare;
    }

    public void setTare(String tare)
    {
        this.tare = tare;
    }

    public String getGearboxN()
    {
        return gearboxN;
    }

    public void setGearboxN(String gearboxN)
    {
        this.gearboxN = gearboxN;
    }

    public String getAxDri()
    {
        return axDri;
    }

    public String getAxTot()
    {
        return axTot;
    }

    public String getCapSit()
    {
        return capSit;
    }

    public String getCapStand()
    {
        return capStand;
    }

    public String getCntryOfExpCd()
    {
        return cntryOfExpCd;
    }

    public String getCntryOfImpCd()
    {
        return cntryOfImpCd;
    }

    public String getDifferentialN()
    {
        return differentialN;
    }

    public String getEngineDisp()
    {
        return engineDisp;
    }

    public String getFuelTypeCd()
    {
        return fuelTypeCd;
    }

    public String getGcm()
    {
        return gcm;
    }

    public String getGvm()
    {
        return gvm;
    }

    public String getMvRegtTypeQualCd()
    {
        return mvRegtTypeQualCd;
    }

    public String getMvRegtTypeQualChgD()
    {
        return mvRegtTypeQualChgD;
    }

    public String getNetPower()
    {
        return netPower;
    }

    public String getOverallHeight()
    {
        return overallHeight;
    }

    public String getOverallLen()
    {
        return overallLen;
    }

    public String getOverallWidth()
    {
        return overallWidth;
    }

    public String getTransmissionCd()
    {
        return transmissionCd;
    }

    public void setAxDri(String axDri)
    {
        this.axDri = axDri;
    }

    public void setAxTot(String axTot)
    {
        this.axTot = axTot;
    }

    public void setCapSit(String capSit)
    {
        this.capSit = capSit;
    }

    public void setCapStand(String capStand)
    {
        this.capStand = capStand;
    }

    public void setCntryOfExpCd(String cntryOfExpCd)
    {
        this.cntryOfExpCd = cntryOfExpCd;
    }

    public void setCntryOfImpCd(String cntryOfImpCd)
    {
        this.cntryOfImpCd = cntryOfImpCd;
    }

    public void setDifferentialN(String differentialN)
    {
        this.differentialN = differentialN;
    }

    public void setEngineDisp(String engineDisp)
    {
        this.engineDisp = engineDisp;
    }

    public void setFuelTypeCd(String fuelTypeCd)
    {
        this.fuelTypeCd = fuelTypeCd;
    }

    public void setGcm(String gcm)
    {
        this.gcm = gcm;
    }

    public void setGvm(String gvm)
    {
        this.gvm = gvm;
    }

    public void setMvRegtTypeQualCd(String mvRegtTypeQualCd)
    {
        this.mvRegtTypeQualCd = mvRegtTypeQualCd;
    }

    public void setMvRegtTypeQualChgD(String mvRegtTypeQualChgD)
    {
        this.mvRegtTypeQualChgD = mvRegtTypeQualChgD;
    }

    public void setNetPower(String netPower)
    {
        this.netPower = netPower;
    }

    public void setOverallHeight(String overallHeight)
    {
        this.overallHeight = overallHeight;
    }

    public void setOverallLen(String overallLen)
    {
        this.overallLen = overallLen;
    }

    public void setOverallWidth(String overallWidth)
    {
        this.overallWidth = overallWidth;
    }

    public void setTransmissionCd(String transmissionCd)
    {
        this.transmissionCd = transmissionCd;
    }

    public String getMvCertN()
    {
        return mvCertN;
    }

    public String getMvRegN()
    {
        return mvRegN;
    }

    public void setMvCertN(String string)
    {
        mvCertN = string;
    }

    public void setMvRegN(String string)
    {
        mvRegN = string;
    }
}



