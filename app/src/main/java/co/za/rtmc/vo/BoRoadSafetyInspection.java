package co.za.rtmc.vo;

/**
 * Created by jamasithole on 2017/12/07.
 */

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author JamaS
 */
public class BoRoadSafetyInspection implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    private BigDecimal rfn;
    private String mvRegn;
    private String make;
    private String model;
    private String color;
    private String licDiscExpd;
    private String mvRwStat;
    private String sapMark;
    private String driverName;
    private String driverSurname;
    private String gpsYCoordinates;
    private String gpsXCoordinates;
    private String locationName;
    private String dateTime;
    private String actionStatus;
    private Lmidtypecd idTypeCd;
    private String idNumber;

    public String getInfrastructureNumber() {
        return infrastructureNumber;
    }

    public void setInfrastructureNumber(String infrastructureNumber) {
        this.infrastructureNumber = infrastructureNumber;
    }

    private String infrastructureNumber;


    public Date getCaptureD() {
        return captureD;
    }

    public void setCaptureD(Date captureD) {
        this.captureD = captureD;
    }

    private Date captureD;

    public String getAddressData() {
        return addressData;
    }

    public void setAddressData(String addressData) {
        this.addressData = addressData;
    }

    private String addressData;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    private String action;

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public BoRoadSafetyInspection() {
    }

    public BoRoadSafetyInspection(BigDecimal rfn) {
        this.rfn = rfn;
    }

    public BigDecimal getRfn() {
        return rfn;
    }

    public void setRfn(BigDecimal rfn) {
        this.rfn = rfn;
    }

    public String getMvRegn() {
        return mvRegn;
    }

    public void setMvRegn(String mvRegn) {
        this.mvRegn = mvRegn;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLicDiscExpd() {
        return licDiscExpd;
    }

    public void setLicDiscExpd(String licDiscExpd) {
        this.licDiscExpd = licDiscExpd;
    }

    public String getMvRwStat() {
        return mvRwStat;
    }

    public void setMvRwStat(String mvRwStat) {
        this.mvRwStat = mvRwStat;
    }

    public String getSapMark() {
        return sapMark;
    }

    public void setSapMark(String sapMark) {
        this.sapMark = sapMark;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverSurname() {
        return driverSurname;
    }

    public void setDriverSurname(String driverSurname) {
        this.driverSurname = driverSurname;
    }

    public String getGpsYCoordinates() {
        return gpsYCoordinates;
    }

    public void setGpsYCoordinates(String gpsYCoordinates) {
        this.gpsYCoordinates = gpsYCoordinates;
    }

    public String getGpsXCoordinates() {
        return gpsXCoordinates;
    }

    public void setGpsXCoordinates(String gpsXCoordinates) {
        this.gpsXCoordinates = gpsXCoordinates;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getActionStatus() {
        return actionStatus;
    }

    public void setActionStatus(String actionStatus) {
        this.actionStatus = actionStatus;
    }

    public Lmidtypecd getIdTypeCd() {
        return idTypeCd;
    }

    public void setIdTypeCd(Lmidtypecd idTypeCd) {
        this.idTypeCd = idTypeCd;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rfn != null ? rfn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BoRoadSafetyInspection)) {
            return false;
        }
        BoRoadSafetyInspection other = (BoRoadSafetyInspection) object;
        if ((this.rfn == null && other.rfn != null) || (this.rfn != null && !this.rfn.equals(other.rfn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.za.rtmc.backoffice.persist.entity.BoRoadSafetyInspection[ rfn=" + rfn + " ]";
    }

}
