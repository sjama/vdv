package co.za.rtmc.vo;

/**
 * Created by jamasithole on 2019/06/06.
 */
public class ChargeDetails {

    private Ltcnt ltcnt;
    private Ltcntfees ltcntfees;

    public ChargeDetails() {
    }

    public Ltcnt getLtcnt() {
        return ltcnt;
    }

    public void setLtcnt(Ltcnt ltcnt) {
        this.ltcnt = ltcnt;
    }

    public Ltcntfees getLtcntfees() {
        return ltcntfees;
    }

    public void setLtcntfees(Ltcntfees ltcntfees) {
        this.ltcntfees = ltcntfees;
    }

}

