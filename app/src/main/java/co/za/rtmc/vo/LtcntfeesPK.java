package co.za.rtmc.vo;

import java.util.Date;

/**
 * Created by jamasithole on 2019/06/06.
 */

public class LtcntfeesPK {

    private String cntcd;
    private String cnteffd;


    public LtcntfeesPK() {
    }

    public String getCntcd() {
        return cntcd;
    }

    public void setCntcd(String cntcd) {
        this.cntcd = cntcd;
    }

    public String getCnteffd() {
        return cnteffd;
    }

    public void setCnteffd(String cnteffd) {
        this.cnteffd = cnteffd;
    }
}
