package co.za.rtmc.vo;

import co.za.rtmc.dto.per.LocAuthCd;
import co.za.rtmc.dto.per.NatOfPer;
import co.za.rtmc.dto.per.NatPopGrp;
import co.za.rtmc.dto.per.PerId;
import co.za.rtmc.dto.per.PostAddr;
import co.za.rtmc.dto.per.RegAuthCd;
import co.za.rtmc.dto.per.StreetAddr;

public class PerDet {
    private NatOfPer NatOfPer;

    private RegAuthCd RegAuthCd;

    private NatPopGrp NatPopGrp;

    private String Age;

    private String DCEEAddr;

    private String ChgOfAddrD;

    private String PerIdN;

    private PostAddr PostAddr;

    private PerId PerId;

    private String BirthDate;

    private LocAuthCd LocAuthCd;

    private StreetAddr StreetAddr;

    public NatOfPer getNatOfPer ()
    {
        return NatOfPer;
    }

    public void setNatOfPer (NatOfPer NatOfPer)
    {
        this.NatOfPer = NatOfPer;
    }

    public RegAuthCd getRegAuthCd ()
    {
        return RegAuthCd;
    }

    public void setRegAuthCd (RegAuthCd RegAuthCd)
    {
        this.RegAuthCd = RegAuthCd;
    }

    public NatPopGrp getNatPopGrp ()
    {
        return NatPopGrp;
    }

    public void setNatPopGrp (NatPopGrp NatPopGrp)
    {
        this.NatPopGrp = NatPopGrp;
    }

    public String getAge ()
    {
        return Age;
    }

    public void setAge (String Age)
    {
        this.Age = Age;
    }

    public String getDCEEAddr ()
    {
        return DCEEAddr;
    }

    public void setDCEEAddr (String DCEEAddr)
    {
        this.DCEEAddr = DCEEAddr;
    }

    public String getChgOfAddrD ()
    {
        return ChgOfAddrD;
    }

    public void setChgOfAddrD (String ChgOfAddrD)
    {
        this.ChgOfAddrD = ChgOfAddrD;
    }

    public String getPerIdN ()
    {
        return PerIdN;
    }

    public void setPerIdN (String PerIdN)
    {
        this.PerIdN = PerIdN;
    }

    public PostAddr getPostAddr ()
    {
        return PostAddr;
    }

    public void setPostAddr (PostAddr PostAddr)
    {
        this.PostAddr = PostAddr;
    }

    public PerId getPerId ()
    {
        return PerId;
    }

    public void setPerId (PerId PerId)
    {
        this.PerId = PerId;
    }

    public String getBirthDate ()
    {
        return BirthDate;
    }

    public void setBirthDate (String BirthDate)
    {
        this.BirthDate = BirthDate;
    }

    public LocAuthCd getLocAuthCd ()
    {
        return LocAuthCd;
    }

    public void setLocAuthCd (LocAuthCd LocAuthCd)
    {
        this.LocAuthCd = LocAuthCd;
    }

    public StreetAddr getStreetAddr ()
    {
        return StreetAddr;
    }

    public void setStreetAddr (StreetAddr StreetAddr)
    {
        this.StreetAddr = StreetAddr;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [NatOfPer = "+NatOfPer+", RegAuthCd = "+RegAuthCd+", NatPopGrp = "+NatPopGrp+", Age = "+Age+", DCEEAddr = "+DCEEAddr+", ChgOfAddrD = "+ChgOfAddrD+", PerIdN = "+PerIdN+", PostAddr = "+PostAddr+", PerId = "+PerId+", BirthDate = "+BirthDate+", LocAuthCd = "+LocAuthCd+", StreetAddr = "+StreetAddr+"]";
    }
}
