package co.za.rtmc.vo;

import java.io.Serializable;

import co.za.rtmc.dto.per.PostalCd2;

/**
 * Created by Jama on 2016/05/12.
 */
public class MvLicDiscVo  implements Serializable{
    private static final long serialVersionUID = 1L;
    private String certN  = "";
    private String userNumber  = "";
    private String docCtrlN  = "";
    private String mvRegN  = "";
    private String regtN  = "";
    private String mvDescCdDesc  = "";
    private String make  = "";
    private String model  = "";
    private String color  = "";
    private String vin  = "";
    private String engineNo  = "";
    private String Expiry  = "";

    private String desc;

    public String getDesc() { return this.desc; }

    public void setDesc(String desc) { this.desc = desc; }

    private String idDocN;
    private String initials;

    public String getIdDocN() {
        return idDocN;
    }

    public void setIdDocN(String idDocN) {
        this.idDocN = idDocN;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getBusOrSurname() {
        return busOrSurname;
    }

    public void setBusOrSurname(String busOrSurname) {
        this.busOrSurname = busOrSurname;
    }

    private String busOrSurname;



    public String getMvState() {
        return mvState;
    }

    public void setMvState(String mvState) {
        this.mvState = mvState;
    }

    private String mvState  = "";


    public boolean isScanned() {
        return isScanned;
    }

    public void setScanned(boolean scanned) {
        isScanned = scanned;
    }

    private boolean isScanned =false;

    public String getRwStatus() {
        return rwStatus;
    }

    public void setRwStatus(String rwStatus) {
        this.rwStatus = rwStatus;
    }

    public String getSapMark() {
        return sapMark;
    }

    public void setSapMark(String sapMark) {
        this.sapMark = sapMark;
    }

    private String rwStatus  = "";
    private String sapMark  = "";
    private boolean enatisNoData;
    private boolean enatisNoMatch;

    private String addr1;

    public String getAddr1() { return this.addr1; }

    public void setAddr1(String addr1) { this.addr1 = addr1; }

    private String addr2;

    public String getAddr2() { return this.addr2; }

    public void setAddr2(String addr2) { this.addr2 = addr2; }

    private String addr3;

    public String getAddr3() { return this.addr3; }

    public void setAddr3(String addr3) { this.addr3 = addr3; }

    private String postalCd;

    public String getPostalCd() { return this.postalCd; }

    public void setPostalCd(String postalCd) { this.postalCd = postalCd; }
    public String getCertN() {
        return certN;
    }

    public void setCertN(String certN) {
        this.certN = certN;
    }

    public String getUsernumber() {
        return userNumber;
    }

    public void setUsernumber(String usernumber) {
        this.userNumber = usernumber;
    }

    public String getDocCtrlN() {
        return docCtrlN;
    }

    public void setDocCtrlN(String docCtrlN) {
        this.docCtrlN = docCtrlN;
    }

    public String getMvRegN() {
        return mvRegN;
    }

    public void setMvRegN(String mvRegN) {
        this.mvRegN = mvRegN;
    }

    public String getRegtN() {
        return regtN;
    }

    public void setRegtN(String regtN) {
        this.regtN = regtN;
    }

    public String getMvDescCdDesc() {
        return mvDescCdDesc;
    }

    public void setMvDescCdDesc(String mvDescCdDesc) {
        this.mvDescCdDesc = mvDescCdDesc;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getExpiry() {
        return Expiry;
    }

    public void setExpiry(String expiry) {
        Expiry = expiry;
    }
    public boolean isEnatisNoData() {
        return enatisNoData;
    }

    public void setEnatisNoData(boolean enatisNoData) {
        this.enatisNoData = enatisNoData;
    }

    public boolean isEnatisNoMatch() {
        return enatisNoMatch;
    }

    public void setEnatisNoMatch(boolean enatisNoMatch) {
        this.enatisNoMatch = enatisNoMatch;
    }


}
