package co.za.rtmc.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by jamasithole on 2019/06/06.
 */

public class Ltcntfees implements Serializable {


    private LtcntfeesPK id;

    private long cntpenaltyamt;
    private long cntdiscountamt;
    private String cntpenalty;
    private long cntdemer;
    private String cnttype;
    private String cnteffdto;
    private String opercntcd;
    private int speedminvalue;
    private int speedmaxvalue;
    private int speedlimit;

    public Ltcntfees() {
    }

    public LtcntfeesPK getId() {
        return id;
    }

    public void setId(LtcntfeesPK id) {
        this.id = id;
    }




    public long getCntpenaltyamt() {
        return cntpenaltyamt;
    }

    public void setCntpenaltyamt(long cntpenaltyamt) {
        this.cntpenaltyamt = cntpenaltyamt;
    }

    public long getCntdiscountamt() {
        return cntdiscountamt;
    }

    public void setCntdiscountamt(long cntdiscountamt) {
        this.cntdiscountamt = cntdiscountamt;
    }

    public String getCntpenalty() {
        return cntpenalty;
    }

    public void setCntpenalty(String cntpenalty) {
        this.cntpenalty = cntpenalty;
    }

    public long getCntdemer() {
        return cntdemer;
    }

    public void setCntdemer(long cntdemer) {
        this.cntdemer = cntdemer;
    }

    public String getCnttype() {
        return cnttype;
    }

    public void setCnttype(String cnttype) {
        this.cnttype = cnttype;
    }

    public String getCnteffdto() {
        return cnteffdto;
    }

    public void setCnteffdto(String cnteffdto) {
        this.cnteffdto = cnteffdto;
    }

    public String getOpercntcd() {
        return opercntcd;
    }

    public void setOpercntcd(String opercntcd) {
        this.opercntcd = opercntcd;
    }

    public int getSpeedminvalue() {
        return speedminvalue;
    }

    public void setSpeedminvalue(int speedminvalue) {
        this.speedminvalue = speedminvalue;
    }

    public int getSpeedmaxvalue() {
        return speedmaxvalue;
    }

    public void setSpeedmaxvalue(int speedmaxvalue) {
        this.speedmaxvalue = speedmaxvalue;
    }

    public int getSpeedlimit() {
        return speedlimit;
    }

    public void setSpeedlimit(Byte speedlimit) {
        this.speedlimit = speedlimit;
    }


}

