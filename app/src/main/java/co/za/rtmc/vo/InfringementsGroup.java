package co.za.rtmc.vo;

/**
 * Created by jamasithole on 2018/02/03.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

public class InfringementsGroup {
    /**
     * @return the courtStatusList
     */
    public List<Infringement> getCourtStatusList() {
        return courtStatusList;
    }

    /**
     * @param courtStatusList the courtStatusList to set
     */
    public void setCourtStatusList(List<Infringement> courtStatusList) {
        this.courtStatusList = courtStatusList;
    }

    /**
     * @return the enforcementStatusList
     */
    public List<Infringement> getEnforcementStatusList() {
        return enforcementStatusList;
    }

    /**
     * @param enforcementStatusList the enforcementStatusList to set
     */
    public void setEnforcementStatusList(List<Infringement> enforcementStatusList) {
        this.enforcementStatusList = enforcementStatusList;
    }

    /**
     * @return the warrantStatusList
     */
    public List<Infringement> getWarrantStatusList() {
        return warrantStatusList;
    }

    /**
     * @param warrantStatusList the warrantStatusList to set
     */
    public void setWarrantStatusList(List<Infringement> warrantStatusList) {
        this.warrantStatusList = warrantStatusList;
    }

    @SerializedName("infringements")
    @Expose
    private List<Infringement> infringements = new ArrayList<>();


    private List<Infringement> courtStatusList = new ArrayList<>();
    private List<Infringement> enforcementStatusList = new ArrayList<>();
    private List<Infringement> warrantStatusList = new ArrayList<>();

    public List<Infringement> getInfringements() {
        return infringements;
    }

    public void setInfringements(List<Infringement> infringements) {
        this.infringements = infringements;
    }
}
