package co.za.rtmc.vo;

import java.io.Serializable;

/**
 * Created by jamasithole on 2017/12/07.
 */

public class Lmidtypecd   implements Serializable {
    private static final long serialVersionUID = 1L;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    private  String code;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private  String description;
}
