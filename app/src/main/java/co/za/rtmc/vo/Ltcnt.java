package co.za.rtmc.vo;

/**
 * Created by jamasithole on 2019/06/05.
 */

public class Ltcnt {

    private String cntcd;
    private String cntshort;
    private String cntshortsec;
    private String cntlong;
    private String cntlongsec;
    private String cntindex;
    private String cntpenalty;
    private String cntopercharge;
    private String iddriverind;

    public Ltcnt() {
    }

    public String getCntcd() {
        return cntcd;
    }

    public void setCntcd(String cntcd) {
        this.cntcd = cntcd;
    }

    public String getCntshort() {
        return cntshort;
    }

    public void setCntshort(String cntshort) {
        this.cntshort = cntshort;
    }

    public String getCntshortsec() {
        return cntshortsec;
    }

    public void setCntshortsec(String cntshortsec) {
        this.cntshortsec = cntshortsec;
    }

    public String getCntlong() {
        return cntlong;
    }

    public void setCntlong(String cntlong) {
        this.cntlong = cntlong;
    }

    public String getCntlongsec() {
        return cntlongsec;
    }

    public void setCntlongsec(String cntlongsec) {
        this.cntlongsec = cntlongsec;
    }

    public String getCntindex() {
        return cntindex;
    }

    public void setCntindex(String cntindex) {
        this.cntindex = cntindex;
    }

    public String getCntpenalty() {
        return cntpenalty;
    }

    public void setCntpenalty(String cntpenalty) {
        this.cntpenalty = cntpenalty;
    }

    public String getCntopercharge() {
        return cntopercharge;
    }

    public void setCntopercharge(String cntopercharge) {
        this.cntopercharge = cntopercharge;
    }

    public String getIddriverind() {
        return iddriverind;
    }

    public void setIddriverind(String iddriverind) {
        this.iddriverind = iddriverind;
    }
}
