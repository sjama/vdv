package co.za.rtmc.vo;


import java.io.Serializable;

/**
 *
 * @author RTMC
 */

public class RdtstrptresRwsVo implements Serializable{

    private static final long serialVersionUID = 1L;
    private String lmoptioncd;
    private String resultscd;
    private String rdtstrpt;

    public RdtstrptresRwsVo() {
    }

    public RdtstrptresRwsVo( String lmmvtypeoptionscd,
                             String resultscd,
                             String rdtstrpt ) {
        this.lmoptioncd = lmmvtypeoptionscd;
        this.resultscd = resultscd;
        this.rdtstrpt = rdtstrpt;
    }

    public String getLmoptioncd() {
        return lmoptioncd;
    }

    public void setLmoptioncd(String lmoptioncd) {
        this.lmoptioncd = lmoptioncd;
    }

    public String getResultscd() {
        return resultscd;
    }

    public void setResultscd( String resultscd ) {
        this.resultscd = resultscd;
    }

    public String getRdtstrpt() {
        return rdtstrpt;
    }

    public void setRdtstrpt( String rdtstrpt ) {
        this.rdtstrpt = rdtstrpt;
    }

    @Override
    public String toString() {
        return new StringBuffer( "lmoptioncd : " ).append(
                this.lmoptioncd)
                .append( " resultscd : " ).append( this.resultscd )
                .append( " rdtstrpt : " ).append( this.rdtstrpt )
                .toString();
    }

}
