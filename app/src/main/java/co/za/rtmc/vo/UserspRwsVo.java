/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.rtmc.vo;

import java.io.Serializable;

/**
 *
 * @author Ngwane
 */

public class UserspRwsVo implements Serializable{

    public String username;
    private String idn;
    private String firstname;
    private String middlename;
    private String surname;
    private String infn;
    private String dtoid;
    private String gendercd;
    private String idtypecd;
    private String poscd;
    private String emailAddress;

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public UserspRwsVo() {
    }

    public UserspRwsVo( String username,
    String idn,
    String firstname,
    String middlename,
    String surname,
    String infn,
    String dtoid,
    String gendercd,
    String idtypecd,
    String poscd ) {
        this.username = username;
        this.idn = idn;
        this.firstname = firstname;
        this.middlename = middlename;
        this.surname = surname;
        this.infn = infn;
        this.dtoid = dtoid;
        this.gendercd = gendercd;
        this.idtypecd = idtypecd;
        this.poscd = poscd;
    }

    public String getUsername() {
        return username;
    }

    
    public void setUsername( String username ) {
        this.username = username;
    }

    public String getIdn() {
        return idn;
    }

    
    public void setIdn( String idn ) {
        this.idn = idn;
    }

    public String getFirstname() {
        return firstname;
    }

    
    public void setFirstname( String firstname ) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    
    public void setMiddlename( String middlename ) {
        this.middlename = middlename;
    }

    public String getSurname() {
        return surname;
    }

    
    public void setSurname( String surname ) {
        this.surname = surname;
    }

    public String getInfn() {
        return infn;
    }

    
    public void setInfn( String infn ) {
        this.infn = infn;
    }

    public String getDtoid() {
        return dtoid;
    }

    
    public void setDtoid( String dtoid ) {
        this.dtoid = dtoid;
    }

    public String getGendercd() {
        return gendercd;
    }

    
    public void setGendercd( String gendercd ) {
        this.gendercd = gendercd;
    }

    public String getIdtypecd() {
        return idtypecd;
    }

    
    public void setIdtypecd( String idtypecd ) {
        this.idtypecd = idtypecd;
    }

    public String getPoscd() {
        return poscd;
    }

    
    public void setPoscd( String poscd ) {
        this.poscd = poscd;
    }

    @Override
    public String toString() {
        return new StringBuffer( "username : " ).append( this.username )
        .append( " idn : " ).append( this.idn )
        .append( " firstname : " ).append( this.firstname )
        .append( " middlename : " ).append( this.middlename )
        .append( " surname : " ).append( this.surname )
        .append( " infn : " ).append( this.infn )
        .append( " dtoid : " ).append( this.dtoid )
        .append( " gendercd : " ).append( this.gendercd )
        .append( " idtypecd : " ).append( this.idtypecd )
        .append( " poscd : " ).append( this.poscd )
        .toString();
    }

}
