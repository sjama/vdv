/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.rtmc.vo;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author RTMC
 */
public class RdtstrptresVoRwsVo  implements Serializable {

    private static final long serialVersionUID = 1L;

    private RdtstrptRwsVo rdtstrptRwsVo;
    private List<RdtstrptresRwsVo> rdtstrptresRwsVo = new ArrayList<RdtstrptresRwsVo>();
    public RdtstrptresVoRwsVo() {
    }

    public RdtstrptresVoRwsVo( RdtstrptRwsVo rdtstrptRwsVo,
                               List<RdtstrptresRwsVo> rdtstrptresRwsVo ) {
        this.rdtstrptRwsVo = rdtstrptRwsVo;
        this.rdtstrptresRwsVo = rdtstrptresRwsVo;
    }

    public RdtstrptRwsVo getRdtstrptRwsVo() {
        return rdtstrptRwsVo;
    }

    public void setRdtstrptRwsVo( RdtstrptRwsVo rdtstrptRwsVo ) {
        this.rdtstrptRwsVo = rdtstrptRwsVo;
    }

    public List<RdtstrptresRwsVo> getRdtstrptresRwsVo() {
        return rdtstrptresRwsVo;
    }

    public void setRdtstrptresRwsVo( List<RdtstrptresRwsVo> rdtstrptresRwsVo ) {
        this.rdtstrptresRwsVo = rdtstrptresRwsVo;
    }

    @Override
    public String toString() {
        return new StringBuffer( "rdtstrptresRwsVo : " ).append(
                this.rdtstrptresRwsVo )
                .append( " rdtstrptresRwsVo : " ).append(
                        this.rdtstrptresRwsVo )
                .toString();
    }

}
