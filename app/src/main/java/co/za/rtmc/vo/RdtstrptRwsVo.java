/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.za.rtmc.vo;


import android.util.Base64;

import java.io.Serializable;

/**
 *
 * @author RTMC
 */

public class RdtstrptRwsVo implements Serializable {

    private static final long serialVersionUID = 1L;
    private String rptnum;
    private String submitd;
    private String infn;
    private String liccd;
    private String driname;
    private String drisurname;
    private String driidn;
    private String mvreg;
    private String road;
    private String prosecution;
    private String itemn;
    private String ntpn;
    private String drividtypecd;
    private String drividtypedesc;
    private String mvtypecd;
    private String mvtypedesc;
    private String provcd;
    private String provdesc;
    private String driLicValidToD;
    private String gpsYCoordinates;
    private String gpsXCoordinates;
    private String locationName;
    private String addressData;
    private String gender;

    private String provice;
    private String cityTown;
    private String street;
    private String routeNumber;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProvice() {
        return provice;
    }

    public void setProvice(String provice) {
        this.provice = provice;
    }

    public String getCityTown() {
        return cityTown;
    }

    public void setCityTown(String cityTown) {
        this.cityTown = cityTown;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }

    public String getAddressData() {
        return addressData;
    }

    public void setAddressData(String addressData) {
        this.addressData = addressData;
    }

    public String getGpsYCoordinates() {
        return gpsYCoordinates;
    }

    public void setGpsYCoordinates(String gpsYCoordinates) {
        this.gpsYCoordinates = gpsYCoordinates;
    }

    public String getGpsXCoordinates() {
        return gpsXCoordinates;
    }

    public void setGpsXCoordinates(String gpsXCoordinates) {
        this.gpsXCoordinates = gpsXCoordinates;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getLicCardNo() {
        return licCardNo;
    }

    public void setLicCardNo(String licCardNo) {
        this.licCardNo = licCardNo;
    }

    private String licCardNo;

    public String getDriPrDPValidToD() {
        return driPrDPValidToD;
    }

    public void setDriPrDPValidToD(String driPrDPValidToD) {
        this.driPrDPValidToD = driPrDPValidToD;
    }

    private String driPrDPValidToD;

    public String getDriPrDPs() {
        return driPrDPs;
    }

    public void setDriPrDPs(String driPrDPs) {
        this.driPrDPs = driPrDPs;
    }

    private String driPrDPs;
    private String imageString;

    private String usersp;

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }



    public String getDriLicValidToD() {
        return driLicValidToD;
    }

    public void setDriLicValidToD(String driLicValidToD) {
        this.driLicValidToD = driLicValidToD;
    }


    public RdtstrptRwsVo() {
    }

    public RdtstrptRwsVo( String rptnum,
                          String submitd,
                          String infn,
                          String liccd,
                          String driname,
                          String drisurname,
                          String driidn,
                          String mvreg,
                          String road,
                          String prosecution,
                          String itemn,
                          String ntpn,
                          String drividtypecd,
                          String drividtypedesc,
                          String mvtypecd,
                          String mvtypedesc,
                          String provcd,
                          String provdesc,
                          String username,
                          String driLicValidToD) {
        this.rptnum = rptnum;
        this.submitd = submitd;
        this.infn = infn;
        this.liccd = liccd;
        this.driname = driname;
        this.drisurname = drisurname;
        this.driidn = driidn;
        this.mvreg = mvreg;
        this.road = road;
        this.prosecution = prosecution;
        this.itemn = itemn;
        this.ntpn = ntpn;
        this.drividtypecd = drividtypecd;
        this.drividtypedesc = drividtypedesc;
        this.mvtypecd = mvtypecd;
        this.mvtypedesc = mvtypedesc;
        this.provcd = provcd;
        this.provdesc = provdesc;
        this.usersp = username;
        this.driLicValidToD = driLicValidToD;
    }

    public String getRptnum() {
        return rptnum;
    }


    public void setRptnum( String rptnum ) {
        this.rptnum = rptnum;
    }

    public String getSubmitd() {
        return submitd;
    }


    public void setSubmitd( String submitd ) {
        this.submitd = submitd;
    }

    public String getInfn() {
        return infn;
    }


    public void setInfn( String infn ) {
        this.infn = infn;
    }

    public String getLiccd() {
        return liccd;
    }

    public void setLiccd( String liccd ) {
        this.liccd = liccd;
    }

    public String getDriname() {
        return driname;
    }

    public void setDriname( String driname ) {
        this.driname = driname;
    }

    public String getDrisurname() {
        return drisurname;
    }


    public void setDrisurname( String drisurname ) {
        this.drisurname = drisurname;
    }

    public String getDriidn() {
        return driidn;
    }

    public void setDriidn( String driidn ) {
        this.driidn = driidn;
    }

    public String getMvreg() {
        return mvreg;
    }

    public void setMvreg( String mvreg ) {
        this.mvreg = mvreg;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad( String road ) {
        this.road = road;
    }

    public String getProsecution() {
        return prosecution;
    }


    public void setProsecution( String prosecution ) {
        this.prosecution = prosecution;
    }

    public String getItemn() {
        return itemn;
    }


    public void setItemn( String itemn ) {
        this.itemn = itemn;
    }

    public String getNtpn() {
        return ntpn;
    }


    public void setNtpn( String ntpn ) {
        this.ntpn = ntpn;
    }

    public String getDrividtypecd() {
        return drividtypecd;
    }


    public void setDrividtypecd( String drividtypecd ) {
        this.drividtypecd = drividtypecd;
    }

    public String getDrividtypedesc() {
        return drividtypedesc;
    }


    public void setDrividtypedesc( String drividtypedesc ) {
        this.drividtypedesc = drividtypedesc;
    }

    public String getMvtypecd() {
        return mvtypecd;
    }


    public void setMvtypecd( String mvtypecd ) {
        this.mvtypecd = mvtypecd;
    }

    public String getMvtypedesc() {
        return mvtypedesc;
    }


    public void setMvtypedesc( String mvtypedesc ) {
        this.mvtypedesc = mvtypedesc;
    }

    public String getProvcd() {
        return provcd;
    }


    public void setProvcd( String provcd ) {
        this.provcd = provcd;
    }

    public String getProvdesc() {
        return provdesc;
    }


    public void setProvdesc( String provdesc ) {
        this.provdesc = provdesc;
    }

    public String getUsername() {
        return usersp;
    }


    public void setUsername( String username ) {
        this.usersp = username;
    }

    @Override
    public String toString() {
        return new StringBuffer( "rptnum : " ).append( this.rptnum )
                .append( " submitd : " ).append( this.submitd )
                .append( " infn : " ).append( this.infn )
                .append( " liccd : " ).append( this.liccd )
                .append( " driname : " ).append( this.driname )
                .append( " drisurname : " ).append( this.drisurname )
                .append( " driidn : " ).append( this.driidn )
                .append( " mvreg : " ).append( this.mvreg )
                .append( " road : " ).append( this.road )
                .append( " prosecution : " ).append( this.prosecution )
                .append( " itemn : " ).append( this.itemn )
                .append( " ntpn : " ).append( this.ntpn )
                .append( " drividtypecd : " ).append( this.drividtypecd )
                .append( " drividtypedesc : " ).append( this.drividtypedesc )
                .append( " mvtypecd : " ).append( this.mvtypecd )
                .append( " mvtypedesc : " ).append( this.mvtypedesc )
                .append( " provcd : " ).append( this.provcd )
                .append( " provdesc : " ).append( this.provdesc )
                .append( " username : " ).append( this.usersp )
                .toString();
    }

}
