package co.za.rtmc.vo;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by jamasithole on 2018/02/03.
 */

public class GroupDTO implements Parcelable {
    public GroupDTO() {
    }

    protected GroupDTO(Parcel in) {
        groupName = in.readString();
        groupTotal = in.readString();
    }

    public static final Creator<GroupDTO> CREATOR = new Creator<GroupDTO>() {
        @Override
        public GroupDTO createFromParcel(Parcel in) {
            return new GroupDTO(in);
        }

        @Override
        public GroupDTO[] newArray(int size) {
            return new GroupDTO[size];
        }
    };

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupTotal() {
        return groupTotal;
    }

    public void setGroupTotal(String groupTotal) {
        this.groupTotal = groupTotal;
    }

    private  String groupName;
    private  String groupTotal;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(groupName);
        dest.writeString(groupTotal);
    }
}
