package co.za.rtmc.vo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by jamasithole on 2018/02/03.
 */

public class Infringement implements Parcelable {

    String infringementNoticeNumber;
    String infringementDate;
    String infringementStatusDescription;
    String licenceNumber;

    public Infringement(){

    }

    public Infringement(String infringementNoticeNumber, String infringementDate, String infringementStatusDescription, String licenceNumber) {
        this.infringementNoticeNumber = infringementNoticeNumber;
        this.infringementDate = infringementDate;
        this.infringementStatusDescription = infringementStatusDescription;
        this.licenceNumber = licenceNumber;
    }

    protected Infringement(Parcel in) {
        infringementNoticeNumber = in.readString();
        infringementDate = in.readString();
        infringementStatusDescription = in.readString();
        licenceNumber = in.readString();
    }

    public static final Creator<Infringement> CREATOR = new Creator<Infringement>() {
        @Override
        public Infringement createFromParcel(Parcel in) {
            return new Infringement(in);
        }

        @Override
        public Infringement[] newArray(int size) {
            return new Infringement[size];
        }
    };

    public String getInfringementDate() {
        return infringementDate;
    }

    public void setInfringementDate(String infringementDate) {
        this.infringementDate = infringementDate;
    }

    public String getInfringementStatusDescription() {
        return infringementStatusDescription;
    }

    public void setInfringementStatusDescription(String infringementStatusDescription) {
        this.infringementStatusDescription = infringementStatusDescription;
    }

    public String getLicenceNumber() {
        return licenceNumber;
    }

    public void setLicenceNumber(String licenceNumber) {
        this.licenceNumber = licenceNumber;
    }

    public String getInfringementNoticeNumber() {
        return infringementNoticeNumber;
    }

    public void setInfringementNoticeNumber(String infringementNoticeNumber) {
        this.infringementNoticeNumber = infringementNoticeNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(infringementNoticeNumber);
        dest.writeString(infringementDate);
        dest.writeString(infringementStatusDescription);
        dest.writeString(licenceNumber);
    }
}
